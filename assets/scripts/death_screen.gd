extends Panel

# network

const MODULE = 15
const MODULE_BUY = 0
const MODULE_BUYC = 1
const MODULE_SELL = 2
const MODULE_UP = 3
var network_helper

# respawn

signal respawn

# modules 

onready var module_scn = load("res://assets/scenes/respawn_menu_module.tscn")
var modules_data = null
var last_modules = {}

var buyc_requests = []
var active_modules = []

class module:
	var id
	var name
	var desc
	var type
	var pric
	var conf
	var vals

func _ready():
	$"/root/game".connect("respawn",self,"respawn")
	$"/root/game".connect("modules_data",self,"build_modules_list")
	$"/root/game".connect("modules_update",self,"update_modules_list")
	$"/root/game".connect("mony_update",self,"update_mony")

func module_buy(id):
	if id in active_modules or !(id in last_modules.keys()):
		$"/root/game".peer.put_var([MODULE, MODULE_BUY, id])
	else:
		$"/root/game".peer.put_var([MODULE, MODULE_UP, id])

func module_buyc(id):
	$"/root/game".peer.put_var([MODULE, MODULE_BUYC, id])
	buyc_requests.append(id)

func module_sell(id):
	$"/root/game".peer.put_var([MODULE, MODULE_SELL, id])

func update_mony(player_id, mony_data):
	
	if player_id != network_helper.enet_id: return
	
	$mony.text = str(mony_data[0]) + "$ (" + str(mony_data[1]) + "$)"

func update_modules_list(player_id, bought_modules, modules_order):
	
	if player_id != network_helper.enet_id: return
	
	active_modules = []
	
	for module_node in $modules/container.get_children():
		
		module_node.get_node("buy").text = "buy"
		
		var module_id = int(module_node.name)
		if module_id in bought_modules.keys():
			
			var module_obj = null
			for module in modules_data:
				if module.id == module_id:
					module_obj = module
					update_active_modules(module_id, module_obj.conf, modules_order)
					
					if !(module_id in active_modules):
						module_node.get_node("buy").text = "act"
					else:
						module_node.get_node("buy").text = "buy"
						
			if module_obj == null: continue
			
			var text = module_obj.name + "   lvl. "
			if bought_modules[module_id] == module_obj.pric.size():
				text += "max"
				
			else:
				var module_lvl = bought_modules[module_id]
				text += str(module_lvl) + "   " + str(module_obj.pric[module_lvl]) + "$"
			
			if bought_modules[module_id] > 0 and !(module_id in buyc_requests):
				module_node.get_node("sell").disabled = false
			
			module_node.get_node("text").text = text
		
		elif module_id in last_modules.keys():
			
			var module_obj = null
			for module in modules_data:
				if module.id == module_id:
					module_obj = module
					update_active_modules(module_id, module_obj.conf, modules_order)
			if module_obj == null: continue
			
			module_node.get_node("text").text = module_obj.name + "   lvl. 0   "+str(module_obj.pric[0])+"$"
			module_node.get_node("sell").disabled = true
	
	last_modules = bought_modules.duplicate()

func update_active_modules(id, conf, order):
	var cur_order_idx = order.find(id)
	
	if cur_order_idx > -1:
		
		var can_add = true
		
		for conf_id in conf:
			var conf_order_idx = order.find(int(conf_id))
			if conf_order_idx > -1:
				if conf_order_idx > cur_order_idx:
					can_add = false
		
		if can_add: active_modules.append(id)

func build_modules_list(modules):
	modules_data = modules
	for module in modules:
		#if module.type == "mech": continue
		var m = module_scn.instance()
		m.name = str(module.id)
		m.get_node("text").text = module.name + "   lvl. 0   "+str(module.pric[0])+"$"
		m.get_node("buy").connect("pressed",self,"module_buy",[module.id])
		m.get_node("buyc").connect("pressed",self,"module_buyc",[module.id])
		m.get_node("sell").connect("pressed",self,"module_sell",[module.id])
		m.get_node("sell").disabled = true
		$modules/container.add_child(m)

func respawn(sec):
	if !visible: 
		buyc_requests = []
		show()
	
	if sec == 42: # if waiting for map after hot restart
		$head/respawn.disabled = true
		$head/respawn.text = "PLEASE_WAIT..."
		return
	
	if sec != -1:
		$head/respawn.disabled = true
		$head/respawn.text = "RESPAWNING IN " + str(int(sec+1))
	else:
		$head/respawn.disabled = false
		$head/respawn.text = "RESPAWN"

func _on_respawn_pressed():
	hide()
	emit_signal("respawn")
