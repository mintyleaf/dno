extends Node2D

var timer = 0.0

func update_bar(val):
	if $bar.modulate.a == 0:
		$anim.play("show")
	timer = 2.5
	$tw.stop_all()
	$tw.interpolate_property($bar,"value",$bar.value,val,0.25,Tween.TRANS_CUBIC,Tween.EASE_OUT)
	$tw.start()

func _ready():
	update()
	set_process(true)

func _process(delta):
	if timer > 0:
		timer -= delta
		if timer < 0:
			$anim.play("hide")
	position = get_global_mouse_position()

func _draw():
	draw_circle(Vector2(0,0),2,Color(1,1,1))