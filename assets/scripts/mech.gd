extends KinematicBody2D

# useful

const HB_LT = 0
const HB_RT = 1
const HB_LB = 2
const HB_RB = 3
const HB_CE = 4
const EYE = 5
const LEG_1 = 6
const LEG_2 = 7

const PLAYER_DATA = 2
const PD_HEAL = 5
const PROP = 22

onready var hitbox_graphics = {
		HB_LT : $"graphics/base/corpus_lt",
		HB_RT : $"graphics/base/corpus_rt",
		HB_LB : $"graphics/base/corpus_lb",
		HB_RB : $"graphics/base/corpus_rb",
		HB_CE : $"graphics/base/center",
		EYE : $"graphics/base/eye",
		LEG_1 : $"graphics/legs/leg01",
		LEG_2 : $"graphics/legs/leg02"
}

onready var hitbox_collision = {
		HB_LT : $"hitbox/lt",
		HB_RT : $"hitbox/rt",
		HB_LB : $"hitbox/lb",
		HB_RB : $"hitbox/rb",
		HB_CE : $"hitbox/c",
		EYE : $"hitbox/eye",
		LEG_1 : $"hitbox/leg01",
		LEG_2 : $"hitbox/leg02"
}

# config
var STEPS = [preload("res://assets/sounds/STEP1.wav"),
		preload("res://assets/sounds/STEP2.wav"),
		preload("res://assets/sounds/STEP3.wav")
]

var HITBOX_DEFAULT_BIG = 0
var HITBOX_DEFAULT_CENTER = 0

var MASS = 0

const GRAVITY = 980
var WALK_SPEED = 300
var JUMP_SPEED = 0
const JET_SPEED = 450
const RESISTANCE = 1500
var WALK_MAX_SPEED = 0

var JET_SPEED_FACTOR = 1
var JET_RESTORE_FACTOR = 5
var JET_FUEL_MAX = 0
var JET_ACTIVATE = 0.3
var WALK_SPEED_BONUS = 0
var MAX_JUMPS = 1

const LEGS_DEFAULT_Y = 40
const LEGS_GRAPHICS_DEFAULT_Y = 42

const PROP_BLOW_FORCE = 400
const PROP_BLOW_ROT = 15
const INV_TIME = 1.0

# input
var crouch
var walk_left
var walk_right
var jump
var jet
var dash

# animation
const DASH_PATH = "res://assets/scenes/proj/dash.tscn"

var anim = ""
var new_anim = ""
var anim_pos = 0.0
var orientation = false
var orientation_old = false
var walk_old = false
var walk_reverse = false
var oseek = 0.0
var prev_thrust_anim = ""
var legs_anim_backwards = false
var leg_0 = true

var inv_timer = INV_TIME

# gamish
var jet_activate = 0.0
var jet_fuel = JET_FUEL_MAX
var jumps = MAX_JUMPS
var crouching = false
var crouching_offset_target_y = 0
var onair_jet_time = 0.0
onready var hitbox_max = {
		HB_LT : HITBOX_DEFAULT_BIG,
		HB_RT : HITBOX_DEFAULT_BIG,
		HB_LB : HITBOX_DEFAULT_BIG,
		HB_RB : HITBOX_DEFAULT_BIG,
		HB_CE : HITBOX_DEFAULT_CENTER
}
var all_props = range(8)
var mech_team = -1

# modules
const SHIELD_NONE = 0
const SHIELD_SIMPLE = 1
const SHIELD_DOME = 2

const STEALTH_ACTIVE_COLOR = Color(1, 1, 1, 5)
const STEALTH_DEFAULT_RED_COLOR = Color(1, 0.5, 0.5, 2)
const STEALTH_DEFAULT_BLU_COLOR = Color(0.5, 0.5, 1, 2)
const STEALTH_DECLINE_COLOR = Color(0, 0, 0)

const STEALTH_MODE_INACTIVE = -1
const STEALTH_MODE_DEFAULT = 0
const STEALTH_MODE_ACTIVE = 1
const STEALTH_MODE_DECLINE = 2

var dash_reload_timer = 0

var shield_delay_timer = 0.1
var shield_timer = 0.1
var shield_hp = 0
var shield_active = false

var hidden = false
var stealth_timer = 0
var stealth_delay_timer = 0
var stealth_mode = -1
var last_anim_stealth_mode = 0

var dash_distance = 0
var dash_reload = 0

var shield_type = 0
var shield_max_hp = 0
var shield_hp_regen = 0
var shield_regen_delay = 0

var stealth = 0
var stealth_delay = 0

var aim = false

# physics
var onair_time = 0.0
var velocity = Vector2(0, 0)
var master_velocity = Vector2(0, 0)

# network
const UN_MECH_DATA = 0

const MECH_DATA_CROUCH = 0
const MECH_DATA_WALK_LEFT = 1
const MECH_DATA_WALK_RIGHT = 2
const MECH_DATA_JUMP = 3
const MECH_DATA_JET = 4
const MECH_DATA_MOUSE_POS = 5
const MECH_DATA_POS = 6
const MECH_DATA_VEL = 7
const MECH_DATA_JET_FUEL = 8
const MECH_DATA_DASH = 9
const MECH_DATA_SHIELD_HP = 10
const MECH_DATA_HIDDEN = 11
const MECH_DATA_STEALTH_MODE = 12
const MECH_DATA_DRILL = 13
const MECH_DATA_AIM = 14

var network_object = null
onready var network_helper = $"/root/network_helper"
var is_master = false

# last_sent
var last_crouch = false
var last_walk_left = false
var last_walk_right = false
var last_jump = false
var last_jet = false
var last_dash = false
var last_mouse_pos = Vector2(0, 0)
var last_pos = Vector2(0, 0)
var last_vel = Vector2(0, 0)
var last_jet_fuel = 0.0
var last_shield_hp = 0
var last_hidden = false
var last_stealth_mode = -1
var last_shield_type = -1
var last_drill = false
var last_aim = false

var test = ""

onready var node_graphics = $"graphics"
onready var node_weapon = $"weapon"
onready var node_game = $"/root/game"
onready var node_stealth_bar = $"stealth_bar/bar"
onready var node_rays = $"rays"
onready var node_shield_1 = $"shields/shield_1"
onready var node_shield_2 = $"shields/shield_2"
onready var node_leg01 = $"graphics/legs/leg01"
onready var node_leg02 = $"graphics/legs/leg02"
onready var node_leg01_thruster = $"graphics/legs/leg01/thruster"
onready var node_leg02_thruster = $"graphics/legs/leg02/thruster"
onready var node_leg01_bar = $"graphics/legs/leg01/c/bar"
onready var node_leg02_bar = $"graphics/legs/leg02/c/bar"
onready var node_legs_anim = $"legs_anim"
onready var node_base_anim = $"base_anim"


func _ready():
	
	for i in $"graphics/base".get_children():
		if i.material: i.material = i.material.duplicate()
	
	for i in node_rays.get_children():
		i.add_exception(self)
	
	if node_game.players.has(int(name)):
		network_object = node_game.players[int(name)].mech
		is_master = network_object.is_master
		team(node_game.players[int(name)].team)
		if is_master:
			node_game.connect("me_dead", self, "death")
	else:
		print("Can't link mech with network object!")
		return
	
	# wait for information from server
	yield(get_tree().create_timer(1.0), "timeout")
	
	var network_modules = node_game.players[int(name)].modules
	var network_modules_order = node_game.players[int(name)].modules_order
	var modules_data = node_game.modules_data
	$"modules".update_modules(self, is_master, network_modules, network_modules_order, modules_data)
	
	setup_shield()
	setup_stealth()
	
	yield(get_tree(),"idle_frame")
	
	node_weapon.show()
	$"weapon/appear".play("appear")
	
	jet_fuel = JET_FUEL_MAX



func _physics_process(delta):
	
	if is_master:
		
		crouch = Input.is_action_pressed("crouch")
		walk_left = Input.is_action_pressed("walk_left")
		walk_right = Input.is_action_pressed("walk_right")
		jump = Input.is_action_just_pressed("jump")
		jet = Input.is_action_pressed("jump")
		dash = Input.is_action_just_pressed("dash") and dash_distance > 0 and dash_reload_timer == 0
		node_weapon.look_at(get_global_mouse_position())
		orientation = not (get_global_mouse_position() - position).x > 0
		
		# send all
		
		if crouch != last_crouch:
			last_crouch = crouch
			send_data(MECH_DATA_CROUCH, last_crouch)
			
		if walk_left != last_walk_left:
			last_walk_left = walk_left
			send_data(MECH_DATA_WALK_LEFT, last_walk_left)
			
		if walk_right != last_walk_right:
			last_walk_right = walk_right
			send_data(MECH_DATA_WALK_RIGHT, last_walk_right)
			
		if jump != last_jump:
			last_jump = jump
			send_data(MECH_DATA_JUMP, last_jump)
			
		if jet != last_jet:
			last_jet = jet
			send_data(MECH_DATA_JET, last_jet)
			
		if dash != last_dash:
			last_dash = dash
			send_data(MECH_DATA_DASH, last_dash)
			
		if get_global_mouse_position().snapped(Vector2(1,1)) != last_mouse_pos:
			last_mouse_pos = get_global_mouse_position().snapped(Vector2(1,1))
			send_data(MECH_DATA_MOUSE_POS, last_mouse_pos)
			
		if position.round() != last_pos:
			last_pos = position.round()
			send_data(MECH_DATA_POS, last_pos)
			node_game.emit_signal("me_pos", last_pos)
			
		if velocity != last_vel:
			last_vel = velocity
			send_data(MECH_DATA_VEL, last_vel)
			
		if jet_fuel != last_jet_fuel:
			last_jet_fuel = jet_fuel
			send_data(MECH_DATA_JET_FUEL, last_jet_fuel)
			
		if shield_hp != last_shield_hp:
			last_shield_hp = shield_hp
			send_data(MECH_DATA_SHIELD_HP, last_shield_hp)
			
		if hidden != last_hidden:
			last_hidden = hidden
			send_data(MECH_DATA_HIDDEN, last_hidden)
			
		if stealth_mode != last_stealth_mode:
			last_stealth_mode = stealth_mode
			send_data(MECH_DATA_STEALTH_MODE, last_stealth_mode)
			
		if node_weapon.drill_active != last_drill:
			last_drill = node_weapon.drill_active
			send_data(MECH_DATA_DRILL, last_drill)
			
		if aim != last_aim:
			last_aim = aim
			send_data(MECH_DATA_AIM, last_aim)
		
		### modules
		
		# stealth
		
		if crouch and stealth_timer > 0 and stealth_delay_timer == 0:
			stealth_timer = clamp(stealth_timer - delta, 0, 9999.0)
			if stealth_timer == 0:
				stealth_delay_timer = stealth_delay
			hidden = true
			stealth_mode = STEALTH_MODE_ACTIVE
		else:
			stealth_timer = clamp(stealth_timer + delta, -9999.0, stealth)
			
			if stealth_delay_timer > 0:
				stealth_delay_timer = clamp(stealth_delay_timer - delta, 0, 9999.0)
				stealth_mode = STEALTH_MODE_DECLINE
			else:
				stealth_mode = STEALTH_MODE_DEFAULT
			hidden = false
		
		node_stealth_bar.value = stealth_timer
		
		# dash reload
		dash_reload_timer = clamp(dash_reload_timer - delta, 0, 9999.0)
		
		# shield delay and regen
		shield_delay_timer = clamp(shield_delay_timer - delta, 0, 9999.0)
		
		if shield_delay_timer == 0:
			if shield_hp < shield_max_hp:
				if shield_timer > 0:
					shield_timer -= delta
					if shield_timer < 0:
						shield_hp += 1
						shield_timer = shield_hp_regen
		
	else:
		crouch = network_object.crouch
		walk_left = network_object.walk_left
		walk_right = network_object.walk_right
		jump = network_object.jump
		jet = network_object.jet
		dash = network_object.dash
		node_weapon.look_at(network_object.mouse_pos)
		if (network_object.mouse_pos - position).x > 0: orientation = false
		else: orientation = true
		position = network_object.pos
		velocity = network_object.vel
		jet_fuel = network_object.jet_fuel
		shield_hp = network_object.shield_hp
		hidden = network_object.hidden
		stealth_mode = network_object.stealth_mode
	
	### inv timer
	
	if inv_timer > 0:
		inv_timer -= delta
		if inv_timer < 0:
			inv_timer = 0
	
	### modules
	
	# stealth
	
	if last_anim_stealth_mode != stealth_mode:
		
		match int(stealth_mode):
			
			STEALTH_MODE_ACTIVE:
				
				node_stealth_bar.modulate = STEALTH_ACTIVE_COLOR if is_master else Color(0,0,0,0)
				
			STEALTH_MODE_DEFAULT:
				
				match mech_team:
					0: node_stealth_bar.modulate = STEALTH_DEFAULT_RED_COLOR if is_master else Color(0,0,0,0)
					1: node_stealth_bar.modulate = STEALTH_DEFAULT_BLU_COLOR if is_master else Color(0,0,0,0)
					
			STEALTH_MODE_DECLINE:
				
				node_stealth_bar.modulate = STEALTH_DECLINE_COLOR if is_master else Color(0,0,0,0)
		
		last_anim_stealth_mode = stealth_mode
	
	# hidden vfx
	
	if inv_timer == 0:
		
		modulate.a = 0.2 if hidden else 1.0
	
	# shield on/off
	
	if shield_type > 0:
		var need_shield_update = false
		if shield_hp < 1:
			if shield_active: 
				shield_active = false
				need_shield_update = true
		else:
			if !shield_active:
				shield_active = true
				need_shield_update = true
		
		if need_shield_update:
			match shield_type:
				
				SHIELD_SIMPLE:
					
					node_shield_1.set_collision_layer_bit(4, shield_active)
					node_shield_1.set_collision_mask_bit(4, shield_active)
					node_shield_1.visible = shield_active
					
				SHIELD_DOME:
					
					node_shield_2.set_collision_layer_bit(4, shield_active)
					node_shield_2.set_collision_mask_bit(4, shield_active)
					node_shield_2.visible = shield_active
	
	# weapon orient
	
	node_weapon.orientation = orientation
	
	# leg indicator
	
	if JET_FUEL_MAX != 0:
		node_leg01_bar.color.a = lerp(0, 1, jet_fuel / JET_FUEL_MAX)
		node_leg02_bar.color.a = lerp(0, 1, jet_fuel / JET_FUEL_MAX)
	
	# mirroring
	
	if orientation_old != orientation:
		if anim == "walk": oseek = node_legs_anim.current_animation_position
		anim = ""
		orientation_old = orientation
	if walk_left and !walk_old:
		anim = ""
		velocity.x = 0
		walk_old = true
	if walk_right and walk_old:
		anim = ""
		velocity.x = 0
		walk_old = false
		
	if orientation:
		if walk_left: walk_reverse = false
		else: walk_reverse = true
		node_graphics.scale.x = -1
		$"hitbox".scale.x = -1
		node_rays.scale.x = -1
	else:
		if walk_right: walk_reverse = false
		else: walk_reverse = true
		node_graphics.scale.x = 1
		$"hitbox".scale.x = 1
		node_rays.scale.x = 1
		
	###
	
	if jet: jet_activate+=delta
	
	if network_object.is_master:
		
		if walk_left:
			if abs(velocity.x) < WALK_MAX_SPEED - MASS:
				if abs(velocity.x) < (WALK_SPEED + WALK_SPEED_BONUS): velocity.x=-(WALK_SPEED + WALK_SPEED_BONUS)
				velocity.x-=(WALK_SPEED + WALK_SPEED_BONUS)*delta
		if walk_right:
			if abs(velocity.x) < WALK_MAX_SPEED - MASS:
				if abs(velocity.x) < (WALK_SPEED + WALK_SPEED_BONUS): velocity.x=(WALK_SPEED + WALK_SPEED_BONUS)
				velocity.x+=(WALK_SPEED + WALK_SPEED_BONUS)*delta
		if jump and jumps > 0:
			velocity.y = -(JUMP_SPEED - MASS/2)
			jumps-=1
			
		if jet and jet_activate > JET_ACTIVATE and jet_fuel > 0:
			velocity.y = -(JET_SPEED - MASS/2)*JET_SPEED_FACTOR
			jet_fuel-=delta
			
		if !walk_left and !walk_right:
			if velocity.x > 0:
				velocity.x = clamp(velocity.x - RESISTANCE*delta,0,INF)
			elif velocity.x < 0:
				velocity.x = clamp(velocity.x + RESISTANCE*delta,-INF,0)
		
	# crouching
	
	if crouch:
		crouching = true
	else:
		if !$"rays/crouch_0".is_colliding() and !$"rays/crouch_1".is_colliding(): crouching = false
		
	if crouching:
		if crouching_offset_target_y == 0:
			crouching_offset_target_y = -16
	else:
		if crouching_offset_target_y == -16:
			crouching_offset_target_y = 0
			
	$"legs".position.y = lerp($"legs".position.y,LEGS_DEFAULT_Y + crouching_offset_target_y,0.4)
	$"graphics/legs".position.y = lerp($"graphics/legs".position.y,LEGS_GRAPHICS_DEFAULT_Y + crouching_offset_target_y,0.4)
	
	# onair
	
	if velocity.y != 0 and !is_on_floor():
		onair_time += delta
		if jet and jet_activate > 0.3 and jet_fuel > 0:
			if node_leg01_thruster.animation == "open" and node_leg01_thruster.frame == 7:
				node_leg01_thruster.animation = "fire"
				node_leg02_thruster.animation = "fire"
			elif node_leg01_thruster.animation != "fire":
				node_leg01_thruster.animation = "open"
				node_leg02_thruster.animation = "open"
			onair_jet_time += delta
		else:
			node_leg01_thruster.animation = "close"
			node_leg02_thruster.animation = "close"
		if node_leg01_thruster.animation != prev_thrust_anim:
			$"graphics/legs/thrust_anim".play(node_leg01_thruster.animation)
			prev_thrust_anim = node_leg01_thruster.animation
			
	if is_on_floor():
		if onair_time > 0.1:
			onair_time = 0.0
			onair_jet_time = 0.0
			jumps = MAX_JUMPS
			jet_activate = 0.0
			sound_footstep(0)
		if jet_fuel < JET_FUEL_MAX: jet_fuel+=delta*JET_RESTORE_FACTOR
		if jet_fuel > JET_FUEL_MAX: jet_fuel = JET_FUEL_MAX
	
	# dash
	
	if dash:
		
		var side = 1
		var pos_before = global_position
		
		if ((orientation and walk_right) or (!orientation and walk_left)): 
			side = -1
		
		move_and_collide(Vector2(dash_distance * side * node_rays.scale.x,0))
		dash_reload_timer = dash_reload
		
		var p = load(DASH_PATH).instance()
		p.points[0] = pos_before
		p.points[1] = global_position
		$"/root/game/proj".add_child(p)
	
	# animation
	
	if node_legs_anim.playback_speed != abs(velocity.x) / 50 and abs(velocity.x) / 50 > 1:
		node_legs_anim.playback_speed = abs(velocity.x) / 50
		node_base_anim.playback_speed = abs(velocity.x) / 50
		
	if anim == "walk":
		anim_pos = node_legs_anim.current_animation_position
		
	if onair_time > 0.1:
		var angle
		if velocity.y < 0:
			new_anim = "jump"
			angle = (velocity.angle() + PI / 2) * node_graphics.scale.x + deg2rad(10)
		else:
			angle = (velocity.angle() + PI / 2) * node_graphics.scale.x * (-node_graphics.scale.x) * 0.15
			new_anim = "fall"
			if jet_activate <= 0.3 and jumps > 0:
				jet_activate = 0.0
		node_leg01.rotation = lerp(node_leg01.rotation, clamp(angle, deg2rad(-35), deg2rad(35)), 0.2)
		node_leg02.rotation = lerp(node_leg02.rotation, clamp(angle, deg2rad(-35), deg2rad(35)), 0.2)
		
	else:
		if abs(velocity.x) < 50: new_anim = "idle"
		else: new_anim = "walk"
		
	if anim != new_anim:
		anim = new_anim
		
		if anim_pos > 0.5 and anim_pos < 1.5:
			leg_0 = false
		else:
			leg_0 = true
			
		
		match new_anim:
			
			"jump", "fall":
				
				if walk_reverse:
					node_legs_anim.play("free_0" if leg_0 == walk_reverse else "free_1")
				else:
					node_legs_anim.play("free_1" if leg_0 == walk_reverse else "free_0")
			
			"idle":
				
				if walk_reverse:
					node_legs_anim.play("idle_0" if leg_0 == walk_reverse else "idle_1")
				else:
					node_legs_anim.play("idle_1" if leg_0 == walk_reverse else "idle_0")
				
			"walk":
				
				leg_0 =  node_legs_anim.current_animation in ["idle_0","free_0"]
				
				if walk_reverse:
					node_base_anim.play_backwards("walk")
					node_legs_anim.play_backwards("walk")
					legs_anim_backwards = true
				
				else:
					node_base_anim.play("walk")
					node_legs_anim.play("walk")
					legs_anim_backwards = false
				
				if !leg_0:
					node_legs_anim.seek(1.0)
				
				if oseek != 0.0:
					node_legs_anim.seek(oseek)
					oseek = 0.0
		
		node_base_anim.play(new_anim)


	# physics
	if network_object.is_master:
		velocity.y += GRAVITY * delta
		master_velocity = velocity
	velocity = move_and_slide(velocity,Vector2(0,-1))

func setup_stealth():
	
	match mech_team:
		0: node_stealth_bar.modulate = STEALTH_DEFAULT_RED_COLOR
		1: node_stealth_bar.modulate = STEALTH_DEFAULT_BLU_COLOR
	
	if stealth > 0:
		
		if is_master:
			node_stealth_bar.max_value = stealth
			stealth_timer = stealth
		else:
			node_stealth_bar.max_value = 1
			node_stealth_bar.value = 1


func setup_shield():
	
	match int(shield_type):
		
		SHIELD_NONE:
			
			node_shield_1.set_collision_layer_bit(4, false)
			node_shield_1.set_collision_mask_bit(4, false)
			
			node_shield_2.set_collision_layer_bit(4, false)
			node_shield_2.set_collision_mask_bit(4, false)
			
			node_shield_1.hide()
			node_shield_2.hide()
			
		SHIELD_SIMPLE:
			
			node_shield_1.set_collision_layer_bit(4, true)
			node_shield_2.set_collision_mask_bit(4, false)
			
			node_shield_2.set_collision_layer_bit(4, false)
			node_shield_1.set_collision_mask_bit(4, true)
			
			node_shield_1.show()
			node_shield_2.hide()
			
			var l_team_color = Color(1, 1, 1)
			
			match mech_team:
				0:
					l_team_color = Color(1, 0.5, 0.5)
				1:
					l_team_color = Color(0.5, 0.5, 1)
				
			$"shields/shield_1/tex".self_modulate = l_team_color
			
		SHIELD_DOME:
			
			node_shield_1.set_collision_layer_bit(4, false)
			node_shield_2.set_collision_mask_bit(4, true)
			
			node_shield_2.set_collision_layer_bit(4, true)
			node_shield_1.set_collision_mask_bit(4, false)
			
			node_shield_1.hide()
			node_shield_2.show()
			
			var l_team_color = Color(1, 1, 1)
			
			match mech_team:
				0:
					l_team_color = Color(1, 0.5, 0.5)
				1:
					l_team_color = Color(0.5, 0.5, 1)
				
			$"shields/shield_2/tex".self_modulate = l_team_color


func reset_stealth():
	stealth_delay_timer = stealth_delay


func shield_damage():
	shield_delay_timer = shield_regen_delay
	
	match shield_type:
		
		SHIELD_SIMPLE: 
			$"shields/shield_1/anim".play("dmg")
		
		SHIELD_DOME: 
			$"shields/shield_2/anim".play("dmg")


func heal_hitbox(part, amount):
	
	hitbox_max[part] += amount
	if is_master:
		node_game.peer.put_var([PLAYER_DATA, PD_HEAL, part, amount])


func update_hitbox(part=null):
	
	if part != null:
		
		shield_damage()
		
		var max_hp = hitbox_max[part]
		var hp = network_object.hitbox[part]
		
		var force = 0
		if max_hp != 0:
			force = float(max_hp - hp) / max_hp
		
		hitbox_graphics[part].get_node("hit_anim").play("hit")
		hitbox_graphics[part].material.set_shader_param("force",force)
		if hp < 1:
			hitbox_graphics[part].hide()
			hitbox_collision[part].set_collision_layer_bit(4, false)
			hitbox_collision[part].set_collision_mask_bit(4, false)
			if is_master:
				request_part_prop(
						part,
						offset_polygon(
								hitbox_collision[part].get_node("col").polygon, 
								hitbox_graphics[part].position, 
								part),
						hitbox_graphics[part].global_position,
						0.0,
						(hitbox_graphics[part].position.normalized() + Vector2(0, -0.5)) * PROP_BLOW_FORCE * node_graphics.scale,
						(1.0 - randf()) * PROP_BLOW_ROT)
	
	else:
		
		for part in hitbox_max.keys():
			
			var max_hp = hitbox_max[part]
			var hp = network_object.hitbox[part]
			var force = 0
			if max_hp != 0:
				force = float(max_hp-hp) / max_hp
			
			hitbox_graphics[part].material.set_shader_param("force",force)
			if hp < 1:
				hitbox_graphics[part].hide()
				hitbox_collision[part].set_collision_layer_bit(4, false)
				hitbox_collision[part].set_collision_mask_bit(4, false)
				if is_master:
					request_part_prop(
							part,
							offset_polygon(
									hitbox_collision[part].get_node("col").polygon,
									hitbox_graphics[part].position, 
									part),
							hitbox_graphics[part].global_position,
							0.0,
							(hitbox_graphics[part].position.normalized() + Vector2(0, -0.5)) * PROP_BLOW_FORCE * node_graphics.scale,
							(1.0 - randf()) * PROP_BLOW_ROT)
			else:
				hitbox_graphics[part].show()
				hitbox_collision[part].set_collision_layer_bit(4, true)
				hitbox_collision[part].set_collision_mask_bit(4, true)


func death():
	set_process(false)
	while all_props.size() > 0:
		var part = all_props[0]
		request_part_prop(
				part,
				offset_polygon(
						hitbox_collision[part].get_node("col").polygon,
						hitbox_graphics[part].position, part),
				hitbox_graphics[part].global_position,
				0.0,
				(hitbox_graphics[part].position.normalized() + Vector2(0, -0.5)) * PROP_BLOW_FORCE,
				(1.0 - randf()) * PROP_BLOW_ROT)
	
	if all_props.size() == 0:
		queue_free()


func offset_polygon(p, o, part):
	var normalized_p = PoolVector2Array()
	
	if part in [LEG_1,LEG_2]:
		if !leg_0: o *= -1
		o += $"graphics/legs".position
	
	for point in p:
		normalized_p.append(point - o)
	
	return normalized_p


func sound_footstep(what):
	match what:
		0:
			$"footsteps1".stop()
			$"footsteps1".stream = STEPS[randi()%STEPS.size()]
			$"footsteps1".play()
		1:
			$"footsteps2".stop()
			$"footsteps2".stream = STEPS[randi()%STEPS.size()]
			$"footsteps2".play()


func team(team): # future: color setup 
	
	mech_team = team
	
	var color = Color(1, 1, 1)
	
	match team:
		0:
			color = Color(1, 0.15, 0.1)
		1:
			color = Color(0.15, 0.1, 1)
	
	$"graphics/base/center/team".modulate = color.lightened(0.5)
	$"graphics/base/eye/team".modulate = color.lightened(0.5)
	node_leg01_bar.color = color.lightened(0.5)
	node_leg02_bar.color = color.lightened(0.5)
	$"light".color = color.lightened(0.5)
	
	$"weapon/aim_line".modulate = color.lightened(0.5)


func request_part_prop(part,col,pos,rot,vel,ang):
	
	if part in all_props:
		all_props.erase(part)
	else:
		return
	
	var texture_path = null
	
	match part:
		HB_LT: texture_path = "res://assets/graphics/props/mech/lt.png"
		HB_RT: texture_path = "res://assets/graphics/props/mech/rt.png"
		HB_LB: texture_path = "res://assets/graphics/props/mech/lb.png"
		HB_RB: texture_path = "res://assets/graphics/props/mech/rb.png"
		HB_CE: texture_path = "res://assets/graphics/props/mech/c.png"
		EYE: texture_path = "res://assets/graphics/props/mech/eye.png"
		LEG_1: texture_path = "res://assets/graphics/props/mech/leg.png"
		LEG_2: texture_path = "res://assets/graphics/props/mech/leg.png"
		_: return
	
	var data = [PROP, pos, rot, vel, ang, texture_path, col]
	node_game.peer.put_var(data)


func send_data(key, val):
	var data = [UN_MECH_DATA, key, val]
	if get_tree().multiplayer.has_network_peer():
		get_tree().multiplayer.send_bytes(var2bytes(data), 1, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)
	
