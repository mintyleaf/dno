extends RigidBody2D

const VOID_Y_LEVEL = 3600
const UN_PROP_DATA = 2
const PROP_POS = 0
const PROP_ROT = 1
const PROP_VEL = 2
const PROP_ANG = 3

const TIMEOUT = 10.0
var timer = 0.0

var is_master = false

var last_pos = Vector2(0,0)
var last_rot = 0.0
var last_vel = Vector2(0,0)
var last_ang = 0.0

var connection_by_id = {}
var props = {}

var data_tex = null
var data_col = null

func _ready():
	
	if is_master:
		queue_free()
	
	timer = TIMEOUT + randi()%int(TIMEOUT) - TIMEOUT/2

func build_prop(pos,rot,vel,ang,tex,col):
	
	position = pos
	rotation = rot
	linear_velocity = vel
	angular_velocity = ang
	
	$tex.texture = load(tex)
	$col.set_polygon(col)
	
	data_tex = tex
	data_col = col

func _process(delta):
	
	if timer > 0:
		timer -= delta
		if timer < 0:
			$anim.play("disappear")
	
	if position.y > VOID_Y_LEVEL:
		queue_free()