extends Control

const TEAM_RED = 0
const TEAM_BLU = 1

const PLAYER_RED_COLOR = Color(1,0.2,0.2)
const PLAYER_BLU_COLOR = Color(0.2,0.2,1)
const PLAYER_HIDDEN_COLOR = Color(0,0,0,0)

const SEAT_RED_COLOR = Color(1,0.4,0.4)
const SEAT_BLU_COLOR = Color(0.4,0.4,1)

const POINT_STATE_DEACTIVE_COLOR = Color(0,0,0,0)
const POINT_STATE_ACTIVE_COLOR = Color(1,1,1)
const POINT_STATE_CAP_RED_COLOR = Color(1,0.7,0.7)
const POINT_STATE_CAP_BLU_COLOR = Color(0.7,0.7,1)
const POINT_STATE_CAPPED_RED_COLOR = Color(1,0.3,0.3)
const POINT_STATE_CAPPED_BLU_COLOR = Color(0.3,0.3,1)

const POINT_STATE_DEACTIVE = 0
const POINT_STATE_ACTIVE = 1 
const POINT_STATE_CAP_RED = 2
const POINT_STATE_CAP_BLU = 3
const POINT_STATE_CAPPED_RED = 4
const POINT_STATE_CAPPED_BLU = 5

var map_offset = Vector2(0,0)
var players = {}
var last_move_pos = Vector2(0,0)

func _ready():
	$"/root/game/tilemap".connect("map_ready",self,"load_map")
	$"/root/game".connect("seat",self,"load_seats")
	$"/root/game".connect("me_pos",self,"move")
	$"/root/game".connect("cell",self,"cell")
	players = $"/root/game".players

func world_to_map(vec):
	return $"/root/game/tilemap".world_to_map(vec)

func pos_align(pos):
	return (world_to_map(pos) - map_offset)

func _process(delta):
	
	var player_positions = []
	var player_colors = []
	
	for pl in players:
		if players[pl].mech != null:
			if players[pl] == $"/root/game".me:
				match players[pl].team:
					TEAM_RED:
						player_colors.append(PLAYER_RED_COLOR.lightened(0.5))
					TEAM_BLU:
						player_colors.append(PLAYER_BLU_COLOR.lightened(0.5))
				player_positions.append(last_move_pos)
				continue
			else:
				player_positions.append(pos_align(players[pl].mech.pos))
			if players[pl].team != $"/root/game".me.team and players[pl].mech.hidden:
				player_colors.append(PLAYER_HIDDEN_COLOR)
			elif players[pl].team == TEAM_RED:
				player_colors.append(PLAYER_RED_COLOR)
			elif players[pl].team == TEAM_BLU:
				player_colors.append(PLAYER_BLU_COLOR)
			else:
				player_colors.append(Color(0,0,0))
	
	$container/map/players.player_positions = player_positions
	$container/map/players.player_colors = player_colors
	$container/map/players.update()
			

func update_point_state(p_name, pos, state):
	if $container/map/points.points == null:
		$container/map/points.points = {p_name : [pos_align(pos), state]}
	else:
		$container/map/points.points[p_name] = [pos_align(pos), state]
	
	$container/map/points.update()

func move(pos):
	
	last_move_pos = pos_align(pos)

	$container/map.position = - (last_move_pos * $container/map.scale.x - $container.rect_size/2) 

func load_seats(seat_objs):
	
	var seat_positions = []
	
	for seat in seat_objs:
		var c = Color(0,0,0)
		match seat.team:
			TEAM_RED: c = SEAT_RED_COLOR
			TEAM_BLU: c = SEAT_BLU_COLOR
		seat_positions.append([pos_align(seat.position),c])

	$container/map/seats.seat_positions = seat_positions
	$container/map/seats.update()

func cell(pos, is_solid):
	
	var img = $container/map.texture.get_data()
	img.lock()
	
	var color = Color(0,0,0)
	
	if is_solid:
		color = Color(0.3,0.3,0.3)
	else:
		color = Color(0.1,0.1,0.1)
	
	img.set_pixelv(pos - map_offset, color)
	
	img.unlock()
	
	var imgtex = ImageTexture.new()
	imgtex.create_from_image(img,0)
	
	$container/map.texture = imgtex
	$container/map.update()

func load_map(tilemap):
	
	var used_rect = tilemap.get_used_rect()
	var size = used_rect.end - used_rect.position
	
	map_offset = used_rect.position
	
	var img = Image.new()
	img.create(size.x,size.y,false,Image.FORMAT_RGBA8)
	img.lock()
	
	for id in tilemap.BG_CELLS:
		for cell in tilemap.get_used_cells_by_id(id):
			img.set_pixelv(cell - used_rect.position,Color(0.1,0.1,0.1))
			
	for id in tilemap.CELLS:
		for cell in tilemap.get_used_cells_by_id(id):
			img.set_pixelv(cell - used_rect.position,Color(0.3,0.3,0.3))
			
	for id in tilemap.HARD_CELLS:
		for cell in tilemap.get_used_cells_by_id(id):
			img.set_pixelv(cell - used_rect.position,Color(0.5,0.5,0.5))
	
	img.unlock()
	
	var imgtex = ImageTexture.new()
	imgtex.create_from_image(img,0)
	
	$container/map.texture = imgtex
	$container/map.update()
	
	yield(get_tree(),"idle_frame")
	
	$container/map/points.POINTS_STATE_COLORS = {
		POINT_STATE_ACTIVE : POINT_STATE_ACTIVE_COLOR,
		POINT_STATE_CAPPED_BLU : POINT_STATE_CAPPED_BLU_COLOR,
		POINT_STATE_CAPPED_RED : POINT_STATE_CAPPED_RED_COLOR,
		POINT_STATE_CAP_BLU : POINT_STATE_CAP_BLU_COLOR,
		POINT_STATE_CAP_RED : POINT_STATE_CAP_RED_COLOR,
		POINT_STATE_DEACTIVE : POINT_STATE_DEACTIVE_COLOR
	}
	$container/map/points.points = null
	$container/map/points.update()