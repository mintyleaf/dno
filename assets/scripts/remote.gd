extends "res://assets/scripts/net_constants.gd"

const AUTH = 3
const AUTH_RESPONSE = 2
const EXEC = 2
const RESPONSE = 0

var tcp_server
var connection = {}

var keyring
var key = null

var commands = { # func name : args count (except 'who')
	"help" : 0, 
	"restart" : 0,
	"timer" : 1,
	"team_points" : 2
	#"kill" : 1
	}

class remoter_data:
	var ip = ""
	var id = -1

#func kill(who, nick):
#
#	if !nick is String:
#		send_data(who,RESPONSE,connection[who].id,"Nick isn't string?")
#		return
#
#	for client in $"../game".connection:
#		if $"../game".connection[client].nickname == nick:
#			$"../game".broadcast_kill($"../game".connection[client])
#			send_data(who,RESPONSE,connection[who].id,nick + " killed!")
#			return
#
#	send_data(who,RESPONSE,connection[who].id,"No such player: "+nick)

func team_points(who, key, amount):
	
	if $"../game".team_points.keys().has(int(key)):
		$"../game".team_points[int(key)] += int(amount)
		$"../game".team_points_last = int(key)
		$"../game".update_team_points()
	else:
		send_data(who,RESPONSE,connection[who].id,"Wrong key value!")
	
	send_data(who,RESPONSE,connection[who].id,"Team points added!")

func timer(who, time):
	
	$"../game".timer = int(time)
	
	send_data(who,RESPONSE,connection[who].id,"Game timer set!")

func help(who):
	var resp = "List of available commands: \n"
	
	for c in commands.keys():
		resp += c + ", "
	resp = resp.left(resp.length()-2)
	
	send_data(who,RESPONSE,connection[who].id,resp)

func restart(who):
	
	$"..".restart()
	
	send_data(who,RESPONSE,connection[who].id,"Server restarted!")
	
func start_remote():
	
	set_process(false)
	
	tcp_server = TCP_Server.new()
	if tcp_server.listen(PORT+1) == OK:
		state.print_log("Remote helper started on port "+str(PORT+1))
	if !load_keyring():
		state.print_log("No private file found! Remote helper is running in free mode")
	else:
		key = keyring["remote_key"]
	
	set_process(true)
	

func send_data(who, type, id, body):
	
	var size = body.to_utf8().size() + "F".to_utf8().size() + 8
	
#	print("SEND")
#	print(size," ",id," ",type," ",body)
	
	who.put_32(size)
	who.put_32(id)
	who.put_32(type)
	who.put_data(body.to_utf8())
	who.put_data("F".to_utf8())
	

func _process(delta):
	
	if tcp_server == null: return
	
	if tcp_server.is_connection_available() and connection.size() < MAX_REMOTES:
		var client = tcp_server.take_connection()
		connection[client] = remoter_data.new()
		connection[client].ip = client.get_connected_host()
		state.print_log("Remote from " + connection[client].ip + " open")
		
	for client in connection:
		
		client.get_status()
		
		if !client.is_connected_to_host():
			state.print_log("Remote from " + connection[client].ip + " closed")
			connection.erase(client)
			client = null
			continue
		
		if client.get_available_bytes() > 0:
			
			var size = client.get_32()
			var id = client.get_32()
			var type = client.get_32()
			var body = client.get_utf8_string(size-10)
			client.get_utf8_string(2)
			
#			print("RECV")
#			print(size," ",id," ",type," ",body)
			
			match type:
				
				AUTH:
					
					if body == key:
						
						state.print_log("Remote from " + connection[client].ip + " auth successful!")
						send_data(client,AUTH_RESPONSE,id,"F")
						
						connection[client].id = id
						
					else:
						
						state.print_log("Remote from " + connection[client].ip + " auth unsuccessful!")
						send_data(client,AUTH_RESPONSE,-1,"F")
						
						client.disconnect_from_host()
						connection.erase(client)
						client = null
				
				EXEC:
					
					var command = body.split(" ")[0]
					
					if command in commands.keys():
						
						var args = body.split(" ")
						args = Array(args)
						args[0] = client
						
						if args.size() == commands[command]+1:
							callv(command, args)
						else:
							send_data(client,RESPONSE,id,"This command need to have " + str(commands[command]) + " arg(s)!")
					
					else:
						send_data(client,RESPONSE,id,"No such command found! Try help for list of available commands.")

func load_keyring():
	var f = File.new()
	if f.open("res://private",File.READ) == OK:
		keyring = f.get_as_text()
		keyring = parse_json(keyring)
		f.close()
		return true
	else:
		return false