extends Node2D

# frontend

const POINT_STATE_DEACTIVE = 0
const POINT_STATE_ACTIVE = 1 
const POINT_STATE_CAP_RED = 2
const POINT_STATE_CAP_BLU = 3
const POINT_STATE_CAPPED_RED = 4
const POINT_STATE_CAPPED_BLU = 5

var hp = -1
var state = -1
var time = -1

# backend

const POINTS_SCN_PATH = "res://assets/scenes/points"

const POINT = 7

const ORIENT_HORZ = 0
const ORIENT_VERT = 1

const POINT_ENTER = 0
const POINT_EXIT = 1

var type = ""
var orientation_type = 0
var root_point = Vector2()
var mirror_x = false
var length = -1

const MAX_HP = 20
const MAX_TIME = 5.0

var progress = 0.0
var old_progress = 0.0

func update_info():
	var color = Color(1, 1, 1)
	
	match state:
		POINT_STATE_CAPPED_RED:
			color = Color(1, 0.15, 0.1)
		POINT_STATE_CAPPED_BLU:
			color = Color(0.15, 0.1, 1)
		POINT_STATE_DEACTIVE:
			color = Color(0.1, 0.1, 0.1)
	
	progress = (1 - time/MAX_TIME) * float(hp)/MAX_HP
	
	if hp < 0:
		progress = 0.0
		
		
	elif state in [POINT_STATE_CAPPED_RED, POINT_STATE_CAPPED_BLU]:
		progress = float(hp)/MAX_HP
	
	# TODO: Please, optimize this nodes!
	if progress > old_progress and progress >= 1.0:
		
		$"point/indicator/anim".play("pulse")
		if has_node("point/indicator2"):
			$"point/indicator2/anim".play("pulse")
	
	if $"point/indicator/light".color != color:
		$"point/indicator/color".color = color.lightened(0.5)
		$"point/indicator/light".color = color
		if has_node("point/indicator2") and $"point/indicator2/light".color != color:
			$"point/indicator2/color".color = color.lightened(0.5)
			$"point/indicator2/light".color = color
	
	if $point/anim.current_animation != "default":
		$point/anim.current_animation = "default"
	
	$point/anim.advance((progress - old_progress))
	old_progress = progress
	
	#var text = str(progress) + " HP: " + str(hp)
	#var text = name + ", " + type + ", " + str(orientation_type) + "\nState : " + str(state) + ", Time : " + str(int(time*10)/10.0) + ", HP : " + str(hp)
	#$info.text = text
	
func set_hp(val):
	hp = val
	update_info()
	
func set_state(val):
	state = val
	$"/root/game/fg/hud/minimap".update_point_state(name, position, val)
	update_info()
	
func set_time(val):
	time = val
	update_info()

func _on_capture_body_entered(body):
	if body == $"/root/game".me_mech:
		$"/root/game".peer.put_var(
		[POINT, POINT_ENTER, int(name)]
		)

func _on_capture_body_exited(body):
	if body == $"/root/game".me_mech:
		$"/root/game".peer.put_var(
		[POINT, POINT_EXIT, int(name)]
		)

func load_visual():
	
	var points_scn = []
	
	var dir = Directory.new()
	if dir.open(POINTS_SCN_PATH) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while (file_name != ""):
			if !dir.current_is_dir():
				points_scn.append(file_name.replace(".tscn",""))
			file_name = dir.get_next()
	
	var point_scn = null
	
	for point_name in points_scn:
		if type.begins_with(point_name):
			point_scn = load(POINTS_SCN_PATH + "/" + point_name + ".tscn").instance()
			
	if point_scn == null:
		if "default" in points_scn:
			print(type + " scene not found!")
			point_scn = load(POINTS_SCN_PATH + "/default.tscn").instance()
		else: return
	
	add_child(point_scn)
	
	var s = randf()
	$"point/indicator/anim".seek(s)
	if has_node("point/indicator2"):
		$"point/indicator2/anim".seek(s)

func _ready():
	$info.hide()
	var tilemap = $"/root/game/tilemap"
	var move = Vector2(0,0)
	if orientation_type in [0,2]:
		if length%2 == 0:
			move += Vector2(0,16) # center tile
		else:
			move += Vector2(16,16) # center tile
	elif orientation_type == 1:
		if length%2 == 0:
			move += Vector2(16,32)
		else:
			move += Vector2(16,16)
	
	move += tilemap.map_to_world(root_point) # move to struct root point
	
	var cell_on_tilemap = tilemap.world_to_map(position + move)
	
	if !(tilemap.get_cellv(
	cell_on_tilemap
	) in tilemap.BG_CELLS or
	tilemap.get_cellv(
	cell_on_tilemap
	) in tilemap.HARD_CELLS
	): # mirror side struct
		move.x*=-1
		move.x+=32
		
		# orientation right
		if orientation_type == 1:
			rotation_degrees = 90
	
	# orientation left
	if orientation_type == 1 and rotation_degrees == 0:
		rotation_degrees = -90
	
	if type.begins_with("engine_side"):
		if rotation_degrees == 90:
			move.x+=16
			move.y-=16
		elif rotation_degrees == -90:
			move.x-=16
			move.y-=16
		rotation_degrees = 0
	
	position += move
	
	load_visual()