extends Control

var chat_active = false
var team_chat_focus = false

var timer = 0.0
var hidden = true

var messages = []

onready var networkv2 = $"/root/networkv2"

func _ready():
	set_process(true)
	
func _process(delta):
	
	if timer > 0:
		timer-=delta
		if hidden:
			$anim.play("show")
			hidden = false
		if timer < 0:
			if !hidden:
				$anim.play("hide")
				hidden = true
	
	if Input.is_action_pressed("chat_team"): team_chat_focus = true
	
	if team_chat_focus:
		$team.text = "T"
	else:
		$team.text = "A"
	
	if chat_active: timer = 5.0
	
	if Input.is_action_just_pressed("chat_all"):
		if !chat_active:
			$line.grab_focus()
			chat_active = true
			$"/root/game".ui_busy = true
		else:
			
			if $line.text.length() > 0:
				if team_chat_focus:
					$"/root/game".peer.put_var([3,$line.text,true])
				else:
					$"/root/game".peer.put_var([3,$line.text,false])
			
			$line.clear()
			$line.release_focus()
			chat_active = false
			$"/root/game".ui_busy = false
			team_chat_focus = false

func redraw_chat():
	$list.clear()
	for i in messages:
		$list.append_bbcode(i)
		$list.newline()

func update_chat(message,id,team):
	var who_formated = networkv2.get_player_text_data(id)
	if team:
		message = "[color="+who_formated[1]+"][team] "+who_formated[0]+"[/color]: " + message
	else:
		message = "[color="+who_formated[1]+"][all] "+who_formated[0]+"[/color]: " + message
	messages.insert(0,message)
	
	$list.clear()
	for i in messages:
		$list.append_bbcode(i)
		$list.newline()
	
	timer = 5.0
	pass