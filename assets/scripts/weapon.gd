extends Node2D

# const visual

const VISUAL_X_OFFSETS = {
	1 : 1,
	2 : 47,
	3 : 1,
}
const CLIP_X_OFFSET = 1
const ROCKET_CLIP_X_OFFSET = 14
const LASER_CLIP_S_X_OFFSET = 4
const ROCKET_CLIP_Y_OFFSET = 1
const LASER_CLIP_Y_OFFSET = 3
const BOLT_X_OFFSET = 2
const TEAM_RED_COLOR = Color(1,0.2,0.2)
const TEAM_BLU_COLOR = Color(0.2,0.2,1)

const LASER_CLIP_BAR_SIZES = {
	0 : 2,
	1 : 3,
	2 : 4,
	3 : 3,
	4 : 4,
	5 : 5,
	6 : 4,
	7 : 5,
	8 : 6
}
const LASER_BASE_BAR_SIZES_X_OFFSETS = {
	0 : [1,8],
	1 : [3,7],
	2 : [5,6],
	3 : [7,5],
	4 : [9,4]
}
const LASER_BASE_S_BAR_X_OFFSET = 4

const GRENADE_LAUNCHER_X_OFFSET = 2
const GRENADE_LAUNCHER_Y_OFFSET = 1
const GRENADE_LAUNCHER_BASE_S_Y_OFFSET = 3
const GRENADE_LAUNCHER_RE_BOOST_Y_OFFSET = 1
const GRENADE_LAUNCHER_RE_BOOST_S_Y_OFFSET = 2
const GRENADE_LAUNCHER_RE_BOOST_LASER_Y_OFFSET = 4
const GRENADE_LAUNCHER_ROCKET_Y_OFFSET = 8

# const network

const UN_WEAPON_DATA = 1 
const WEAPON_DATA_SHOOT = 0

const HIT = 6
const TILEMAP_HIT = 21
const TILEMAP_EXPLODE = 23
const POINT = 7
const POINT_HIT = 2
const TILEMAP_HP = 10
var HARD_CELLS = range(8,16)

# const for shoot packets

const SHOOT_TYPE_FIREARM = 0
const SHOOT_TYPE_LASER = 1
const SHOOT_TYPE_EXPLOSIVE = 2
const SHOOT_TYPE_GRENADE = 3

const SHOOT_TYPE = 0
const SHOOT_PREF = 1
const SHOOT_AMMO = 2

const LASER_PREF_RAY_PATH = 0
const LASER_PREF_OUT = 1
const LASER_PREF_ALIVE_TIME = 2

const TYPE_ROCKET = 0
const TYPE_GRENADE = 1

const EXPLOSIVE_PREF_RADIUS = 0
const EXPLOSIVE_PREF_GRAVITY = 1
const EXPLOSIVE_PREF_VELOCITY = 2
const EXPLOSIVE_PREF_POSITION = 3
const EXPLOSIVE_PREF_ROTATION = 4
const EXPLOSIVE_PREF_TYPE = 5

# const player hitbox

const HB_LT = 0
const HB_RT = 1
const HB_LB = 2
const HB_RB = 3
const HB_CE = 4
const SHIELD = 5

# const backend

const BULLET_PATH = "res://assets/scenes/proj/bullet.tscn"
const LASER_PATH = "res://assets/scenes/proj/laser.tscn"
const EXPLOSIVE_PATH = "res://assets/scenes/proj/explosive.tscn"
const PROJ_SCN_PATH = "/root/game/proj"
const RAY_EXCEPTIONS_PATHS = (["..","../hitbox/lt", "../hitbox/rt",
"../hitbox/lb", "../hitbox/rb",
"../hitbox/c", "../hitbox/explosion_collider",
"../shields/shield_1", "../shields/shield_2"])
const RAY_MAX_DISTANCE = 16384
const DRILL_MAX_DISTANCE = 64

const WTYPE_NO_BOOST = 0
const WTYPE_BOOST = 1
const WTYPE_SHOTGUN = 2
const WTYPE_SNIPEGUN = 3
const WTYPE_ROCKET = 4
const WTYPE_LASER = 5

# vars frontend

var WTYPE = WTYPE_NO_BOOST
var SECOND_BASE = false
var GRENADE_LAUNCHER = false
var GUN_LVL = 1
var CLIP_LVL = 0

var DMG = 0
var SHIELD_DMG = 0
var SPREAD = 0
var PENETRATE = false
var BULLETS = 1
var BULLETS_DELAY = 0.0
var ROCKET_RAD = 0

var MAX_AMMO = 0
var AMMO_SIZE = 5
var RELOAD = 0.5
var RELOAD_MULT = 1.0

var MAIN_RECOIL = 0.0
var MAIN_ANG = 20

var GRENADE_ANG = 25
var GRENADE_DMG = 0
var GRENADE_RAD = 0

var DRILL_RECOIL = 0.2
var DRILL_ANG = 80
var DRILL_ATTEMPTS = 24

var DRILL_DMG = 1
var DRILL_BLOCK_DMG = 5

var ROCKET_FORCE = 0
var GRENADE_FORCE = 0

var FIREARM_AMMO_SIZE = 5
var SNIPEGUN_AMMO_SIZE = 1
var SHOTGUN_AMMO_SIZE = 5
var ROCKET_AMMO_SIZE = 0.5
var LASER_AMMO_SIZE = 10

var FIREARM_RECOIL = 0.4
var SNIPEGUN_RECOIL = 1.1
var SHOTGUN_RECOIL = 1.6
var ROCKET_RECOIL = 1.0
var LASER_RECOIL = 0.1
var GRENADE_RECOIL = 1.6

var FIREARM_RELOAD = 0.5
var SNIPEGUN_RELOAD = 1
var SHOTGUN_RELOAD = 0.5
var ROCKET_RELOAD = 1
var LASER_RELOAD = 0.25

var FIREARM_ANGLE = 20
var SNIPEGUN_ANGLE = 60
var SHOTGUN_ANGLE = 45
var ROCKET_ANGLE = 35
var GRENADE_ANGLE = 25
var LASER_ANGLE = 10

# vars backend

var default_visual_pos
var default_bolt_pos
var last_slave_data = {}
var orientation = false
var prev_orientation = false
var active_shooters = []
var barrels = []
var shoot_timer = 0.0
var drill_timer = 0.0
var grenade_timer = 0.0
var reload_timer = 0.0
var drill_active = false
var ammo = MAX_AMMO*AMMO_SIZE
var no_recoil = true
onready var grenade_shooter = $shooters/grenade_shooter
onready var drill_shooter = $shooters/drill_shooter

onready var length_visual = [$visual/base, $visual/base_s, $visual/re_boost, $visual/boost, $visual/barrel]

func _process(delta):
	
	if $"..".is_master:
		
		var can_regen = true

		if Input.is_action_pressed("drill"):
			
			drill_active = true
			
			$visual.hide()
			$drill.show()
			if drill_timer == 0:
				for i in range(DRILL_ATTEMPTS):
					drill_shooter.get_node("ray").rotation_degrees = (DRILL_ANG * 1.0/(i+1)) - DRILL_ANG/2
					drill_shooter.get_node("ray").force_raycast_update()
					if drill_shooter.get_node("ray").is_colliding():
						var ray = drill_shooter.get_node("ray")
						var collider = ray.get_collider()
						process_collider(collider, DRILL_DMG, DRILL_BLOCK_DMG)
						drill_timer = DRILL_RECOIL
			
		else:
			
			drill_active = false
			
			$visual.show()
			$drill.hide()
			
			if Input.is_action_pressed("grenade"):
				if grenade_timer == 0 and GRENADE_LAUNCHER:
					
					grenade_shoot(GRENADE_DMG, GRENADE_RAD, GRENADE_FORCE)
					
					grenade_timer = GRENADE_RECOIL
				
			if Input.is_action_pressed("shoot"):
				if shoot_timer == 0 and ammo > 0:
					
					#laser_shoot(4)
					match WTYPE:
						
						WTYPE_LASER:
							
							laser_shoot(DMG, SHIELD_DMG)
							
						WTYPE_ROCKET:
							
							rocket_shoot(DMG, ROCKET_RAD, ROCKET_FORCE)
							
						_:
							
							firearm_shoot(DMG, SPREAD, PENETRATE, BULLETS, BULLETS_DELAY)
							
					can_regen = false
					reload_timer = RELOAD*RELOAD_MULT
					$"..".reset_stealth()
					ammo-=1
					
					shoot_timer = MAIN_RECOIL
			
		if $"..".aim:
			$aim_line.show()
			
			var start = Vector2(0,0)
			var end = Vector2(RAY_MAX_DISTANCE,0)
			
			if $aim.is_colliding():
				end = $aim.position + Vector2($aim.global_position.distance_to($aim.get_collision_point()),-$aim_line.position.y)
			
			$aim_line.points = [start, end - Vector2($aim_line.position.x,0)]
			
		else:
			$aim_line.hide()
		
		if drill_timer > 0:
			drill_timer -= delta
			if drill_timer < 0:
				drill_timer = 0
		
		if shoot_timer > 0:
			shoot_timer -= delta
			if shoot_timer < 0:
				shoot_timer = 0

		if grenade_timer > 0:
			grenade_timer -= delta
			if grenade_timer < 0:
				grenade_timer = 0
		
		if shoot_timer == 0 and can_regen:
			if reload_timer > 0:
				reload_timer -= delta
				if reload_timer < 0:
					no_recoil = true
					if ammo < MAX_AMMO*AMMO_SIZE:
						ammo += 1
						reload_timer = RELOAD*RELOAD_MULT
						# send ammo
						var data = [UN_WEAPON_DATA, WEAPON_DATA_SHOOT, {SHOOT_AMMO : ammo}]
						if get_tree().multiplayer.has_network_peer():
							get_tree().multiplayer.send_bytes(var2bytes(data), 1, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)
	
	else:
		
		# huge workaround (why this is happening???)
		if $"/root/game".players.has(int($"..".name)):
			if $"/root/game".players[int($"..".name)].mech != null:
				if $"/root/game".players[int($"..".name)].mech.weapon.shoot != last_slave_data:
					last_slave_data = $"/root/game".players[int($"..".name)].mech.weapon.shoot
					slave_shoot()
				if $"/root/game".players[int($"..".name)].mech.drill:
					$visual.hide()
					$drill.show()
				else:
					$visual.show()
					$drill.hide()
				
				if $"/root/game".players[int($"..".name)].mech.aim:
					$aim_line.show()
					
					var start = Vector2(0,0)
					var end = Vector2(RAY_MAX_DISTANCE,0)
					
					if $aim.is_colliding():
						end = $aim.position + Vector2($aim.global_position.distance_to($aim.get_collision_point()),-$aim_line.position.y)
					
					$aim_line.points = [start, end - Vector2($aim_line.position.x,0)]
					
				else:
					$aim_line.hide()
	
	if WTYPE == WTYPE_LASER:
		
		$visual/clip/laser_bar.scale.y = float(ammo)/(MAX_AMMO*AMMO_SIZE) * LASER_CLIP_BAR_SIZES[CLIP_LVL]
		$visual/clip_s/laser_bar.scale.y = $visual/clip/laser_bar.scale.y
		
		$visual/base/laser_bar.scale.y = float(ammo)/(MAX_AMMO*AMMO_SIZE) * LASER_BASE_BAR_SIZES_X_OFFSETS[GUN_LVL][0]
		$visual/base_s/laser_bar.scale.y = $visual/base/laser_bar.scale.y
		
	if prev_orientation != orientation:
		for shooter in active_shooters:
			shooter.position.y *= -1
		grenade_shooter.position.y *= -1
		$drill.scale.y *= -1
		for proj in $proj.get_children():
			proj.out.y *= -1
		$visual.scale.y *= -1
		$aim_line.position.y *= -1
		prev_orientation = orientation

func grenade_shoot(dmg, radius, force):
	
	var to_send_visuals = {}
	
	var shooter = grenade_shooter
	
	var grenade_obj = load(EXPLOSIVE_PATH).instance()
	var out = shooter.get_node("out")
	var ray = shooter.get_node("ray")
	
	grenade_obj.is_master = $"..".is_master
	grenade_obj.radius = radius
	grenade_obj.dmg = dmg
	grenade_obj.gravity_scale = 2.0
	grenade_obj.linear_velocity = Vector2(force,0).rotated(ray.global_rotation)
	if !shooter.get_node("checker").is_colliding(): 
		grenade_obj.position = out.global_position
	else:
		grenade_obj.position = shooter.get_node("checker").get_collision_point()
	grenade_obj.rotation = ray.global_rotation
	grenade_obj.type = TYPE_GRENADE
	grenade_obj.add_collision_exception_with($"../hitbox/explosion_collider")
	
	to_send_visuals[0] = {
		EXPLOSIVE_PREF_RADIUS : grenade_obj.radius,
		EXPLOSIVE_PREF_GRAVITY : grenade_obj.gravity_scale,
		EXPLOSIVE_PREF_VELOCITY : grenade_obj.linear_velocity,
		EXPLOSIVE_PREF_POSITION : grenade_obj.position,
		EXPLOSIVE_PREF_ROTATION : grenade_obj.rotation,
		EXPLOSIVE_PREF_TYPE : grenade_obj.type
	}
	
	animate(SHOOT_TYPE_GRENADE)
	
	get_node(PROJ_SCN_PATH).add_child(grenade_obj)
	
	var to_send = {
		SHOOT_TYPE : SHOOT_TYPE_GRENADE,
		SHOOT_PREF : to_send_visuals
	}
	# send data
	var data = [UN_WEAPON_DATA, WEAPON_DATA_SHOOT, to_send]
	if get_tree().multiplayer.has_network_peer():
		get_tree().multiplayer.send_bytes(var2bytes(data), 1, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)


func rocket_shoot(dmg, radius, force):
	
	var to_send_visuals = {}
	
	var shooter = active_shooters[0]
	
	var rocket_obj = load(EXPLOSIVE_PATH).instance()
	var out = shooter.get_node("out")
	var ray = shooter.get_node("ray")
	
	rocket_obj.is_master = $"..".is_master
	rocket_obj.radius = radius
	rocket_obj.dmg = dmg
	rocket_obj.gravity_scale = 0.1
	rocket_obj.linear_velocity = Vector2(force,0).rotated(ray.global_rotation)
	if !shooter.get_node("checker").is_colliding(): 
		rocket_obj.position = out.global_position
	else:
		rocket_obj.position = shooter.get_node("checker").get_collision_point()
	rocket_obj.rotation = ray.global_rotation
	rocket_obj.type = TYPE_ROCKET
	rocket_obj.add_collision_exception_with($"../hitbox/explosion_collider")
	
	to_send_visuals[0] = {
		EXPLOSIVE_PREF_RADIUS : rocket_obj.radius,
		EXPLOSIVE_PREF_GRAVITY : rocket_obj.gravity_scale,
		EXPLOSIVE_PREF_VELOCITY : rocket_obj.linear_velocity,
		EXPLOSIVE_PREF_POSITION : rocket_obj.position,
		EXPLOSIVE_PREF_ROTATION : rocket_obj.rotation,
		EXPLOSIVE_PREF_TYPE : rocket_obj.type
	}
	
	animate(SHOOT_TYPE_EXPLOSIVE)
	
	get_node(PROJ_SCN_PATH).add_child(rocket_obj)
	
	var to_send = {
		SHOOT_TYPE : SHOOT_TYPE_EXPLOSIVE,
		SHOOT_PREF : to_send_visuals,
		SHOOT_AMMO : ammo
	}
	# send data
	var data = [UN_WEAPON_DATA, WEAPON_DATA_SHOOT, to_send]
	if get_tree().multiplayer.has_network_peer():
		get_tree().multiplayer.send_bytes(var2bytes(data), 1, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)

func laser_shoot(dmg, shield_dmg):
	
	var to_send_visuals = {}
	
	for shooter in active_shooters:
		# initial setup
		var laser_obj = null
		var out = shooter.get_node("out")
		var ray = shooter.get_node("ray")
		var col_point = Vector2(0,0)
		var target
		var target_part
		
		# if we don't have laser visual - load it
		if !$proj.has_node("laser_bullet_"+str(active_shooters.find(shooter))):
			laser_obj = load(LASER_PATH).instance()
			laser_obj.name = "laser_bullet_"+str(active_shooters.find(shooter))
			laser_obj.ray = ray
			laser_obj.out = out.position + shooter.position + $shooters.position
			laser_obj.alive_time = MAIN_RECOIL
			
			match $"/root/game".players[int($"..".name)].team:
				
				0: laser_obj.default_color = TEAM_RED_COLOR.lightened(0.2)
				1: laser_obj.default_color = TEAM_BLU_COLOR.lightened(0.2)
			
			$proj.add_child(laser_obj)
			to_send_visuals[active_shooters.find(shooter)] = {
				LASER_PREF_RAY_PATH : get_path_to(ray),
				LASER_PREF_OUT : laser_obj.out,
				LASER_PREF_ALIVE_TIME : MAIN_RECOIL
			}
		else:
			$proj.get_node("laser_bullet_"+str(active_shooters.find(shooter))).alive_time = MAIN_RECOIL
			$proj.get_node("laser_bullet_"+str(active_shooters.find(shooter))).width = 5
			to_send_visuals[active_shooters.find(shooter)] = {
				LASER_PREF_ALIVE_TIME : MAIN_RECOIL
			}
		
		while true:
			# finding collisions
			if ray.is_colliding():
				# get col point
				col_point = ray.get_collision_point()
				
				# get target name and if possible hitbox id
				
				if is_instance_valid(ray.get_collider()):
					target = ray.get_collider().get_node("../..").name
					target_part = match_target_part(ray.get_collider().name)
				
				# process target exceptions 
				match target:
					
					"root":
						
						if target_part == "tilemap":
							# process tilemap hit request to server
							var tilemap = ray.get_collider()
							var cell = tilemap.world_to_map(col_point + Vector2(2,0).rotated(ray.global_rotation))
							var cell_id = tilemap.get_cellv(cell)
							# if not (-1 (unexpected) or HARD CELL) - send data
							if !(cell_id in HARD_CELLS or cell_id == -1):
								
								$"/root/game".peer.put_var(
								[TILEMAP_HIT, cell, dmg]
								)
					
					"game":
						
						if target_part is String:
							if target_part.begins_with("MEGUMIN"):
								ray.get_collider().blow_up()
					
					
					"points":
						# process point hit request to server
						var point = ray.get_collider().get_node("..")
						if point.hp > 0:
							$"/root/game".peer.put_var(
							[POINT, POINT_HIT, dmg, int(point.name)]
							)
					
					_: 
					
						# process hit if target looks like player id
						target = int(target)
						
						if $"/root/game".players.has(target):
							
							# if smth unexpected - prevent crash
							if $"/root/game".players[target].mech == null: break
							
							if target_part == SHIELD:
								if $"..".mech_team == $"/root/game".players[target].team:
									ray.add_exception(ray.get_collider())
									ray.force_raycast_update()
									laser_obj.exceptions = []
									continue
								else:
									$"/root/game".peer.put_var([HIT,target,target_part,shield_dmg])
									
							else:
								$"/root/game".peer.put_var([HIT,target,target_part,dmg])
								
			
			break
	
	# animate
	animate(SHOOT_TYPE_LASER)
	
	# prepare send data
	var to_send = {
		SHOOT_TYPE : SHOOT_TYPE_LASER,
		SHOOT_PREF : to_send_visuals,
		SHOOT_AMMO : ammo
	}
	# send data
	var data = [UN_WEAPON_DATA ,WEAPON_DATA_SHOOT, to_send]
	if get_tree().multiplayer.has_network_peer():
		get_tree().multiplayer.send_bytes(var2bytes(data), 1, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)

func firearm_shoot(dmg, spread, penetrate=false, bullets=1, mult_bullets_delay=0.0):
	
	var to_send_visuals = {}
	var b_count = 0
	
	# call animation
	animate(SHOOT_TYPE_FIREARM)
	
	for bullet in range(bullets):
		if !no_recoil:
			if spread != 0:
				$shooters.rotation_degrees = randi()%int(spread) - int(spread)/2
			else:
				$shooters.rotation_degrees = 0
		else: $shooters.rotation_degrees = 0
		no_recoil = false
		$visual.rotation_degrees = $shooters.rotation_degrees
		for shooter in active_shooters:
			# initial setup
			var bullet_obj = load(BULLET_PATH).instance()
			var out = shooter.get_node("out")
			var ray = shooter.get_node("ray")
			var col_point
			var target
			var target_part
			var temp_dmg = dmg
			
			var exceptions = []
			var removed_tiles = {}
			var tilemap = null
			
			# finding collisions 
			while true:
				
				ray.force_raycast_update()
				
				col_point = (ray.global_position + 
				Vector2(RAY_MAX_DISTANCE,0).rotated(
				ray.global_rotation))
				
				var cont = penetrate
				
				if ray.is_colliding():
					# get col point
					col_point = ray.get_collision_point()
					
					# get target name and if possible hitbox id
					
					if is_instance_valid(ray.get_collider()):
						target = ray.get_collider().get_node("../..").name
						target_part = match_target_part(ray.get_collider().name)
					
					# if penetrate - add collider to exceptions
					if penetrate:
						if ray.get_collider().name != "tilemap":
							exceptions.append(ray.get_collider())
							ray.add_exception(ray.get_collider())
					
					# process target exceptions 
					match target:
						
						"root":
							
							if target_part == "tilemap":
								# process tilemap hit request to server
								tilemap = ray.get_collider()
								var cell = tilemap.world_to_map(col_point + Vector2(2,0).rotated(ray.global_rotation))
								var cell_id = tilemap.get_cellv(cell)
								# break if -1 (unexpected) or HARD CELL
								if cell_id in HARD_CELLS or cell_id == -1:
									break
								
								$"/root/game".peer.put_var(
								[TILEMAP_HIT, cell, temp_dmg]
								)
								# subtract impact from dmg if penetrate
								if $"/root/game".tilemap_hit.keys().has(cell):
									temp_dmg -= $"/root/game".tilemap_hit[cell]
								else:
									temp_dmg -= TILEMAP_HP
								
								removed_tiles[cell] = tilemap.get_cellv(cell)
								tilemap.set_cellv(cell,-1)
								tilemap.update_dirty_quadrants()
							
							else:
								break
						
						"points":
							# process point hit request to server
							var point = ray.get_collider().get_node("..")
							if point.hp > 0:
								$"/root/game".peer.put_var(
								[POINT, POINT_HIT, temp_dmg, int(point.name)]
								)
								temp_dmg -= point.hp
						
						"game":
							
							if target_part is String:
								if target_part.begins_with("MEGUMIN"):
									ray.get_collider().blow_up()
								elif int(target_part) != 0: break
						
						_: 
						
							# process hit if target looks like player id
							target = int(target)
							
							if $"/root/game".players.has(target):
								
								# if smth unexpected - prevent crash
								if $"/root/game".players[target].mech == null: break
								
								if target_part == SHIELD:
									if $"..".mech_team == $"/root/game".players[target].team:
										exceptions.append(ray.get_collider())
										ray.add_exception(ray.get_collider())
										shooter.get_node("checker").add_exception(ray.get_collider())
										shooter.get_node("checker").force_raycast_update()
										cont = true
										continue
									else:
										$"/root/game".peer.put_var([HIT,target,target_part,temp_dmg])
										break
								else:
									$"/root/game".peer.put_var([HIT,target,target_part,temp_dmg])
								
								temp_dmg -= $"/root/game".players[target].mech.hitbox[target_part]
								
							else: break
					
				else: break
				
				if !cont or temp_dmg < 1: break
				
			# processing visuals
			bullet_obj.points[0] = out.global_position
			bullet_obj.points[1] = col_point
			bullet_obj.width += dmg*0.025
			
			# draw only if barrel didn't blocked by something
			if !shooter.get_node("checker").is_colliding(): 
				to_send_visuals[b_count] = bullet_obj.points
				get_node(PROJ_SCN_PATH).add_child(bullet_obj)
				b_count += 1
			
			# taking all back
			for ex in exceptions:
				ray.remove_exception(ex)
				shooter.get_node("checker").remove_exception(ex)
			
			if tilemap != null:
				for cell in removed_tiles:
					tilemap.set_cellv(cell,removed_tiles[cell])
		
		if mult_bullets_delay > 0:
			yield(get_tree().create_timer(mult_bullets_delay),"timeout")
		
	# prepare send data
	var to_send = {
		SHOOT_TYPE : SHOOT_TYPE_FIREARM,
		SHOOT_PREF : to_send_visuals,
		SHOOT_AMMO : ammo
	}
	# send data
	var data = [UN_WEAPON_DATA, WEAPON_DATA_SHOOT, to_send]
	if get_tree().multiplayer.has_network_peer():
		get_tree().multiplayer.send_bytes(var2bytes(data), 1, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)

func slave_shoot():
	
	var type = last_slave_data.get(SHOOT_TYPE,-1)
	var pref = last_slave_data.get(SHOOT_PREF,{})
	ammo = last_slave_data.get(SHOOT_AMMO,MAX_AMMO * AMMO_SIZE)
	
	animate(type)
	
	match type:
		
		SHOOT_TYPE_FIREARM:
			
			for b in pref.keys():
				var bullet_obj = load(BULLET_PATH).instance()
				bullet_obj.points = pref[b]
				get_node(PROJ_SCN_PATH).add_child(bullet_obj)
			
		SHOOT_TYPE_LASER:
			
			for l in pref.keys():
				# if we don't have laser visual - load it
				if pref[l].keys().size() > 1:
					if $proj.has_node("laser_bullet_"+str(l)): 
						$proj.get_node("laser_bullet_"+str(l)).queue_free()
					var laser_obj = load(LASER_PATH).instance()
					laser_obj.name = "laser_bullet_"+str(l)
					laser_obj.ray = get_node(pref[l][LASER_PREF_RAY_PATH])
					laser_obj.out = pref[l][LASER_PREF_OUT]
					laser_obj.alive_time = pref[l][LASER_PREF_ALIVE_TIME]
					
					match $"/root/game".players[int($"..".name)].team:
						
						0: laser_obj.default_color = TEAM_RED_COLOR.lightened(0.2)
						1: laser_obj.default_color = TEAM_BLU_COLOR.lightened(0.2)
					
					$proj.add_child(laser_obj)
				elif $proj.has_node("laser_bullet_"+str(l)):
					$proj.get_node("laser_bullet_"+str(l)).alive_time = pref[l][LASER_PREF_ALIVE_TIME]
					$proj.get_node("laser_bullet_"+str(l)).width = 6
		
		SHOOT_TYPE_EXPLOSIVE, SHOOT_TYPE_GRENADE:
			
			var e = pref[0]
			
			var explosive_obj = load(EXPLOSIVE_PATH).instance()
			explosive_obj.radius = e[EXPLOSIVE_PREF_RADIUS]
			explosive_obj.gravity_scale = e[EXPLOSIVE_PREF_GRAVITY]
			explosive_obj.linear_velocity = e[EXPLOSIVE_PREF_VELOCITY]
			explosive_obj.position = e[EXPLOSIVE_PREF_POSITION]
			explosive_obj.rotation = e[EXPLOSIVE_PREF_ROTATION]
			explosive_obj.type = e[EXPLOSIVE_PREF_TYPE]
			explosive_obj.add_collision_exception_with($"../hitbox/explosion_collider")
			
			get_node(PROJ_SCN_PATH).add_child(explosive_obj)

func animate(type):
	
	match type:
		
		SHOOT_TYPE_FIREARM:
			
			yield(get_tree(),"idle_frame")
			
			$visual.rotation_degrees = -MAIN_ANG * $visual.scale.y
			$visual.position.x = 0
			
			$tw.interpolate_property($visual,"rotation",$visual.rotation,0,MAIN_RECOIL,Tween.TRANS_CUBIC,Tween.EASE_OUT)
			$tw.interpolate_property($visual,"position",$visual.position,default_visual_pos,MAIN_RECOIL,Tween.TRANS_CUBIC,Tween.EASE_OUT)
			
			if $visual/bolt.texture != null:
				$visual/bolt.position.x -= ($visual/bolt.texture.get_width() - BOLT_X_OFFSET)
				$tw.interpolate_property($visual/bolt,"position",$visual/bolt.position,default_bolt_pos,MAIN_RECOIL,Tween.TRANS_CUBIC,Tween.EASE_OUT)
			
			$tw.start()
			
		SHOOT_TYPE_LASER:
			
			pass
			#$visual.rotation_degrees = randi()%MAIN_ANG - MAIN_ANG/2
			#$visual.position.x = 0
			
			#$tw.interpolate_property($visual,"rotation",$visual.rotation,0,MAIN_RECOIL,Tween.TRANS_CUBIC,Tween.EASE_OUT)
			#$tw.interpolate_property($visual,"position",$visual.position,default_visual_pos,MAIN_RECOIL*4,Tween.TRANS_CUBIC,Tween.EASE_OUT)
			
			#$tw.start()
			
		SHOOT_TYPE_EXPLOSIVE:
			
			yield(get_tree(),"idle_frame")
			
			$visual.rotation_degrees = -MAIN_ANG * $visual.scale.y
			$visual.position.x = 0
			
			$tw.interpolate_property($visual,"rotation",$visual.rotation,0,MAIN_RECOIL,Tween.TRANS_CUBIC,Tween.EASE_OUT)
			$tw.interpolate_property($visual,"position",$visual.position,default_visual_pos,MAIN_RECOIL,Tween.TRANS_CUBIC,Tween.EASE_OUT)
			
			$tw.start()

		SHOOT_TYPE_GRENADE:

			yield(get_tree(),"idle_frame")
				
			$visual.rotation_degrees = -GRENADE_ANG * $visual.scale.y
			$visual.position.x = 0
			
			$tw.interpolate_property($visual,"rotation",$visual.rotation,0,GRENADE_RECOIL,Tween.TRANS_CUBIC,Tween.EASE_OUT)
			$tw.interpolate_property($visual,"position",$visual.position,default_visual_pos,GRENADE_RECOIL,Tween.TRANS_CUBIC,Tween.EASE_OUT)
			
			$tw.start()

			

func process_collider(obj, dmg, block_dmg):
	
	var target = null
	var target_part = null
	var ray = drill_shooter.get_node("ray")
	
	var col_point = (ray.global_position + 
	Vector2(DRILL_MAX_DISTANCE,0).rotated(
	ray.global_rotation))
	
	if ray.is_colliding():
		col_point = ray.get_collision_point()
	
	if is_instance_valid(obj):
		target = obj.get_node("../..").name
		target_part = match_target_part(obj.name)
	 
	match target:
		
		"root":
			
			if target_part == "tilemap":
				# process tilemap hit request to server
				var tilemap = obj
				var cell = tilemap.world_to_map(col_point + Vector2(2,0).rotated(ray.global_rotation))
				var cell_id = tilemap.get_cellv(cell)
				# break if -1 (unexpected) or HARD CELL
				if cell_id in HARD_CELLS or cell_id == -1:
					return
				
				$"/root/game".peer.put_var(
				[TILEMAP_HIT, cell, block_dmg]
				)
		
		"game":
			
			if target_part is String:
				if target_part.begins_with("MEGUMIN"):
					obj.blow_up()
		
		"points":
			# process point hit request to server
			var point = obj.get_node("..")
			if point.hp > 0:
				$"/root/game".peer.put_var(
				[POINT, POINT_HIT, dmg, int(point.name)]
				)
		
		_: 
		
			# process hit if target looks like player id
			target = int(target)
			
			if $"/root/game".players.has(target):
				
				# if smth unexpected - prevent crash
				if $"/root/game".players[target].mech == null: return
			
				$"/root/game".peer.put_var([HIT,target,target_part,dmg])
				

func match_target_part(n):
	var result
	
	match n:
		"lt": result = HB_LT
		"rt": result = HB_RT
		"lb": result = HB_LB
		"rb": result = HB_RB
		"c": result = HB_CE
		_: result = n
	
	if n.begins_with("shield"):
		result = SHIELD
	
	return result

func update_visual():
	var length_sum = 0
	for node in length_visual:
		if node.texture == null: continue
		# process offsets
		var even_offset_x = 1-node.texture.get_width()%2
		var even_offset_y = node.texture.get_height()%2
		var offset = VISUAL_X_OFFSETS.get(length_visual.find(node),0)
		# place
		node.position.x = length_sum + node.texture.get_width()/2 - offset - even_offset_x
		node.position.y += even_offset_y
		length_sum += node.texture.get_width() - offset
	# process shooters
	for y in barrels:
		active_shooters[barrels.find(y)].position.y = y
		active_shooters[barrels.find(y)].get_node("out").position.x = length_sum - 1
		if WTYPE == WTYPE_ROCKET:
			active_shooters[barrels.find(y)].get_node("out").position.x -= $visual/barrel.texture.get_width()
	# count bolt default position
	if $visual/bolt.texture != null:
		$visual/bolt.position.x = ($visual/bolt.texture.get_width() - BOLT_X_OFFSET) / 2
	# set default positions
	default_visual_pos = $visual.position
	default_bolt_pos = $visual/bolt.position
	
	$visual/clip/laser_bar.hide()
	$visual/clip_s/laser_bar.hide()
	$visual/base/laser_bar.hide()
	$visual/base_s/laser_bar.hide()
	
	if GRENADE_LAUNCHER:
		
		if $visual/re_boost.texture != null:
			
			if WTYPE == WTYPE_LASER:
				
				$visual/grenade_launcher.position.x = $visual/re_boost.position.x + $visual/re_boost.texture.get_width()/2 - GRENADE_LAUNCHER_X_OFFSET
				$visual/grenade_launcher.position.y = $visual/re_boost.texture.get_height() - GRENADE_LAUNCHER_RE_BOOST_Y_OFFSET
			
			else:
				
				var y_offset = GRENADE_LAUNCHER_RE_BOOST_Y_OFFSET
				if $visual/base_s.texture != null: y_offset = GRENADE_LAUNCHER_RE_BOOST_S_Y_OFFSET
				
				$visual/grenade_launcher.position.x = $visual/re_boost.position.x + $visual/re_boost.texture.get_width()/2 - GRENADE_LAUNCHER_X_OFFSET
				$visual/grenade_launcher.position.y = $visual/re_boost.texture.get_height() - y_offset
			
		else:
			
			if $visual/base_s.texture != null:
				
				$visual/grenade_launcher.position.x = $visual/base_s.position.x + $visual/base_s.texture.get_width()/2 - GRENADE_LAUNCHER_X_OFFSET
				$visual/grenade_launcher.position.y = $visual/base_s.texture.get_height() + GRENADE_LAUNCHER_BASE_S_Y_OFFSET
				
			else:
				
				$visual/grenade_launcher.position.x = $visual/base.position.x + $visual/base.texture.get_width()/2 - GRENADE_LAUNCHER_X_OFFSET
				$visual/grenade_launcher.position.y = $visual/base.texture.get_height() - GRENADE_LAUNCHER_Y_OFFSET
		
		if WTYPE == WTYPE_ROCKET:
				
			$visual/grenade_launcher.position.x = $visual/base.position.x + $visual/base.texture.get_width()/2 - GRENADE_LAUNCHER_X_OFFSET
			$visual/grenade_launcher.position.y = $visual/base.texture.get_height() - GRENADE_LAUNCHER_ROCKET_Y_OFFSET
				
	
	match WTYPE:
		
		WTYPE_LASER:
			
			$visual/clip.position.x = $visual/base.position.x
			$visual/clip_s.position.x = $visual/base_s.position.x - LASER_CLIP_S_X_OFFSET
			
			$visual/clip.position.y = - $visual/clip.texture.get_height() + LASER_CLIP_Y_OFFSET
			$visual/clip_s.position.y = $visual/clip.position.y
			
			if $visual/clip_s.texture != null: $visual/clip_s/laser_bar.show()
			if $visual/base_s.texture != null: $visual/base_s/laser_bar.show()
			$visual/clip/laser_bar.show()
			$visual/base/laser_bar.show()
			
			$visual/base/laser_bar.position.x = LASER_BASE_BAR_SIZES_X_OFFSETS[GUN_LVL][1]
			$visual/base_s/laser_bar.position.x = $visual/base/laser_bar.position.x - LASER_BASE_S_BAR_X_OFFSET
			if $visual/re_boost.texture == null: $visual/grenade_launcher.position.y -= GRENADE_LAUNCHER_Y_OFFSET
			
		WTYPE_ROCKET:
			
			# clip
			$visual/clip.position.x = $visual/base.position.x + ROCKET_CLIP_X_OFFSET
			$visual/clip.position.y = - $visual/base.texture.get_height()/2 - $visual/clip.texture.get_height()/2 + ROCKET_CLIP_Y_OFFSET
			# barrel
			$visual/barrel.position -= Vector2(1,-1)
			
		_:
			
			var even_offset_y = 1-$visual/clip.texture.get_height()%2
			
			$visual/clip.position.x = $visual/base.position.x + CLIP_X_OFFSET
			$visual/clip_s.position.x = $visual/base_s.position.x - CLIP_X_OFFSET
			
			$visual/clip.position.y = $visual/base.texture.get_height()/2 + $visual/clip.texture.get_height()/2 - even_offset_y
			$visual/clip_s.position.y = $visual/clip.position.y
		
		
		
func update_shooters():
	for shooter in $shooters.get_children():
		var out = shooter.get_node("out")
		var ray = shooter.get_node("ray")
		var checker = shooter.get_node("checker")
		
		ray.cast_to.x = RAY_MAX_DISTANCE
		checker.cast_to.x = out.position.x
		
		for ex in RAY_EXCEPTIONS_PATHS:
			ray.add_exception(get_node(ex))
			checker.add_exception(get_node(ex))
		ray.force_raycast_update()
	
	drill_shooter.get_node("ray").cast_to.x = DRILL_MAX_DISTANCE

func _ready():
	randomize()
	set_process(false)
	$aim.add_exception($"../hitbox/explosion_collider")
	
	if $"..".name != "root":
		
		$"../modules".connect("end",self,"end_init")
		
	else:
		
		var c = Camera2D.new()
		c.zoom /= 10
		c.current = true
		c.position.x += 64
		add_child(c)
		
		if SECOND_BASE:
			barrels.append(-2)
			barrels.append(3)
			active_shooters.append($shooters/shooter)
			active_shooters.append($shooters/second_shooter)
			$visual/clip_s.texture = $visual/clip.texture
		else:
			barrels.append(0)
			active_shooters.append($shooters/shooter)
		
		update_visual()
		update_shooters()
		
		$drill.hide()
		

func end_init():
	
	if SECOND_BASE:
		barrels.append(-2)
		barrels.append(3)
		active_shooters.append($shooters/shooter)
		active_shooters.append($shooters/second_shooter)
		$visual/clip_s.texture = $visual/clip.texture
	else:
		barrels.append(0)
		active_shooters.append($shooters/shooter)
	
	update_visual()
	update_shooters()
	
	match $"/root/game".players[int($"..".name)].team:
		
		0: 
			$visual/clip/laser_bar.modulate = TEAM_RED_COLOR.lightened(0.2)
			$visual/clip_s/laser_bar.modulate = TEAM_RED_COLOR.lightened(0.2)
			$visual/base/laser_bar.modulate = TEAM_RED_COLOR.lightened(0.2)
			$visual/base_s/laser_bar.modulate = TEAM_RED_COLOR.lightened(0.2)
		1: 
			$visual/clip/laser_bar.modulate = TEAM_BLU_COLOR.lightened(0.2)
			$visual/clip_s/laser_bar.modulate = TEAM_BLU_COLOR.lightened(0.2)
			$visual/base/laser_bar.modulate = TEAM_BLU_COLOR.lightened(0.2)
			$visual/base_s/laser_bar.modulate = TEAM_BLU_COLOR.lightened(0.2)
	
	match WTYPE:
		
		WTYPE_BOOST, WTYPE_NO_BOOST:
			
			AMMO_SIZE = FIREARM_AMMO_SIZE
			if MAIN_RECOIL == 0.0: MAIN_RECOIL = FIREARM_RECOIL
			RELOAD = FIREARM_RELOAD
			MAIN_ANG = FIREARM_ANGLE
			
		WTYPE_SNIPEGUN:
			
			AMMO_SIZE = SNIPEGUN_AMMO_SIZE
			if MAIN_RECOIL == 0.0: MAIN_RECOIL = SNIPEGUN_RECOIL
			RELOAD = SNIPEGUN_RELOAD
			MAIN_ANG = SNIPEGUN_ANGLE
			
		WTYPE_SHOTGUN:
			
			AMMO_SIZE = SHOTGUN_AMMO_SIZE
			if MAIN_RECOIL == 0.0: MAIN_RECOIL = SHOTGUN_RECOIL
			RELOAD = SHOTGUN_RELOAD
			MAIN_ANG = SHOTGUN_RELOAD
			
		WTYPE_ROCKET:
			
			AMMO_SIZE = ROCKET_AMMO_SIZE
			if MAIN_RECOIL == 0.0: MAIN_RECOIL = ROCKET_RECOIL
			RELOAD = ROCKET_RELOAD
			MAIN_ANG = ROCKET_ANGLE
			
		WTYPE_LASER:
			
			AMMO_SIZE = LASER_AMMO_SIZE
			if MAIN_RECOIL == 0.0: MAIN_RECOIL = LASER_RECOIL
			RELOAD = LASER_RELOAD
			MAIN_ANG = LASER_ANGLE
	
	ammo = MAX_AMMO * AMMO_SIZE
	
	set_process(true)