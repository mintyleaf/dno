extends Line2D

const ACCELERATION = 200

func _ready():
	var ray = RayCast2D.new()
	ray.cast_to = Vector2(32,0)
	ray.name = "ray"
	ray.enabled = true
	add_child(ray)

func _process(delta):
	position += Vector2(ACCELERATION,0).rotated(rotation)
	points[0] -= Vector2(ACCELERATION/2,0)
	if $ray.is_colliding(): queue_free()