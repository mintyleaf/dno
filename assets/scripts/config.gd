extends Reference

const CONFIG_PATH = "user://dno.cfg"

func new_cfg(c):
	if c.open(CONFIG_PATH, File.WRITE) == OK:
		c.store_string(to_json(state.cfg))
		c.close()
	else:
		print("Can't make config file!")
		c.close()


func write_cfg():
	var c = File.new()
	if c.open(CONFIG_PATH, File.WRITE) == OK:
		c.store_string(to_json(state.cfg))
		c.close()
	else:
		print("Can't write to config file!")
		c.close()


func read_cfg(c):
	var config_file_data = c.get_as_text()
	if validate_json(config_file_data) != "":
		new_cfg(c)
	else:
		config_file_data = parse_json(config_file_data)
		for i in config_file_data.keys():
			if !state.cfg.has(i):
				new_cfg(c)
				return
		
		for i in config_file_data.keys():
			if state.cfg.has(i):
				state.cfg[i] = config_file_data[i]
		
		c.close()


func _init():
	var config_file = File.new()
	if config_file.open(CONFIG_PATH, File.READ) == OK:
		read_cfg(config_file)
	else:
		new_cfg(config_file)