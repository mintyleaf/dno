extends RigidBody2D

const Y_MAX = 5000
const X_MAX = 10000
const TIMEOUT = 5

const TYPE_ROCKET = 0
const TYPE_GRENADE = 1

const TEX_ROCKET = "res://assets/graphics/rocket.png"
const TEX_GRENADE = "res://assets/graphics/grenade.png"
const COL_ROCKET = [2,16]
const COL_GRENADE = [1,5]

var type = 0

var is_master = false
var radius = 0
var dmg = 0

var timer = TIMEOUT

func _ready():
	
	match type:
		
		TYPE_ROCKET:
			$tex.texture = load(TEX_ROCKET)
			$col.shape.radius = COL_ROCKET[0]
			$col.shape.height = COL_ROCKET[1]
			$trail.position.x = -$tex.texture.get_width()/2
			$trail.emitting = true
			
		TYPE_GRENADE:
			$tex.texture = load(TEX_GRENADE)
			$col.shape.radius = COL_GRENADE[0]
			$col.shape.height = COL_GRENADE[1]
			$trail.emitting = false


func explode():
	
	if is_master:
		$"/root/game".process_explode(position, radius, dmg)
	
	$"col".disabled = true
	$"tex".hide()
	set_physics_process(false)
	$"trail".emitting = false
	sleeping = true
	yield(get_tree().create_timer(3), 'timeout')
	queue_free()


func _physics_process(delta):
	
	if timer > 0: timer-=delta
	
	if abs(position.x) > X_MAX or abs(position.y) > Y_MAX:
		explode()
	if get_colliding_bodies().size() > 0 or timer < 0:
		explode()