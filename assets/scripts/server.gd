extends "res://assets/scripts/net_constants.gd"

var tcp_server
var enet_unreliable

var connection = Dictionary()
var connection_by_id = Dictionary()
var client_by_id = Dictionary()
var in_game = []

var error_log = {}
var alive_log = {}

var is_restart = true

class client_data:
	var peer = PacketPeerStream.new()
	var nickname = "UNKNOWN"
	var team = -1
	var cap_weight = 1.0
	var stats = [0,0,0] # K, D, C
	var alive_time = -1
	var seat = null
	var id = -1
	var mech = null
	var modules = {}
	var modules_c = {}
	var modules_order = []
	var mony = 0
	var cred = 0

signal restart
signal reset
signal player_disconnect
signal player_connect
signal other_packet

var closing = false

func _process(delta):
	
	# kicking for bad connection or other issues
	 
	for i in error_log:
		if connection_by_id.has(i):
			if error_log[i] > MAX_ERRORS:
				for j in connection.keys():
					if connection[j] == connection_by_id[i]:
						state.print_log("someone is full of wrong packets!")
						broadcast_disconnect(j)
						error_log.erase(i)
		else:
			error_log.erase(i)
	
	for i in alive_log:
		
		alive_log[i] += delta
		if alive_log[i] > ALIVE_TIMER_TIMEOUT:
			if connection.has(i):
				state.print_log("someone died!")
				alive_log.erase(i)
				broadcast_disconnect(i)
			else:
				alive_log.erase(i)
	
	# taking new connection
	
	if tcp_server.is_connection_available() and connection.size() < MAX_CLIENTS:
		var client = tcp_server.take_connection()
		connection[client] = client_data.new()
		connection[client].peer.set_stream_peer(client)
		state.print_log("Someone connecting...")
	
	# processing clients
	
	for client in connection:
		
		if connection[client].peer.get_available_packet_count() > 0:
			
			for i in range(connection[client].peer.get_available_packet_count()):
				var data = connection[client].peer.get_var()
				
				match data[0]:
					
					PLAYER_CONNECT:
						
						connection[client].id = data[1]
						connection[client].nickname = data[2]
						connection_by_id[connection[client].id] = connection[client]
						client_by_id[connection[client].id] = client
						broadcast_connect(client)
						send_map(client)
						send_loading_end(client)
						alive_log[client] = 0.0
						error_log[connection[client].id] = 0
						
						emit_signal("player_connect",client)
						
						state.print_log(connection[client].nickname + " connected")
						var nicknames = ""
						for c in connection:
							nicknames+=connection[c].nickname + ", "
						state.print_log("Currently on server: " + nicknames.left(nicknames.length()-2))
						
					ALIVE:
						
						alive_log[client] = 0.0
						
					EXIT:
						
						if connection.has(client): 
							broadcast_disconnect(client)
							return
						
					_:
						emit_signal("other_packet",data,client)
		
		# kicking when no connection
		if !client.is_connected_to_host():
			state.print_log(connection[client].nickname + " connection interrupted")
			broadcast_disconnect(client)
			return

func unreliable_packet(id,raw_data):
	
	if closing:
		return
	
	var data = bytes2var(raw_data)
	if !connection_by_id.has(id): 
		state.print_log(str(id) + " - wrong id!")
		error_log[id] += 1
		return
	
	match data[0]:
		
		UN_MECH_DATA:
			
			var acceptable = true
			if connection_by_id[id].mech == null:
				state.print_log(str(id) + " - no player object!")
				error_log[id] += 1
				return
			var data_dict = connection_by_id[id].mech
			
			match data[1]:
				
				MECH_DATA_CROUCH: 
					data_dict["crouch"] = data[2]
				MECH_DATA_WALK_LEFT: 
					data_dict["walk_left"] = data[2]
				MECH_DATA_WALK_RIGHT: 
					data_dict["walk_right"] = data[2]
				MECH_DATA_JUMP: 
					data_dict["jump"] = data[2]
				MECH_DATA_JET: 
					data_dict["jet"] = data[2]
				MECH_DATA_DASH:
					data_dict["dash"] = data[2]
				MECH_DATA_MOUSE_POS: 
					data_dict["mouse_pos"] = data[2]
				MECH_DATA_POS: 
					data_dict["pos"] = data[2]
				MECH_DATA_VEL: 
					data_dict["vel"] = data[2]
				MECH_DATA_JET_FUEL: 
					data_dict["jet_fuel"] = data[2]
				MECH_DATA_SHIELD_HP:
					data_dict["shield_hp"] = data[2]
				MECH_DATA_HIDDEN:
					data_dict["hidden"] = data[2]
				MECH_DATA_STEALTH_MODE:
					data_dict["stealth_mode"] = data[2]
				MECH_DATA_DRILL:
					data_dict["drill"] = data[2]
				MECH_DATA_AIM:
					data_dict["aim"] = data[2]
				_:
					acceptable = false
			
			if acceptable:
				
				data = [id, UN_MECH_DATA, data[1], data[2]]
				raw_data = var2bytes(data)
				
				for i in connection_by_id.keys():
					
					if i == id: continue
					if i in get_tree().multiplayer.get_network_connected_peers():
						get_tree().multiplayer.send_bytes(raw_data, i, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)
						if error_log[id] > 0: error_log[id] -= 1
					else:
						state.print_log(str(id) + " - dropped packet!")
						error_log[id] += 1
			
		UN_WEAPON_DATA:
			
			var acceptable = true
			if connection_by_id[id].mech == null:
				state.print_log(str(id) + " - no player object!")
				error_log[id] += 1
				return
			var data_dict = connection_by_id[id].mech["weapon"]
			
			match data[1]:
				
				WEAPON_DATA_SHOOT:
					data_dict["shoot"] = data[2]
				_:
					acceptable = false
				
			if acceptable:
				
				data = [id, UN_WEAPON_DATA, data[1], data[2]]
				raw_data = var2bytes(data)
				
				for i in connection_by_id.keys():
					
					if i == id: continue
					if i in get_tree().multiplayer.get_network_connected_peers():
						get_tree().multiplayer.send_bytes(raw_data, i, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)
						if error_log[id] > 0: error_log[id] -= 1
					else:
						state.print_log(str(id) + " - dropped packet!")
						error_log[id] += 1

func _ready():
	
	PORT = state.server_port
	get_tree().multiplayer.connect("network_peer_packet", self, "unreliable_packet")
	$remote.start_remote()
	
	OS.set_window_title("dno server")
	
	restart()

func exit():
	closing = true
	yield(get_tree().create_timer(1), "timeout")
	tcp_server.stop()
	enet_unreliable.close_connection(100)
	yield(get_tree().create_timer(1), "timeout")
	get_tree().quit()
	

func restart():
	
	set_process(false)
	$game.set_process(false)
	#$modules.set_process(false)
	$game.connection = connection
	$game.connection_by_id = connection_by_id
	$game.client_by_id = client_by_id
	$modules.connection = connection
	$modules.connection_by_id = connection_by_id
	$modules.client_by_id = client_by_id
	
	is_restart = true
	
	emit_signal("reset")
	
	state.print_log("Generating map...")
	randomize()
	$map.generate_map()
	
	yield($map, "generation_end")
	
	for client in connection:
		send_map(client)
		
	emit_signal("restart")
	
	if tcp_server == null and enet_unreliable == null:
		tcp_server = TCP_Server.new()
		enet_unreliable = NetworkedMultiplayerENet.new()
		
		if tcp_server.listen(PORT) == OK and enet_unreliable.create_server(PORT, MAX_CLIENTS) == 0:
			state.print_log("Server started on port " + str(PORT))
			get_tree().set_network_peer(enet_unreliable)
			set_process(true)
			$game.set_process(true)
			#$modules.set_process(true)
	
	else:
		set_process(true)
		$game.set_process(true)
		#$modules.set_process(true)

# server boring funcs
func broadcast_connect(who):
	var data = [PLAYER_CONNECT, connection[who].id, connection[who].nickname]
	for client in connection:
		if client == who:
			continue
		connection[client].peer.put_var(data)
	
	if connection.size() < 2: return
	
	data = [PLAYER_CONNECT]
	for client in connection:
		if client == who:
			continue
		
		var mech_data = null
		if connection[client].alive_time > -1 :
			mech_data = connection[client].mech.duplicate(true)
		
		data.append([connection[client].id, connection[client].nickname, connection[client].team, mech_data])
	connection[who].peer.put_var(data)

func send_loading_end(who):
	var data = [LOADING_END]
	connection[who].peer.put_var(data)
	
func broadcast_disconnect(who):
	
	state.print_log(connection[who].nickname + " disconnected")
	var data = [PLAYER_DISCONNECT, connection[who].id]
	
	connection_by_id.erase(connection[who].id)
	if in_game.has(str(connection[who].id)): in_game.erase(str(connection[who].id))
	
	emit_signal("player_disconnect",who)
	connection.erase(who)
	for client in connection:
		connection[client].peer.put_var(data)
	who = null
	
	var nicknames = ""
	for c in connection:
		nicknames+=connection[c].nickname + ", "
	if nicknames == "":
		state.print_log("Server empty!")
	else:
		state.print_log("Currently on server: " + nicknames.left(nicknames.length()-2))

# gamish funcs
func send_map(who):
	# cells
	connection[who].peer.put_var([MAP, "start"])
	var packet = []
	for cell in $map.cells:
		if !connection.has(who): return
		if packet.size() < 4096:
			packet.append(cell)
		else:
			connection[who].peer.put_var([MAP, "cells", packet])
			packet = [cell]
	if packet.size() > 0:
		connection[who].peer.put_var([MAP, "cells", packet])
	# bg cells
	packet = []
	for cell in $map.bg_cells:
		if !connection.has(who): return
		if packet.size() < 4096:
			packet.append(cell)
		else:
			connection[who].peer.put_var([MAP, "bg_cells", packet])
			packet = [cell]
	if packet.size() > 0:
		connection[who].peer.put_var([MAP, "bg_cells", packet])
	# structures
	packet = []
	for id in $map.structure_cells.keys():
		if !connection.has(who): return
		for cell in $map.structure_cells[id]:
			if packet.size() < 4096:
				packet.append(cell)
			else:
				connection[who].peer.put_var([MAP, "structure_cells", id, packet])
				packet = [cell]
		if packet.size() > 0:
			connection[who].peer.put_var([MAP, "structure_cells", id, packet])
	# structures_places
	connection[who].peer.put_var([MAP,"structures_places",$map.structures_places.duplicate()])
	# ores
	packet = []
	for cell in $map.ore:
		if !connection.has(who): return
		if packet.size() < 4096:
			packet.append(cell)
		else:
			connection[who].peer.put_var([MAP, "ore", packet])
			packet = [cell]
	if packet.size() > 0:
		connection[who].peer.put_var([MAP, "ore", packet])
	# tnt
	packet = []
	for cell in $map.tnt:
		if !connection.has(who): return
		if packet.size() < 4096:
			packet.append(cell)
		else:
			connection[who].peer.put_var([MAP, "tnt", packet])
			packet = [cell]
	if packet.size() > 0:
		connection[who].peer.put_var([MAP, "tnt", packet])
	
	connection[who].peer.put_var([MAP, "seats", $map.seats])
	
	connection[who].peer.put_var([MAP, "end"])