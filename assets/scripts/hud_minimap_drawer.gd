extends Node2D

var POINTS_STATE_COLORS = null

var texture = null
var seat_positions = null
var player_positions = null
var player_colors = null
var points = null

func _ready():
	update()

func _draw():
	
	if texture != null:
		draw_texture(texture,Vector2(0,0))
		
	if seat_positions != null:
		for seat_data in seat_positions:
			draw_rect(
			Rect2(seat_data[0] - Vector2(3, -2), Vector2(5,2)),
			seat_data[1]
			)
	
	if player_positions != null and player_colors != null:
		for idx in range(player_positions.size()):
			draw_rect(
			Rect2(player_positions[idx] - Vector2(1,1), Vector2(3,3)),
			player_colors[idx]
			)
	
	if points != null and POINTS_STATE_COLORS != null:
		for p in points.keys():
			draw_circle(points[p][0],3,POINTS_STATE_COLORS[points[p][1]])