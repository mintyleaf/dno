extends Light2D

var state = 0
var cap_team = 0
var progress = 0.0

var prev_state = 0
var prev_cap_team = 0

var point_idx
var color_from = Color(1,1,1)
var color_to = Color(1,1,1)
onready var client = $"/root/game"
onready var hud = $"/root/game/fg/hud"

func _process(delta):
	
	if client.points.size()-1 < point_idx: return
	
	if client.points[point_idx]["state"] != state:
		state = client.points[point_idx]["state"]
		update()
	if client.points[point_idx]["cap_team"] != cap_team:
		cap_team = client.points[point_idx]["cap_team"]
		update()
	if client.points[point_idx]["progress"] != progress:
		progress = client.points[point_idx]["progress"]
		update()
	
	if prev_state != state:
		hud.update_points_indicator(point_idx,state)
		prev_state = state
#	if prev_cap_team != cap_team:
#		hud.update_points_indicator(point_idx,state,cap_team)
#		prev_cap_team = cap_team

func update():
	
	if state == 0:
		$bg.modulate = Color(1,1,1,0)
		return
	if state == 1:
		$bg.modulate = Color(1,1,1)
	
	match state:
		1: 
			color_from = Color(1,1,1)
			$bg.modulate = Color(1,1,1)
		2: 
			color_from = Color(0.3,0.25,1).lightened(0.1)
			$bg.modulate = Color(1,0.3,0.1).lightened(0.1)
		3: 
			color_from = Color(1,0.3,0.1).lightened(0.1)
			$bg.modulate = Color(0.3,0.25,1).lightened(0.1)
		
	match cap_team:
		0: color_to = Color(1,0.3,0.1).lightened(0.1)
		1: color_to = Color(0.3,0.25,1).lightened(0.1)

	$z/bar.value = 100 * (progress)
	$z/bar.modulate = color_from.linear_interpolate(color_to,progress)

