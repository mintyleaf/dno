extends Line2D

func _ready():
	
	$light.position = points[0].linear_interpolate(points[1],0.5)
	$light.scale.x = points[0].distance_to(points[1])/64
	$light.rotation = points[0].angle_to_point(points[1])

func _process(delta):
	width-=delta*30
	if width < 0.3:
		queue_free()

