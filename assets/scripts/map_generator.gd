extends TileMap

const STRUCTURES_FOLDER = "res://assets/structures"

const SAMPLE = 2048 # количество итераций за кадр
const SMOOTH_PASSES_DEF = 18 # количество проходов общих фильтров сглаживания

const STRUCTURE_MIN_H = 18 # минимальная высота структуры (проверка на потолок)
const STRUCTURE_CORNER_MIN_DIST = 16 # минимальная возможная дистанция для размещения вертикальной и горизонатальных структур рядом
const STRUCTURE_NORMAL_MIN_DIST = 16 # минимальная возможная дистанция для размещения одинаковых структур рядом

const STRUCTURE_INSIDE_MIN_SIZE = 3 # минимальный размер структуры внутри земли
const STRUCTURE_INSIDE_MAX_SIZE = 7 # максимальный размер структуры внутри земли
const STRUCTURE_INSIDE_MAX_COUNT = 64 # максимальное число структур внутри земли
const STRUCTURE_INSIDE_MAX_FAILED_ATTEMPTS = 512 # максимальное число неудачных подборок для основной точки структуры внутри земли

const BASE_CIRCLES_COUNT = 26 # количество базовых кругов
const BASE_CIRCLE_X_RANGE = 190 # диапазон для основной точки круга по X
const BASE_CIRCLE_Y_RANGE = 50 # диапазон для основной точки круга по Y
const BASE_CIRCLE_RADIUS_MIN = 30 # минимальный радиус круга
const BASE_CIRCLE_RADIUS_RANGE = 30 # диапазон радиуса круга (суммируется с минимальным)
const BASE_CIRCLE_DEF_RANGE = 20 # диапазон переменной, отвечающей за деформацию круга ("ромбирование")

const NOISE_BIG_OCTAVES = 20 # октавы основного шума
const NOISE_BIG_PERIOD_MIN = 50 # минимальный период основного шума
const NOISE_BIG_PERIOD_RANGE = 14 # диапазон периода основного шума (суммируется с минимальным)
const NOISE_BIG_THRESHOLD = 0.03 # порог значения основного шума для удаления тайлов

const NOISE_CAVE_OCTAVES = 8 # октавы шума пещер
const NOISE_CAVE_PERIOD_MIN = 16 # минимальный период шума пещер
const NOISE_CAVE_PERIOD_RANGE = 6 # диапазон периода шума пещер (суммируется с минимальным)
const NOISE_CAVE_THRESHOLD = 0.2 # порог значения шума пещер для удаления тайлов

const NOISE_ORE_OCTAVES = 8 # октавы вшума руд
const NOISE_ORE_PERIOD_MIN = 6 # минимальный период шума руд
const NOISE_ORE_PERIOD_RANGE = 4 # диапазон периода шума руд (суммируется с минимальным)
const NOISE_ORE_THRESHOLD = 0.05 # порог значения шума руд для размещения руды (размещается руда если значение > 0 и < этого порога)

const ORE_DISTANCE_DIVIDER = 40 # делитель для расстояния от центральной точки при размещении руды
const TNT_CHANCE = 20 # шанс на появление динамита на краю

const SEAT_X_RANGE = 32 # диапазон X координаты точки спауна игрока
const SEAT_Y_RANGE = 64 # диапазон Y координаты точки спауна игрока
const SEAT_MIN_DIST = 8 # минимальная дистанция между точками спауна игрока
const SEAT_OFFSET = 32 # высота по Y средней точки области появления точек спауна игрока

var cells = []
var bg_cells = []
var structure_cells = {}
var ore = []
var tnt = []
var seats = []
var structures_places = []
var corner = []

signal generation_end
signal regen

func _ready():
	if $"..".name == "root":
		while true:
			randomize()
			generate_map()
			yield(self,"generation_end")
			yield(self,"regen")

func _process(delta):
	if Input.is_action_just_pressed("jump"):
		emit_signal("regen")
	if Input.is_action_just_pressed("ui_up"):
		$Camera2D.zoom /= 1.1
	if Input.is_action_just_pressed("ui_down"):
		$Camera2D.zoom *= 1.1

func generate_map():
	# clear
	clear()
	$structures.clear()
	# generate circles base
	for i in range(BASE_CIRCLES_COUNT):
		circle(
		randi()%BASE_CIRCLE_X_RANGE-BASE_CIRCLE_X_RANGE/2,
		randi()%BASE_CIRCLE_Y_RANGE-BASE_CIRCLE_Y_RANGE/2,
		BASE_CIRCLE_RADIUS_MIN
		+randi()%BASE_CIRCLE_RADIUS_RANGE-BASE_CIRCLE_RADIUS_RANGE/2,
		true,true,
		randi()%BASE_CIRCLE_DEF_RANGE)
	# do the noise 
	pass_noise()

func pass_noise(smooth_passes=SMOOTH_PASSES_DEF):
	
	cells = get_used_cells()
	bg_cells = []
	structure_cells = {}
	ore = []
	tnt = []
	seats = []
	structures_places = []
	corner = []
	
	# pass a main noise

	var simplex_cell_current = 0
	var noise = OpenSimplexNoise.new()
	noise.seed = randi()
	noise.octaves = NOISE_BIG_OCTAVES
	noise.period = NOISE_BIG_PERIOD_MIN + (randf() - 0.5) * NOISE_BIG_PERIOD_RANGE
	
	while simplex_cell_current < cells.size()-1:
		for i in range(SAMPLE):
			if simplex_cell_current > cells.size()-1:
				break
			var a = cells[simplex_cell_current]
			var result = noise.get_noise_2dv(a)
			if result > NOISE_BIG_THRESHOLD:
				set_cell(a.x,a.y,-1)
			simplex_cell_current+=1
		yield(get_tree(),"idle_frame")
	
	cells = get_used_cells()
	
	# smooth it

	for i in range(smooth_passes/2):
		for cell in cells:
			var score = 0
			for x in range(3):
				for y in range(3):
					if get_cellv((cell + Vector2(x-1,y-1))) == 0:
						score+=1
			if score < 5:
				set_cellv(cell,-1)
	
	bg_cells = get_used_cells()
	cells = get_used_cells()
	
	# do the small caves

	simplex_cell_current = 0
	noise = OpenSimplexNoise.new()
	noise.seed = randi()
	noise.octaves = NOISE_CAVE_OCTAVES
	noise.period = NOISE_CAVE_PERIOD_MIN + (randf() - 0.5) * NOISE_CAVE_PERIOD_RANGE
	
	while simplex_cell_current < cells.size()-1:
		for i in range(SAMPLE):
			if simplex_cell_current > cells.size()-1:
				break
			var a = cells[simplex_cell_current]
			var result = noise.get_noise_2dv(a)
			if result > NOISE_CAVE_THRESHOLD:
				set_cell(a.x,a.y,-1)
			simplex_cell_current+=1
		yield(get_tree(),"idle_frame")
	
	cells = get_used_cells()
	
	# smooth it

	for i in range(smooth_passes):
		for cell in cells:
			var score = 0
			for x in range(3):
				for y in range(3):
					if get_cellv((cell + Vector2(x-1,y-1))) == 0:
						score+=1
			if score < 5:
				set_cellv(cell,-1)
	
	cells = get_used_cells()
	
	# do the ores

	simplex_cell_current = 0
	noise = OpenSimplexNoise.new()
	noise.seed = randi()
	noise.octaves = NOISE_ORE_OCTAVES
	noise.period = NOISE_ORE_PERIOD_MIN + (randf() - 0.5) * NOISE_ORE_PERIOD_RANGE
	
	while simplex_cell_current < cells.size()-1:
		for i in range(SAMPLE):
			if simplex_cell_current > cells.size()-1:
				break
			var a = cells[simplex_cell_current]
			var result = noise.get_noise_2dv(a)
			result += Vector2(0,0).distance_to(a)/ORE_DISTANCE_DIVIDER
			if result > 0 and result < NOISE_ORE_THRESHOLD:
				ore.append(a)
			simplex_cell_current+=1
		yield(get_tree(),"idle_frame")
	
	cells = get_used_cells()
	
	# get the corner tiles to buffer

	get_corner()
	
	# place structures and prepare seats positions

	find_structure_places()
	place_structures()
	seats()
	
	# place tnt 

	for i in corner:
		if get_cellv(i) == -1 and $structures.get_cellv(i) == -1:
			if randi()%TNT_CHANCE == 0:
				tnt.append(i)
	
	###
	
	for i in $structures.tile_set.get_tiles_ids():
		structure_cells[i] = $structures.get_used_cells_by_id(i)
	
	if $"..".name == "root": update()
	
	emit_signal("generation_end")

func seats():
	for team in range(2):
		for i in range(8):
			while true:
				var x = (-1 + 2*team) * (BASE_CIRCLE_RADIUS_MIN + BASE_CIRCLE_RADIUS_RANGE + BASE_CIRCLE_X_RANGE/2 + randi()%SEAT_X_RANGE)
				var y = randi()%SEAT_Y_RANGE - SEAT_Y_RANGE/2 - SEAT_OFFSET
				var fail = false
				
				for seat in seats:
					if Vector2(x,y).distance_to(seat[0]) < SEAT_MIN_DIST:
						fail = true
				
				if !fail:
					seats.append([Vector2(x,y),team])
					break

func get_corner():
	var cells = get_used_cells()
	
	for i in cells:
		for x in range(2):
			var r = i + Vector2(1-x*2,0)
			if get_cellv(r) == -1:
				if !(r in corner):
					corner.append(r)
		for y in range(2):
			var r = i + Vector2(0,1-y*2)
			if get_cellv(r) == -1:
				if !(r in corner):
					corner.append(r)
			
func find_structure_places():
	var corner_buffer = corner.duplicate()
	var inside_tiles = get_used_cells()
	
	var idx = 0
	var point_idx = 0
	
	while true:
		var point = corner_buffer[idx]
		var structure_place = [-1,-1,Vector2(),[],"", -1, Vector2()] # (0 - horiz, 1 - vert), (size), root_vec2, points, name, idx, root_point
		var is_structure_place = false
		var type = 0 # 0 - horiz, 1 - vert
		var structure_place_points = []
		
		for x in range(2):
			# get start point
			var result = corner_buffer.find(point + Vector2(1-x*2,0))
			if result != -1 and x == 0:
				# check height (only up)
				var height_fail = false
				for h in range(STRUCTURE_MIN_H):
					if get_cellv(point + Vector2(0,-h-1)) == 0:
						height_fail = true
				if !height_fail:
					is_structure_place = true
					structure_place_points.append(point)
					structure_place_points.append(corner_buffer[result])
					break

		# if height is ok start searching around
		if is_structure_place:
			var diff = 2
			while true:
				var result = corner_buffer.find(point + Vector2(diff,0))
				if result != -1:
					structure_place_points.append(corner_buffer[result])
					diff+=1
				elif result == -1:
					break
			diff = -1
			var second_way = false
			# searching for two way spare place for this point
			while true:
				var result = corner_buffer.find(point + Vector2(diff,0))
				if result != -1:
					second_way = true
					structure_place_points.append(corner_buffer[result])
					diff-=1
				elif result == -1:
					break
			if !second_way:
				is_structure_place = false
				structure_place_points = []
		
		# if horizontal search fail, trying to find vertical place
		if !is_structure_place:
			for y in range(2):
				var result = corner_buffer.find(point + Vector2(0,1-y*2))
				if result != -1 and y == 0:
					var height_fail = false
					for h in range(STRUCTURE_MIN_H):
						if get_cellv(point + Vector2(h+1,0)) == 0:
							height_fail = true
					if height_fail:
						height_fail = false
						for h in range(STRUCTURE_MIN_H):
							if get_cellv(point + Vector2(-h-1,0)) == 0:
								height_fail = true
					if !height_fail:
						is_structure_place = true
						type = 1
						structure_place_points.append(point)
						structure_place_points.append(corner_buffer[result])
						break
			if is_structure_place:
				var diff = 2
				while true:
					var result = corner_buffer.find(point + Vector2(0,diff))
					if result != -1:
						structure_place_points.append(corner_buffer[result])
						diff+=1
					elif result == -1:
						break
				diff = -1
				var second_way = false
				while true:
					var result = corner_buffer.find(point + Vector2(0,diff))
					if result != -1:
						second_way = true
						structure_place_points.append(corner_buffer[result])
						diff-=1
					elif result == -1:
						break
				if !second_way:
					is_structure_place = false
					structure_place_points = []
		
		if is_structure_place:
			
			var height_fail = false
			for p in structure_place_points:
				for h in range(STRUCTURE_MIN_H):
					if type == 0:
						if get_cellv(p + Vector2(0,h+1)) == 0:
							height_fail = true
					elif type == 1:
						if get_cellv(p + Vector2(h+1,0)) == 0:
							height_fail = true
			if height_fail:
				height_fail = false
				for p in structure_place_points:
					for h in range(STRUCTURE_MIN_H):
						if type == 0:
							if get_cellv(p + Vector2(0,-h-1)) == 0:
								height_fail = true
						elif type == 1:
							if get_cellv(p + Vector2(-h-1,0)) == 0:
								height_fail = true
				
			if !height_fail: 
				# sort points for gettin central point
				structure_place_points.sort()
				var root_point =  structure_place_points[structure_place_points.size()/2]
				
				# check for distance for oth points
				var dist_fail = false
				
				for structure in structures_places:
					if ((structure[2].distance_to(root_point) < STRUCTURE_CORNER_MIN_DIST and structure[0] != type) or
					structure[2].distance_to(root_point) < STRUCTURE_NORMAL_MIN_DIST):
						dist_fail = true
						break
				
				if !dist_fail:
					# form the structure 
					for i in structure_place_points:
						corner_buffer.erase(i)
					structure_place[0] = type
					structure_place[1] = structure_place_points.size()
					structure_place[2] = root_point
					structure_place[3] = structure_place_points
					structure_place[5] = point_idx
					structures_places.append(structure_place)
					point_idx+=1
			
		if idx+1 < corner_buffer.size():
			idx+=1
		else:
			break
	
	var counter = 0
	var failed_iter_count = 0
	
	# also get the inside places
	while true:
		var root_point = inside_tiles[randi()%inside_tiles.size()]
		var structure_place = [2,-1,Vector2(),[], "", -1, Vector2()] # -, (size), root_vec2, -, name, idx, root_point
		
		var size = 0
		var fail = false
		var dist_fail = false
		
		# check for distance
		for structure in structures_places:
			if structure[2].distance_to(root_point) < STRUCTURE_NORMAL_MIN_DIST:
				dist_fail = true
				break
		
		if dist_fail:
			failed_iter_count+=1
			continue
		
		# if the corner points alright, we count that place
		for i in range(STRUCTURE_INSIDE_MIN_SIZE,STRUCTURE_INSIDE_MAX_SIZE+1):
			fail = false
			for x in range(3):
				for y in range(3):
					if get_cellv(root_point + Vector2((x-1)*i,(y-1)*i)) == -1:
						fail = true
			if !fail: size = i
		
		if size > 0:
			# form the structure
			counter+=1
			failed_iter_count = 0
			structure_place[1] = size
			structure_place[2] = root_point
			structure_place[5] = point_idx
			structures_places.append(structure_place)
			point_idx += 1
		else:
			failed_iter_count += 1
		# prevent endless loop or long generation
		if failed_iter_count > STRUCTURE_INSIDE_MAX_FAILED_ATTEMPTS or counter > STRUCTURE_INSIDE_MAX_COUNT:
			break
	
func read_file(path):
	
	var structure = {}
	
	var f = File.new()
	f.open(path,File.READ)
	var json_data = f.get_as_text()
	f.close()
	var data = parse_json(json_data)
	structure["name"] = data["name"]
	structure["type"] = int(data["type"])
	structure["length"] = int(data["length"])
	var text_groups = data["groups"]
	var text_groups_tiles = data["groups_tiles"]
	var text_struct_object_root = data["struct_object_root"]
	var used_tiles = data["tiles"]
	var int_used_tiles = {}
	
	var int_groups = {}
	var int_groups_tiles = {}
	
	for i in text_groups.keys():
		int_groups[int(i)] = []
		for j in text_groups[i]:
			int_groups[int(i)].append(int(j))
	
	for i in text_groups_tiles.keys():
		var vec = i.split(',')
		vec[0] = vec[0].replace('(','')
		vec[1] = vec[1].replace(')','')
		vec = Vector2(int(vec[0]),int(vec[1]))
		int_groups_tiles[vec] = int(text_groups_tiles[i])
	
	structure["groups_tiles"] = int_groups_tiles
	structure["groups"] = int_groups
	
	for i in used_tiles:
		int_used_tiles[int(i)] = []
		for j in used_tiles[i]:
			var vec = j.split(',')
			vec[0] = vec[0].replace('(','')
			vec[1] = vec[1].replace(')','')
			vec = Vector2(int(vec[0]),int(vec[1]))
			int_used_tiles[int(i)].append(vec)
	
	structure["tiles"] = int_used_tiles
	
	var vec = text_struct_object_root.split(',')
	vec[0] = vec[0].replace('(','')
	vec[1] = vec[1].replace(')','')
	vec = Vector2(int(vec[0]),int(vec[1]))
	
	structure["struct_object_root"] = vec
	
	return structure

func place_structures():
	
	var loaded_structures_by_size = {}
	var structures_paths = []
	
	var dir = Directory.new()
	if dir.change_dir(STRUCTURES_FOLDER) != OK:
		print("Can't open dir!")
		return
	
	dir.list_dir_begin()
	var file_name = dir.get_next()
	while file_name != "":
		if !dir.current_is_dir():
			structures_paths.append(STRUCTURES_FOLDER+"/"+file_name)
		file_name = dir.get_next()
	
	for i in structures_paths:
		var structure = read_file(i)
		if structure["length"] in loaded_structures_by_size.keys():
			loaded_structures_by_size[structure["length"]].append(structure)
		else:
			loaded_structures_by_size[structure["length"]] = [structure]
	
	var not_found_places = []
	
	for i in structures_places:
		var length = i[1]
		var type = i[0]
		
		if !loaded_structures_by_size.has(length):
			not_found_places.append(i)
			continue
			
		var possible_structures = loaded_structures_by_size[length].duplicate()
		var rejected_structures = []
		for j in possible_structures:
			if j["type"] != type:
				rejected_structures.append(j)
		
		for j in rejected_structures:
			possible_structures.erase(j)
		
		if possible_structures.size() == 0:
			not_found_places.append(i)
			continue
		
		var to_place = possible_structures[randi()%possible_structures.size()]
		
		i[4] = to_place["name"]
		i[6] = to_place["struct_object_root"]
		place_structure(to_place,i[2],length,type)
	
	for i in not_found_places:
		structures_places.erase(i)

func place_structure(structure,root_point,length,type):
	var orientation = false # true - right
	if type == 1:
		if get_cellv(root_point + Vector2(1,0)) == -1:
			orientation = true
	
	var mirror = bool(randi()%2)
	if structure["name"].begins_with("engine_side"):
		if orientation: mirror = true
		else: mirror = false
	
	for i in structure["groups_tiles"].keys():
		var group_id = structure["groups_tiles"][i]
		var tiles_range = structure["groups"][group_id]
		var pos 
		if type == 0 or (type == 1 and !orientation) or type == 2:
			match type:
				0:
					if mirror:
						if structure["length"]%2 == 0: pos = Vector2(-i.x-1,i.y)
						else: pos = Vector2(-i.x,i.y)
					else:
						pos = i
				1:
					if mirror:
						if structure["length"]%2 == 0: pos = Vector2(i.x,-i.y-1)
						else: pos = Vector2(i.x,-i.y)
					else:
						pos = i
				2:
					pos = i
			$structures.set_cellv(pos+root_point,tiles_range[randi()%tiles_range.size()])
		elif type == 1 and orientation:
			var offset = Vector2(0,0)
			if structure["length"]%2 == 0: offset = Vector2(0,1)
			if mirror:
				if structure["length"]%2 == 0: pos = Vector2(i.x,-i.y-1)
				else: pos = Vector2(i.x,-i.y)
			else:
				pos = i
			$structures.set_cellv(-pos+root_point - offset,tiles_range[randi()%tiles_range.size()])
	
	for i in structure["tiles"].keys():
		for j in structure["tiles"][i]:
			var pos
			if type == 0 or (type == 1 and !orientation) or type == 2:
				match type:
					0:
						if mirror:
							if structure["length"]%2 == 0: pos = Vector2(-j.x-1,j.y)
							else: pos = Vector2(-j.x,j.y)
						else:
							pos = j
					1:
						if mirror:
							if structure["length"]%2 == 0: pos = Vector2(j.x,-j.y-1)
							else: pos = Vector2(j.x,-j.y)
						else:
							pos = j
					2:
						pos = j
				$structures.set_cellv(pos+root_point,i)
			elif type == 1 and orientation:
				if mirror:
					if structure["length"]%2 == 0: pos = Vector2(j.x,-j.y-1)
					else: pos = Vector2(j.x,-j.y)
				else:
					pos = j
				var offset = Vector2(0,0)
				if structure["length"]%2 == 0: offset = Vector2(0,1)
				$structures.set_cellv(-pos+root_point - offset,i)

func _draw():
	for i in corner:
		draw_rect(Rect2(i*32,Vector2(32,32)),Color(1,1,1,0.25))
	for i in ore:
		draw_rect(Rect2(i*32,Vector2(32,32)),Color(1,0.5,0.5,0.5))
	for i in tnt:
		draw_rect(Rect2(i*32,Vector2(32,32)),Color(1,0,1))
	for i in seats:
		draw_circle(i[0]*32,64,Color(1,0,0))
	for i in structures_places:
		if i[0] != 2:
			for j in i[3]:
				var c = Color(1,1,0)
				draw_rect(Rect2(j*32,Vector2(32,32)),c)
		elif i[0] == 2:
			var c = Color(1,1,0)
			draw_rect(Rect2((i[2]*32 - Vector2(32,32)*i[1]/2).snapped(Vector2(32,32)),Vector2(32,32)*i[1]),c)
		draw_rect(Rect2(i[2]*32,Vector2(32,32)),Color(1,0,0))
	pass

func circle(x0, y0, radius, fill=false, full_circle=true, d=1):
	var x = radius-1
	var y = 0
	var dx = d
	var dy = d
	var err = dx - (radius << 1)

	while x >= y:
		
		for i in range(x):
			if fill:
				set_cell(x0 + i, y0 + y, 0)
				set_cell(x0 + y, y0 + i, 0)
				set_cell(x0 - y, y0 + i, 0)
				set_cell(x0 - i, y0 + y, 0)
				if full_circle:
					set_cell(x0 - i, y0 - y, 0)
					set_cell(x0 - y, y0 - i, 0)
					set_cell(x0 + y, y0 - i, 0)
					set_cell(x0 + i, y0 - y, 0)
			else:
				set_cell(x0 + i, y0 + y, -1)
				set_cell(x0 + y, y0 + i, -1)
				set_cell(x0 - y, y0 + i, -1)
				set_cell(x0 - i, y0 + y, -1)
				if full_circle:
					set_cell(x0 - i, y0 - y, -1)
					set_cell(x0 - y, y0 - i, -1)
					set_cell(x0 + y, y0 - i, -1)
					set_cell(x0 + i, y0 - y, -1)
			
		if err <= 0:
			y+=1
			err += dy
			dy += 2
        
		if (err > 0):
			x-=1
			dx += 2
			err += dx - (radius << 1)
