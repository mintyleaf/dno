extends "res://assets/scripts/net_constants.gd"

const CONFIG_PATH = "user://dno.cfg"

var port
var ip
var connection
var enet_unreliable
var peer

var alive_timer = 0.0
var connection_timer = 0.0
var connected = false
var ui_busy = false

var network_helper

signal connecting
signal connected
signal cantconnect
signal disconnected
signal players
signal map
signal loading_end

signal team
signal timer
signal chat
signal respawn
signal modules_data
signal modules_update
signal mony_update
signal team_points_update
signal update_feed
signal update_tab_menu

signal player_connected
signal player_disconnected

signal me_respawn
signal me_dead

signal seat
signal me_pos
signal cell

var players = {}
var me = null
var me_mech = null

var modules_data = []
var tilemap_hit = {}

var props = {}

var cfg = ""

var loaded_ip = ""
var loaded_port = 0
var loaded_nickname = ""
var after_battle = false

class module:
	var id
	var name
	var desc
	var type
	var pric
	var conf
	var prir
	var vals

class prop:
	var pos = Vector2(0,0)
	var rot = 0.0
	var vel = Vector2(0,0)
	var ang = 0.0

class weapon:
	var id
	var ammo_size
	var shoot_rate
	var dmg

class player:
	var id = -1
	var nickname = ""
	var team = -1
	var mech = null
	var seat = null
	var modules = {}
	var modules_order = []
	var mony = 0
	var cred = 0
	var stats = [0,0,0]

class weapon_object:
	var shoot = {}

class mech_object:
	# transform
	var pos = Vector2(0,0)
	var vel = Vector2(0,0)
	# game
	var hitbox = {
		HB_LT : HITBOX_DEFAULT_BIG,
		HB_RT : HITBOX_DEFAULT_BIG,
		HB_LB : HITBOX_DEFAULT_BIG,
		HB_RB : HITBOX_DEFAULT_BIG,
		HB_CE : HITBOX_DEFAULT_CENTER
	}
	var shield_hp = 0
	var shield_type = -1
	var stealth_mode = -1
	# input
	var mouse_pos = Vector2(0,0)
	var crouch = false
	var walk_left = false
	var walk_right = false
	var jump = false
	var jet = false
	var dash = false
	var hidden = false
	# animation stuff
	var jet_fuel = 0.0
	var shoot = {}
	var drill = false
	var aim = false
	# network
	var is_master = false
	# misc
	var weapon = weapon_object.new()

# handling proper wm exit
func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		exit()

func _ready():
	# prereq
	get_tree().multiplayer.connect("network_peer_packet", self, "unreliable_packet")
	$fg/death_screen.connect("respawn",self,"respawn")
	connect("connected",self,"new_player")
	connect("team",self,"team")
	#Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	state.resize()
	set_process(false)
	randomize()
	
	emit_signal("connecting")
	
	if !$"/root".has_node("network_helper"):
		var nv = Node.new()
		nv.name = "network_helper"
		nv.set_script(load("res://assets/scripts/network_helper.gd"))
		$"/root".add_child(nv)
	network_helper = $"/root/network_helper"
	$camera.network_helper = $"/root/network_helper"
	$fg/death_screen.network_helper = $"/root/network_helper"
	
	# network setup
	network_helper.ip = loaded_ip
	network_helper.port = loaded_port
	network_helper.nickname = loaded_nickname
	ip = network_helper.ip
	port = network_helper.port
	network_helper.nickname = loaded_nickname
	if null in [ip, port]:
		state.print_log("Null addr!")
		exit()
		return
	connection = StreamPeerTCP.new()
	enet_unreliable = NetworkedMultiplayerENet.new()
	
	# con tcp&udp
	if connection.connect_to_host(ip, port) != 0:
		if after_battle:
			network_helper.queue_free()
			queue_free()
		else:
			exit()
		return
	if enet_unreliable.create_client(ip, port) != 0:
		if after_battle:
			network_helper.queue_free()
			queue_free()
		else:
			exit()
		return
	
	# udp save id and peer
	get_tree().set_network_peer(enet_unreliable)
	network_helper.enet_id = enet_unreliable.get_unique_id()
	OS.set_window_title("DNO "+str(network_helper.enet_id))
	
	# tcp save peer
	peer = PacketPeerStream.new()
	peer.set_stream_peer(connection)
	
	# process connection
	if connection.get_status() == connection.STATUS_CONNECTED:
		state.print_log("Connected to " + ip + ":" + str(port))
		set_process(true)
		connected = true
		peer.put_var([PLAYER_CONNECT, network_helper.enet_id, network_helper.nickname])
		emit_signal("connected")
		
	elif connection.get_status() == StreamPeerTCP.STATUS_CONNECTING:
		state.print_log("Trying to connect " + ip + ":" + str(port))
		state.print_log("Timeout in: " + str(CLIENT_CONNECTION_TIMEOUT) + " seconds")
		connection_timer = CLIENT_CONNECTION_TIMEOUT
		set_process(true)
		
	elif connection.get_status() == connection.STATUS_NONE or connection.get_status() == StreamPeerTCP.STATUS_ERROR:
		if after_battle:
			network_helper.queue_free()
			queue_free()
			return
		state.print_log("Couldn't connect to " + ip + ":" + str(port))
		emit_signal("cantconnect")
		exit()
	
	if after_battle:
		$"/root/after_battle".queue_free()

func _process(delta):
	
	# process connection w/ timeout
	if !connected:
		
		if connection.get_status() == connection.STATUS_CONNECTED:
			state.print_log("Connected to " + ip + ":" + str(port))
			connected = true
			peer.put_var([PLAYER_CONNECT, network_helper.enet_id, network_helper.nickname])
			emit_signal("connected")
			
		if connection_timer > 0:
			connection_timer -= delta
		else:
			state.print_log("Server timed out")
			emit_signal("cantconnect")
			exit()
			return
	
	# exit if connection issues
	if connection.get_status() == connection.STATUS_NONE or connection.get_status() == connection.STATUS_ERROR:
		state.print_log("Server disconnected")
		emit_signal("disconnected")
		exit()
		set_process(false)
		return
	
	# send alive signal
	if alive_timer < ALIVE_TIMER_RATE:
		if alive_timer == 0.0:
			peer.put_var([ALIVE])
		alive_timer+=delta
		if alive_timer > ALIVE_TIMER_RATE:
			alive_timer = 0.0
	
	# process tcp packets
	if peer.get_available_packet_count() > 0:
		for i in range(peer.get_available_packet_count()):
			var data = peer.get_var()
			
			# match packet type
			match data[0]:
				
				# process new player or players
				PLAYER_CONNECT:
					if !data[1] is Array:
						new_player(data[1],data[2])
						emit_signal("player_connected",data[1],data[2])
						state.print_log(data[2] + " connected")
					else:
						emit_signal("players")
						data.remove(0)
						for p in data:
							new_player(p[0],p[1],p[2])
							if p[3] != null:
								reset_mech(p[0],p[3])
								
				
				PLAYER_DISCONNECT:
					state.print_log(players[data[1]].nickname + " disconnected")
					delete_player(data[1])
					delete_seat(data[1])
					emit_signal("player_disconnected",data[1])
				
				# pick map
				MAP:
					
					if data[1] == "start":
						for seat in $seats.get_children():
							seat.queue_free()
						$tilemap.spawn_anime_girls()
					
					$tilemap.pick_packet(data)
					emit_signal("map", data[1])
				
				# pick chat message
				MESSAGE:
					emit_signal("chat", data[1], data[2])
				
				LOADING_END:
					emit_signal("loading_end")
				
				RESPAWN:
					emit_signal("respawn",data[1])
				
				TEAM:
					players[data[1]].team = data[2]
					emit_signal("team",data[1],data[2])
					if $players.has_node(str(data[1])):
						$players.get_node(str(data[1])).team(data[2])
				
				TIMER:
					emit_signal("timer", data[1], data[2])
				
				HIT:
					var id = data[1]
					var part = data[2]
					var dmg = data[3]
					var from = data[4]
					
					if players.has(id) and $players.has_node(str(id)) and players[id].mech != null:
						if part == SHIELD:
							$players.get_node(str(id)).shield_damage()
							if id == network_helper.enet_id:
								if $players.get_node(str(id)).shield_hp > 0:
									$players.get_node(str(id)).shield_hp -= dmg
									if $players.get_node(str(id)).shield_hp < 0:
										$players.get_node(str(id)).shield_hp = 0
							else:
								if players[id].mech.shield_hp > 0:
									players[id].mech.shield_hp -= dmg
									if players[id].mech.shield_hp < 0:
										players[id].mech.shield_hp = 0
						else:
							players[id].mech.hitbox[part] -= dmg
							$players.get_node(str(id)).update_hitbox(part)
						
				
				TEAM_POINTS:
					
					emit_signal("team_points_update",data[1].duplicate())
				
				FEED:
					
					var feed_type = data[1]
					var feed_data = data[2].duplicate(true)
					
					if feed_type == FEED_WIN:
						exit(feed_data[0])
					
					emit_signal("update_feed",feed_type,feed_data)
				
				STATS:
					
					var id = data[1]
					var stats = data[2]
					
					if players.has(id):
						players[id].stats = stats.duplicate()
						emit_signal("update_tab_menu")
				
				POINT:
					
					match data[1]:
						
						POINT_CAP:
							
							var time = data[2]
							var id = data[3]
							var point_obj = $points.has_node(str(id))
							if point_obj:
								point_obj = $points.get_node(str(id))
								point_obj.set_time(time)
								
							
						POINT_HP:
							
							var hp = data[2]
							var id = data[3]
							var point_obj = $points.has_node(str(id))
							if point_obj:
								point_obj = $points.get_node(str(id))
								point_obj.set_hp(hp)
								
							
						POINT_STATE:
							
							var state = data[2]
							var id = data[3]
							var point_obj = $points.has_node(str(id))
							if point_obj:
								point_obj = $points.get_node(str(id))
								point_obj.set_state(state)
								
							
				
				EXPLOSION:
					
					var pos = data[1]
					var radius = data[2]
					var dmg = data[3]
					var worker_id = data[4]
					var megumin_to_blow = data[5]
					
					var expl_scn = load(EXPLOSION_PATH).instance()
					
					expl_scn.is_master = network_helper.enet_id == worker_id
					expl_scn.position = pos
					expl_scn.radius = radius
					expl_scn.dmg = dmg
					
					if $prop.has_node(megumin_to_blow) and megumin_to_blow != "":
						expl_scn.damage_everything = true
						$prop.get_node(megumin_to_blow).queue_free()
					
					$proj.add_child(expl_scn)
					
				
				TILEMAP_HIT:
					
					if data[1] is Dictionary:
						
						tilemap_hit = data[1].duplicate()
						$tilemap/destruct.clear()
						
						for cell in tilemap_hit.keys():
							
							if tilemap_hit[cell] < TILEMAP_HP:
								if tilemap_hit[cell] > 0:
									$tilemap/destruct.set_cellv(cell,2)
								if tilemap_hit[cell] > TILEMAP_HP * 0.5:
									$tilemap/destruct.set_cellv(cell,1)
								if tilemap_hit[cell] > TILEMAP_HP * 0.7:
									$tilemap/destruct.set_cellv(cell,0)
							else:
								$tilemap/destruct.set_cellv(cell,-1)
							
							if tilemap_hit[cell] < 1:
								$tilemap.set_cellv(cell, $tilemap.BG_CELLS[randi()%$tilemap.BG_CELLS.size()])
								$tilemap/destruct.set_cellv(cell,-1)
								emit_signal("cell", cell, false)
					
					else:
						
						var cell = data[1]
						var cell_dur = data[2]
						
						tilemap_hit[cell] = cell_dur
						
						if cell_dur < TILEMAP_HP:
							if cell_dur > 0:
								$tilemap/destruct.set_cellv(cell,2)
							if cell_dur > TILEMAP_HP * 0.5:
								$tilemap/destruct.set_cellv(cell,1)
							if cell_dur > TILEMAP_HP * 0.7:
								$tilemap/destruct.set_cellv(cell,0)
						else:
							$tilemap/destruct.set_cellv(cell,-1)
						
						if cell_dur < 1:
							$tilemap.set_cellv(cell, $tilemap.BG_CELLS[randi()%$tilemap.BG_CELLS.size()])
							$tilemap/destruct.set_cellv(cell,-1)
							emit_signal("cell", cell, false)
				
				PROP:
					
					var prop_name = data[1]
					
					if prop_name == "init":
						var props_data = data[2]
						
						for prop_data in props_data:
							var p_name = prop_data[0]
							var pos = prop_data[1]
							var rot = prop_data[2]
							var vel = prop_data[3]
							var ang = prop_data[4]
							
							var tex = prop_data[5]
							var col = prop_data[6]
							
							props[p_name] = prop.new()
							props[p_name].pos = pos
							props[p_name].rot = rot
							props[p_name].vel = vel
							props[p_name].ang = ang
							
							var p = load("res://assets/scenes/prop.tscn").instance()
							p.name = p_name
							p.build_prop(pos,rot,vel,ang,tex,col)
							
							$prop.add_child(p)
						
						return
					
					elif prop_name == "del":
						
						if data.size() < 3: return
						
						data.remove(0)
						data.remove(0)
						for del_name in data:
							
							if $prop.has_node(del_name):
								$prop.get_node(del_name).queue_free()
							
						return
					
					else:
						var pos = data[2]
						var rot = data[3]
						var vel = data[4]
						var ang = data[5]
						
						var tex = data[6]
						var col = data[7]
						
						props[prop_name] = prop.new()
						props[prop_name].pos = pos
						props[prop_name].rot = rot
						props[prop_name].vel = vel
						props[prop_name].ang = ang
						
						var p = load("res://assets/scenes/prop.tscn").instance()
						p.name = prop_name
						p.build_prop(pos,rot,vel,ang,tex,col)
						
						$prop.add_child(p)
				
				PLAYER_DATA:
					
					match data[1]:
						
						PD_RESPAWN:
							
							var id = data[2]
							
							respawn(id)
							
						PD_MOVE:
							
							var pos = data[2]
							
							if me_mech != null: me_mech.position = pos
							
						PD_KILL:
							
							var id = data[2]
							
							players[id].mech = null
							if $players.has_node(str(id)):
								
								if id == network_helper.enet_id: 
									emit_signal("me_dead")
									me_mech = null
								else:
									$players.get_node(str(id)).queue_free()
							
						PD_SEAT:
							
							if data[2] is Array:
								
								var seats_info = data[2]
								
								for seat_info in seats_info:
									
									var id = seat_info[0]
									var pos = seat_info[1]
									
									players[id].seat = pos
									spawn_seat(id)
							else:
								
								var id = data[2]
								var pos = data[3]
								
								players[id].seat = pos
								spawn_seat(id)
						
						PD_HEAL:
							
							var hitbox_part = data[2]
							var heal_amount = data[3]
							var id = data[4]
							
							if players[id].mech != null:
								players[id].mech.hitbox[hitbox_part] += heal_amount
							
						PD_MASS_RESET:
							
							for id in players.keys():
								if players[id].mech == null: continue
								
								# reset all players
								players[id].mech = null
								players[id].mech = mech_object.new()
								players[id].mech.is_master = id == network_helper.enet_id
								$players.get_node(str(id))._ready()
								$players.get_node(str(id)).update_hitbox()
							
							# reset map
							for cell in tilemap_hit:
								$tilemap.set_cellv(cell, $tilemap.CELLS[randi()%$tilemap.CELLS.size()])
								emit_signal("cell", cell, true)
							
							tilemap_hit = {}
							$tilemap.spawn_anime_girls()
							$tilemap/destruct.clear()
							
						PD_MODULE:
							
							var id = data[2]
							var modules = data[3]
							
							players[id].modules = modules.duplicate()
							emit_signal("modules_update",id,players[id].modules,players[id].modules_order)
							
						PD_MODULES_ORDER:
							
							var id = data[2]
							var modules_order = data[3]
							
							players[id].modules_order = modules_order.duplicate()
							# РУССКИЕ ИДУТ 12:33 20.07.2019
						
						PD_MONY:
							
							var id = data[2]
							var mony_data = data[3]
							
							players[id].mony = mony_data[0]
							players[id].cred = mony_data[1]
							
							emit_signal("mony_update",id,[players[id].mony,players[id].cred])
					
				MODULES_DATA:
					
					var modules_data_dicts = sort_modules(data[1])
					
					for jm in modules_data_dicts:
						
						var m = module.new()
						
						for val in m.get_property_list():
							if val.name in ["Reference","Script","script","Script Variables"]: continue
							if !jm.keys().has(val.name):
								state.print_log("[MODULES] Client received module with unexisting key: "+val.name)
								return
							
							m.set(val.name, jm[val.name])
							
						modules_data.append(m)
					
					emit_signal("modules_data",modules_data)
				
				MEGUMINS:
					
					$tilemap.DESTROYED_MEGUMINS = data[1].duplicate()
					if data.size() > 2:
						$tilemap.spawn_anime_girls()
					
				CFG:
					
					if !validate_json(data[1]):
						cfg = data[1]
				

# process udp packet

func unreliable_packet(id,raw_data):
	
	var data = bytes2var(raw_data)
	
	if !players.has(data[0]) and data[0] != 1:
		state.print_log("Receiving unknown player data!")
		return
	
	match data[1]:
		
		UN_MECH_DATA:
			
			var acceptable = true
			if players[data[0]].mech == null:
				#state.print_log("Receiving data from player that not respawned yet!")
				return
			var data_obj = players[data[0]].mech
			
			match data[2]:
				
				MECH_DATA_CROUCH: 
					data_obj.crouch = data[3]
				MECH_DATA_WALK_LEFT: 
					data_obj.walk_left = data[3]
				MECH_DATA_WALK_RIGHT: 
					data_obj.walk_right = data[3]
				MECH_DATA_JUMP: 
					data_obj.jump = data[3]
				MECH_DATA_JET: 
					data_obj.jet = data[3]
				MECH_DATA_DASH:
					data_obj.dash = data[3]
				MECH_DATA_MOUSE_POS: 
					data_obj.mouse_pos = data[3]
				MECH_DATA_POS: 
					data_obj.pos = data[3]
				MECH_DATA_VEL: 
					data_obj.vel = data[3]
				MECH_DATA_JET_FUEL: 
					data_obj.jet_fuel = data[3]
				MECH_DATA_SHIELD_HP:
					data_obj.shield_hp = data[3]
				MECH_DATA_HIDDEN:
					data_obj.hidden = data[3]
				MECH_DATA_STEALTH_MODE:
					data_obj.stealth_mode = data[3]
				MECH_DATA_DRILL:
					data_obj.drill = data[3]
				MECH_DATA_AIM:
					data_obj.aim = data[3]
				_:
					acceptable = false
		
		UN_WEAPON_DATA:
			
			var acceptable = true
			if players[data[0]].mech == null:
				state.print_log("Receiving data from player that not respawned yet!")
				return
			var data_obj = players[data[0]].mech.weapon
			
			match data[2]:
				
				WEAPON_DATA_SHOOT:
					data_obj.shoot = data[3]
				_:
					acceptable = false
		
		UN_PROP_DATA:
			
			if !props.has(data[2]):
				#state.print_log("Receiving data from prop that not spawned!")
				return
			
			match data[3]:
				
				PROP_POS: 
					props[data[2]].pos = data[4]
				PROP_ROT: 
					props[data[2]].rot = data[4]
				PROP_VEL: 
					props[data[2]].vel = data[4]
				PROP_ANG: 
					props[data[2]].ang = data[4]

# players management
func new_player(id=-1,nickname="UNKNOWN",team=-1):
	
	var p = player.new()
	
	if id == -1:
		p.id = network_helper.enet_id
		p.nickname = network_helper.nickname
		p.team = team
		me = p
	else:
		p.id = id
		p.nickname = nickname
		p.team = team
	
	players[p.id] = p
	
	emit_signal("update_tab_menu")

func delete_player(id):
	
	if $players.has_node(str(id)):
		get_node("players/"+str(id)).queue_free()
	players.erase(id)
	
	emit_signal("update_tab_menu")

func reset_mech(id,data):
	
	respawn(id)
	
	for key in data.keys():
		
		var val = data[key]
		
		match key:
			
			"weapon":
				
				for weapon_key in data["weapon"]:
					
					val = data[key][weapon_key]
					
					players[id].mech.weapon.set(weapon_key,val)
			
			_:
				players[id].mech.set(key,val)
	
	$players.get_node(str(id)).update_hitbox()

# exit routine
func exit(win=-1):
	set_process(false)
	if connection:
		connection.disconnect_from_host()
		get_tree().set_network_peer(null)
		connection = false
	
	var fallback
	if win > -1:
		fallback = load("res://assets/scenes/after_battle.tscn").instance()
		fallback.who_win = win
		fallback.ip = loaded_ip
		fallback.port = loaded_port
		fallback.nickname = loaded_nickname
	else:
		fallback = load("res://assets/scenes/menu.tscn").instance()
	$"/root".add_child(fallback)
	queue_free()

# gamish funcs
func spawn_seat(id):
	
	delete_seat(id)
	if players[id].seat == null:
		emit_signal("seat",$seats.get_children())
		print($seats.get_children())
		return
	
	var seat = load("res://assets/scenes/seat.tscn")
	seat = seat.instance()
	
	seat.name = str(id)
	seat.position = players[id].seat
	seat.team = players[id].team
	
	$seats.add_child(seat)
	
	if id == network_helper.enet_id:
		emit_signal("me_dead")
	
	emit_signal("seat",$seats.get_children())

func delete_seat(id):
	
	if !$seats.has_node(str(id)): return
	
	$seats.get_node(str(id)).queue_free()
	yield(get_tree(),"idle_frame")
	emit_signal("seat",$seats.get_children())

func respawn(id=-1):
	
	var mech = load("res://assets/scenes/mech.tscn")
	mech = mech.instance()
	
	var player_obj = null
	
	if id == -1:
		mech.name = str(network_helper.enet_id)
		player_obj = players[network_helper.enet_id]
		peer.put_var([PLAYER_DATA, PD_RESPAWN])
	else:
		mech.name = str(id)
		player_obj = players[id]
	
	player_obj.mech = mech_object.new()
	player_obj.mech.is_master = (id == -1)
	
	process_cfg(cfg, mech)
	
	$players.add_child(mech)
	if id == -1: 
		id = network_helper.enet_id
		me_mech = get_node("players/"+str(id))
		emit_signal("me_respawn")
	

func team(id,team):
	
	if id != network_helper.enet_id: return
	
	var color = Color(1,1,1)

	match team:
		0:
			color = Color(1,0.15,0.1)
		1:
			color = Color(0.15,0.1,1)

	for i in $"/root/game/bg".get_children():
		i.get_node("tex").modulate = color.lightened(0.6)
	VisualServer.set_default_clear_color(Color(0.2,0.2,0.2))

func process_explode(pos, radius, dmg):
	
	peer.put_var([EXPLOSION, pos, radius, dmg])

func process_explode_area(obj, dmg, damage_everything):
	
	var target = null
	var target_part = null
	
	if is_instance_valid(obj):
		target = obj.get_node("../..").name
		target_part = match_target_part(obj.name)
	
	match target:
		
		"points":
			# process point hit request to server
			var point = obj.get_node("..")
			if point.hp > 0:
				peer.put_var(
				[POINT, POINT_HIT, dmg, int(point.name)]
				)
		
		"game":
			
			if target_part is String:
				if target_part.begins_with("MEGUMIN"):
					obj.blow_up()
		
		_: 
		
			# process hit if target looks like player id
			target = int(target)
			
			if players.has(target):
				
				# if smth unexpected - prevent crash
				if players[target].mech == null: return
				
				if target_part == HB_CE:
					peer.put_var([HIT,target,target_part,2,damage_everything])
				else:
					peer.put_var([HIT,target,target_part,dmg,damage_everything])
				

func match_target_part(n):
	var result
	
	match n:
		"lt": result = HB_LT
		"rt": result = HB_RT
		"lb": result = HB_LB
		"rb": result = HB_RB
		"c": result = HB_CE
		_: result = n
	
	if n.begins_with("shield"):
		result = SHIELD
	
	return result

func process_cfg(cfg, mech):
	
	if validate_json(cfg): return
	var cfg_json = parse_json(cfg)
	
	for key in cfg_json.keys():
		
		match key:
			
			"mass" : if mech.MASS == 0: mech.MASS = cfg_json[key]
			"fuel" : if mech.JET_FUEL_MAX == 0: mech.JET_FUEL_MAX = cfg_json[key]
			"walk_speed" : 
				if mech.WALK_MAX_SPEED == 0: mech.WALK_MAX_SPEED = cfg_json[key]
				if mech.WALK_SPEED == 0: mech.WALK_SPEED = cfg_json[key] - 100
			"jump_speed" : if mech.JUMP_SPEED == 0: mech.JUMP_SPEED = cfg_json[key]
			
			"part_hp" : if mech.HITBOX_DEFAULT_BIG == 0: mech.HITBOX_DEFAULT_BIG = cfg_json[key]
			"center_hp" : if mech.HITBOX_DEFAULT_CENTER == 0: mech.HITBOX_DEFAULT_CENTER = cfg_json[key]
			
			"default_dmg" : if mech.get_node("weapon").DMG == 0: mech.get_node("weapon").DMG = cfg_json[key]
			"default_spread" : if mech.get_node("weapon").SPREAD == 0: mech.get_node("weapon").SPREAD = cfg_json[key]
			
			"rocket_force" : if mech.get_node("weapon").ROCKET_FORCE == 0: mech.get_node("weapon").ROCKET_FORCE = cfg_json[key]
			"grenade_force" : if mech.get_node("weapon").GRENADE_FORCE == 0: mech.get_node("weapon").GRENADE_FORCE = cfg_json[key]
			
			"default_ammo" : if mech.get_node("weapon").MAX_AMMO == 0: mech.get_node("weapon").MAX_AMMO = cfg_json[key]
			
			"firearm_ammo_size" : mech.get_node("weapon").FIREARM_AMMO_SIZE = cfg_json[key]
			"snipegun_ammo_size" : mech.get_node("weapon").SNIPEGUN_AMMO_SIZE = cfg_json[key]
			"shotgun_ammo_size" : mech.get_node("weapon").SHOTGUN_AMMO_SIZE = cfg_json[key]
			"rocket_ammo_size" : mech.get_node("weapon").ROCKET_AMMO_SIZE = cfg_json[key]
			"laser_ammo_size" : mech.get_node("weapon").LASER_AMMO_SIZE = cfg_json[key]
			
			"firearm_recoil" : mech.get_node("weapon").FIREARM_RECOIL = cfg_json[key]
			"snipegun_recoil" : mech.get_node("weapon").SNIPEGUN_RECOIL = cfg_json[key]
			"shotgun_recoil" : mech.get_node("weapon").SHOTGUN_RECOIL = cfg_json[key]
			"rocket_recoil" : mech.get_node("weapon").ROCKET_RECOIL = cfg_json[key]
			"laser_recoil" : mech.get_node("weapon").LASER_RECOIL = cfg_json[key]
			"grenade_recoil" : mech.get_node("weapon").GRENADE_RECOIL = cfg_json[key]
			
			"firearm_reload" : mech.get_node("weapon").FIREARM_RELOAD = cfg_json[key]
			"snipegun_reload" : mech.get_node("weapon").SNIPEGUN_RELOAD = cfg_json[key]
			"shotgun_reload" : mech.get_node("weapon").SHOTGUN_RELOAD = cfg_json[key]
			"rocket_reload" : mech.get_node("weapon").ROCKET_RELOAD = cfg_json[key]
			"laser_reload" : mech.get_node("weapon").LASER_RELOAD = cfg_json[key]
			 
			"firearm_angle" : mech.get_node("weapon").FIREARM_ANGLE= cfg_json[key]
			"snipegun_angle" : mech.get_node("weapon").SNIPEGUN_ANGLE = cfg_json[key]
			"shotgun_angle" : mech.get_node("weapon").SHOTGUN_ANGLE = cfg_json[key]
			"rocket_angle" : mech.get_node("weapon").ROCKET_ANGLE = cfg_json[key]
			"grenade_angle" : mech.get_node("weapon").GRENADE_ANGLE = cfg_json[key]
			"laser_angle" : mech.get_node("weapon").LASER_ANGLE = cfg_json[key]

# utility
func sort_modules(a):
	var new_a = []
	var last_id = 0
	var att = 0
	
	while true:
		for jm in a:
			if jm["id"] == last_id:
				new_a.append(jm)
				last_id += 1
				att = 0
		att+=1
		if att > 2:
			break
	
	return new_a