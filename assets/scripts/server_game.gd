extends "res://assets/scripts/net_constants.gd"

var respawn_timers = Dictionary()
var teams = [ [],[] ]
var timer = -1
var game_mode = GAME_MODE_NONE
signal feed

# server vars

var connection
var connection_by_id
var client_by_id

# tilemap

var tilemap_hit = {}

# props

var prop_counter = 0
var prop_name_counter = 0

# objects

class seat:
	var position = Vector2()
	var team = -1
	var placed = null

class point:
	var state = -1
	var time = -1
	var capturers = []
	var hp = -1

var seats = []

# game_stage

var game_stage = -1
var last_game_timer_stage = -1

# points 

var points = {}

# team_points 

var team_points = {
	TEAM_POINTS_RED : 0,
	TEAM_POINTS_BLU : 0,
	TEAM_POINTS_RED_BONUS : 0,
	TEAM_POINTS_BLU_BONUS : 0,
	TEAM_POINTS_MAX : 0
}
var team_points_last = -1

### MEGUMINS

var DESTROYED_MEGUMINS = []

# timers messing =========================

func _process(delta):
	
	# game_timers
	
	match game_mode:
		
		GAME_MODE_WAIT:
			
			timer -= delta
			if last_game_timer_stage != -1:
				last_game_timer_stage = -1
				update_game_stage()
			
			if timer < 0:
				
				game_mode = GAME_MODE_MATCH
				timer = TIMER_MATCH_TIME
			
			broadcast_timer()
			
		GAME_MODE_MATCH:
			
			timer -= delta
			if int(timer/GAME_STAGE_DURATION) != last_game_timer_stage:
				last_game_timer_stage = int(timer/GAME_STAGE_DURATION)
				update_game_stage()
			
			if timer < 0:
				
				game_mode = GAME_MODE_PAY_RESPECT
				timer = TIMER_F_TIME
			
			broadcast_timer()
			
		GAME_MODE_PAY_RESPECT:
			
			timer -= delta
			if last_game_timer_stage != 0:
				last_game_timer_stage = 0
				update_game_stage()
			
			if timer < 0:
				
				var winner = NO_PLAYERS
				if team_points[TEAM_POINTS_BLU] > team_points[TEAM_POINTS_RED]:
					winner = TEAM_BLU
				elif team_points[TEAM_POINTS_RED] > team_points[TEAM_POINTS_BLU]:
					winner = TEAM_RED
				
				for client in connection:
					connection[client].peer.put_var([FEED, FEED_WIN, [winner]])
				$"..".exit()
			
	# respawn timer
	
	for i in respawn_timers.keys():
		respawn_timers[i] -= delta
		if connection.keys().has(i):
			connection[i].peer.put_var([RESPAWN, respawn_timers[i]])
		else: 
			respawn_timers.erase(i)
			continue
		if respawn_timers[i] < 0 and game_mode != GAME_MODE_WAIT:
			connection[i].peer.put_var([RESPAWN,-1])
			respawn_timers.erase(i)
	
	for client in connection:
		# alive timer accum
		if connection[client].alive_time > -1:
			connection[client].alive_time += delta
		
		# player kill when out of map
		if connection[client].mech != null:
			if connection[client].mech["pos"].y > VOID_Y_LEVEL:
				broadcast_kill(client)
				emit_signal("feed",FEED_KILL,[connection[client].id,-1,-1,-1])
	
	# points_timers
	
	for idx in points.keys():
		
		if points[idx].state in [POINT_STATE_ACTIVE, POINT_STATE_CAP_BLU, POINT_STATE_CAP_RED]:
			update_point( idx, delta )
		
		broadcast_point_state( idx, points[idx].state )
		broadcast_point_hp( idx, points[idx].hp )
		broadcast_point_time( idx, points[idx].time )
		

# process packets =====================

func process_packet(data,client):
		
		match data[0]:
			
			PLAYER_DATA:
			
				match data[1]:
					
					PD_RESPAWN:
						
						if connection[client].seat != null:
							connection[client].peer.put_var([PLAYER_DATA, PD_MOVE, connection[client].seat.position])
							connection[client].alive_time = 0.0
							broadcast_respawn(client)
							
							emit_signal("feed",FEED_RESPAWN,[connection[client].id])
					
					PD_HEAL:
						
						var hitbox_part = data[2]
						var heal_amount = data[3]
						
						connection[client].mech["hitbox"][hitbox_part] += heal_amount
						broadcast_heal(client, hitbox_part, heal_amount)
			
			HIT:
				
				var id = data[1]
				var part = data[2]
				var dmg = data[3]
				var damage_everything = false
				if data.size() > 4:
					damage_everything = data[4]
				
				if connection_by_id[id].mech != null:
					if ((connection[client].team != connection_by_id[id].team or damage_everything) and 
					connection_by_id[id].alive_time > INV_TIME):
						if part is int:
							if part < SHIELD:
								connection_by_id[id].mech["hitbox"][part] -= dmg
							if part == HB_CE and connection_by_id[id].mech["hitbox"][part] < 1:
								broadcast_kill(client_by_id[id])
								emit_signal("feed",FEED_KILL,[id,part,dmg,connection[client].id])
							else:
								broadcast_hit(client,id,part,dmg)
								emit_signal("feed",FEED_HIT,[id,part,dmg,connection[client].id])
			
			POINT:
				
				match data[1]:
					
					POINT_ENTER:
						
						var id = data[2]
						
						if points.has(id):
							if !client in points[id].capturers:
								points[id].capturers.append(client)
						
					POINT_EXIT:
						
						var id = data[2]
						
						if points.has(id):
							if points[id].capturers.has(client):
								points[id].capturers.erase(client)
						
					POINT_HIT:
						
						var id = data[3]
						var dmg = data[2]
						
						if points.has(id):
							if ((points[id].state == POINT_STATE_CAPPED_RED and 
							connection[client].team == TEAM_BLU) or
							 (points[id].state == POINT_STATE_CAPPED_BLU and 
							connection[client].team == TEAM_RED)):
								points[id].hp -= dmg
								broadcast_point_hp(id,points[id].hp)
								if points[id].hp < 1:
									points[id].state = POINT_STATE_DEACTIVE
									points[id].time = -1
									points[id].hp = -1
									broadcast_point_state(id, POINT_STATE_DEACTIVE)
									broadcast_point_time(id, -1)
									broadcast_point_hp(id, -1)
									update_points()
									emit_signal("feed",FEED_PDEST,[id,connection[client].id])
						
			
			EXPLOSION:
				
				var pos = data[1]
				var radius = data[2]
				var dmg = data[3]
				var megumin_to_blow = "IS_THERE_MEGUMIN"
				if data.size() > 4:
					megumin_to_blow = data[4]
				
				var wtm_pos = $"../map".world_to_map(pos)
				
				circle_for_explosion(int(wtm_pos.x), int(wtm_pos.y), int(radius/$"../map".cell_size.x), 0.05)
				
				for c in connection:
					connection[c].peer.put_var([EXPLOSION, pos, radius, dmg, connection[client].id, megumin_to_blow])
				
				if megumin_to_blow.begins_with("MEGUMIN"):
					DESTROYED_MEGUMINS.append(megumin_to_blow)
					for c in connection:
						connection[c].peer.put_var([MEGUMINS,DESTROYED_MEGUMINS])
			
			TILEMAP_HIT:
				
				var cell = data[1]
				var dmg = data[2]
				var valid = false
				
				var cell_id_map = $"../map".get_cellv(cell)
				var cell_id_struct = $"../map/structures".get_cellv(cell)
				
				if cell_id_struct in HARD_CELLS: return
				if $"../map".get_cellv(cell) == -1: return
				
				if tilemap_hit.has(cell):
					if tilemap_hit[cell] > 0: 
						tilemap_hit[cell] -= dmg
						valid = true
				else:
					tilemap_hit[cell] = TILEMAP_HP-dmg
					valid = true
				
				if tilemap_hit[cell] < 1:
					$"../map".set_cellv(cell,-1)
				
				if valid:
					broadcast_tilemap_hit(cell)
				
			PROP:
				
				var pos = data[1]
				var rot = data[2]
				var vel = data[3]
				var ang = data[4]
				
				var tex = data[5]
				var col = data[6]
				
				broadcast_prop("prop_"+str(prop_name_counter),pos,rot,vel,ang,tex,col)
				
				prop_name_counter += 1
				
# update smth funcs =================

func update_feed( feed_type, feed_data ):
	
	if game_mode in [GAME_MODE_NONE, GAME_MODE_WAIT]: return
	
	var feed_msg = "[ FEED ] "
	var send_feed_msg = true
	var client_update_mony = []
	var client_update_stats = []
	
	match feed_type:
		
		FEED_RESPAWN:
			
			var id = feed_data[0]
			
			feed_msg += (connection_by_id[id].nickname +
			 " is respawned!")
			
		FEED_KILL:
			
			var killed_id = feed_data[0]
			var part = feed_data[1]
			var dmg = feed_data[2]
			var who_kill_id = feed_data[3]
			
			if who_kill_id == -1:
				# format message
				feed_msg += (connection_by_id[killed_id].nickname 
				+ " fall away")
				
				# update stats
				client_update_stats.append(client_by_id[killed_id])
				connection_by_id[killed_id].stats[STATS_DEATHS] += 1
			
			elif connection_by_id[killed_id].team == connection_by_id[who_kill_id].team:
				pass
			
			else:
				# format message
				feed_msg += (connection_by_id[killed_id].nickname 
				+ " is killed by " + connection_by_id[who_kill_id].nickname)
				
				# count and apply mony reward
				var reward = int(PRICELIST[feed_type].get(game_stage,0))
				var reward_team = connection_by_id[who_kill_id].team
				
				connection_by_id[who_kill_id].mony += reward
				client_update_mony.append(client_by_id[who_kill_id])
				
				# update stats
				connection_by_id[who_kill_id].stats[STATS_KILLS] += 1
				connection_by_id[killed_id].stats[STATS_DEATHS] += 1
				
				client_update_stats.append(client_by_id[who_kill_id])
				client_update_stats.append(client_by_id[killed_id])
				
				# apply reward to team points
				if team_points.has(reward_team):
					team_points[reward_team] += reward
					team_points_last = reward_team
			
		FEED_HIT:
			
			var hitted_id = feed_data[0]
			var part = feed_data[1]
			var dmg = feed_data[2]
			var who_hit_id = feed_data[3]
			send_feed_msg = false
			
			if who_hit_id == -1: 
				pass
			
			elif connection_by_id[hitted_id].team == connection_by_id[who_hit_id].team:
				pass
			
			else:
				# count and apply reward
				var reward = int(dmg)
				var reward_team = connection_by_id[who_hit_id].team
				
				connection_by_id[who_hit_id].mony += reward
				client_update_mony.append(client_by_id[who_hit_id])
				
				# apply reward to team points
				if team_points.has(reward_team):
					var team_name = "WHAT"
					match connection_by_id[who_hit_id].team: #id -> who_hit_id
						TEAM_RED:
							team_name = "RED"
						TEAM_BLU:
							team_name = "BLU"
					team_points[reward_team] += reward
					team_points_last = reward_team
			
		FEED_CAP:
			
			var point_idx = feed_data[0]
			var cap_team = feed_data[1]
			var capturers = feed_data[2]
			
			# build feed message parts
			var capturers_names = ""
			var capturers_ids = []
			var team_name = "UNKNOWN"
				# add all the nicknames
			for client in capturers:
				capturers_names += connection[client].nickname + ", "
				# trim names
			if capturers_names.length() > 0:
				capturers_names.left(capturers_names.length() - 2)
			else:
				capturers_names = "SOMEONE ELSE"
				# team name
			match cap_team:
				TEAM_RED:
					team_name = "RED"
				TEAM_BLU:
					team_name = "BLU"
			
			# format message
			feed_msg += (str(point_idx) + " point is captured by " + 
			capturers_names + " from team " + team_name)
			
			# count and apply reward/stats
			var reward = int(PRICELIST[feed_type])
			for client in capturers:
				connection[client].mony += reward
				connection[client].stats[STATS_CAPS] += 1
			
			client_update_mony = capturers
			client_update_stats = capturers
			
			# apply reward to team points
			if team_points.has(cap_team):
				team_points[cap_team] += reward
				team_points_last = cap_team
			
			# replace client objects to ids 
			# for sending across the clients
			for client in capturers:
				capturers_ids.append(connection[client].id)
			
			feed_data[2] = capturers_ids
			
		FEED_WIN:
			
			var team = feed_data[0]
			
			var team_name = "WHAT"
			match team:
				TEAM_RED:
					team_name = "RED"
				TEAM_BLU:
					team_name = "BLU"
				NO_PLAYERS:
					team_name = "NAN"
			
			feed_msg += "TEAM " + team_name + " IS WINS!"
			
		FEED_PDEST:

			var point_idx = feed_data[0]
			var id = feed_data[1]
			
			var cap_team = connection_by_id[id].team
			var team_name = "WHAT"
			match cap_team:
				TEAM_RED:
					team_name = "RED"
				TEAM_BLU:
					team_name = "BLU"
			
			var reward = int(PRICELIST[feed_type])
			connection_by_id[id].mony += reward
			
			if team_points.has(cap_team):
				team_points[cap_team] += reward
				team_points_last = cap_team
			
			feed_msg += (str(point_idx) + " point is destroyed by " + connection_by_id[id].nickname + " from team " + team_name)

	
	update_team_points()
	
	# send mony, stats, feed
	for client in client_update_mony:
		$"../modules".send_mony_data(client)
	
	for client in client_update_stats:
		for c in connection:
			connection[c].peer.put_var([STATS,connection[client].id,connection[client].stats])
	
	for client in connection:
		connection[client].peer.put_var([FEED, feed_type, feed_data])
	
	# print feed msg
	if send_feed_msg:
		state.print_log(feed_msg)

func update_team_points():
	
	var team_points_sum = team_points[TEAM_POINTS_RED] + team_points[TEAM_POINTS_BLU]
	var team_points_last_opp = 1 - team_points_last
	
	if (team_points_sum > team_points[TEAM_POINTS_MAX] and 
	team_points_last > -1):
		team_points[team_points_last_opp] -= team_points_sum - team_points[TEAM_POINTS_MAX]
	
	for key in range(2):
		# TEAM_RED, TEAM_BLU
		team_points[key] = clamp(team_points[key],0,team_points[TEAM_POINTS_MAX])
		
		# BONUS_RED, BONUS_BLU
		var team_points_diff = (team_points[1 - key] - team_points[key])
		var players_count_diff = ((teams[1 - key].size() - teams[key].size()) * 
		TEAM_POINTS_PLAYER_PRICE * game_stage)
		
		team_points[key + 2] = team_points_diff + players_count_diff
	
	for client in connection:
		connection[client].cred = clamp(team_points.get(connection[client].team + 2,0),0,INF)
	
	for client in connection:
		connection[client].peer.put_var([TEAM_POINTS, team_points])

func update_point( idx , delta ):
	
	var point = points[idx]
	var cap_team = -1
	
	var weight = 0
	
	var cap_team_math_score = 0
	for client in point.capturers:
		
		# if player disconnect
		if !connection.has(client):
			point.capturers.erase(client)
			continue
		
		# if player dead
		if connection[client].alive_time < 0:
			point.capturers.erase(client)
			continue
		
		if cap_team != connection[client].team:
			cap_team_math_score+=1
			cap_team = connection[client].team
		if cap_team_math_score > 1:
			weight = 0
			break
		
		weight += connection[client].cap_weight
	
	if weight > 0:
		point.time -= delta * weight
		point.state = 2 + cap_team # where STATE_CAP_RED is 2 and BLU is 3 (cap_team 0 or 1)
		if point.time < 0:
			point.time = -1
			point.state = 4 + cap_team # where STATE_CAPPED_RED is 4 and BLU is 5 (cap_team 0 or 1)
			emit_signal("feed",FEED_CAP,[idx, cap_team, point.capturers])
		
		broadcast_point_state( idx, point.state )
		broadcast_point_time( idx, point.time )
	
	else:
		if point.time < DEFAULT_POINT_CAP_TIME:
			point.time += delta
			broadcast_point_time( idx, point.time )
			if point.state != POINT_STATE_ACTIVE:
				point.state = POINT_STATE_ACTIVE
				broadcast_point_state( idx, point.state )
			if point.time > DEFAULT_POINT_CAP_TIME:
				point.time = DEFAULT_POINT_CAP_TIME
				broadcast_point_time( idx, point.time )
	
func update_game_stage():
	
	if game_mode != GAME_MODE_MATCH: return
	
	if last_game_timer_stage != -1:
		
		var stg = 4 - last_game_timer_stage
		var weight = clamp(int(connection.size() / GAME_STAGE_WEIGHT_PL) + 1,0,4)
		
		if stg > weight:
			game_stage = stg
		else:
			game_stage = weight
		
	else: game_stage = -1
	
	if game_stage in MAX_TEAM_POINTS.keys():
		team_points[TEAM_POINTS_MAX] = MAX_TEAM_POINTS[game_stage]
	else:
		team_points[TEAM_POINTS_MAX] = 0
	
	for client in connection:
		connection[client].peer.put_var([TEAM_POINTS, team_points])
	
	update_points()
	
func update_points():
	
	if points.size() == 0: return
	
#	var points_active_count = points.size()/2
	var points_active_count = clamp(
	int(
	(float(points.size()) / 100) * 
	GAME_STAGE_POINTS_MIN_PERCENT * game_stage),
	1,INF
	)
	
	for p in points:
		if points[p].state > POINT_STATE_DEACTIVE:
			if points_active_count > 0:
				points_active_count -= 1
	
	for i in range(points_active_count):
		
		while true:
			var idx = randi()%points.size()
			if points[points.keys()[idx]].state == POINT_STATE_DEACTIVE:
				points[points.keys()[idx]].state = POINT_STATE_ACTIVE
				points[points.keys()[idx]].hp = DEFAULT_POINT_HP
				points[points.keys()[idx]].time = DEFAULT_POINT_CAP_TIME
				broadcast_point_time(idx, points[points.keys()[idx]].time)
				broadcast_point_state(idx, points[points.keys()[idx]].state)
				broadcast_point_hp(idx,points[points.keys()[idx]].hp)
				break

func soft_reset():
	
	DESTROYED_MEGUMINS = []
	
	for cell in tilemap_hit.keys():
		$"../map".set_cellv(cell,0)
	
	tilemap_hit = {}
	
	team_points = {
		TEAM_POINTS_RED : 0,
		TEAM_POINTS_BLU : 0,
		TEAM_POINTS_RED_BONUS : 0,
		TEAM_POINTS_BLU_BONUS : 0,
		TEAM_POINTS_MAX : 0
	}
	
	for client in connection:
		connection[client].peer.put_var([PLAYER_DATA, PD_MASS_RESET])
		connection[client].mony = 0
		connection[client].cred = 0
		connection[client].modules = {}
		connection[client].modules_order = []
		connection[client].modules_c = {}
		connection[client].peer.put_var([PLAYER_DATA, PD_MODULES_ORDER, connection[client].id, connection[client].modules_order.duplicate()])
		connection[client].peer.put_var([PLAYER_DATA, PD_MODULE, connection[client].id, connection[client].modules.duplicate()])
		$"../modules".send_mony_data(client)
	
	for client in connection:
		connection[client].peer.put_var([TEAM_POINTS, team_points])
		connection[client].stats = [0,0,0]
		for c in connection:
			connection[c].peer.put_var([STATS, connection[c].id, connection[c].stats])
	
	for client in connection:
		connection[client].peer.put_var([MEGUMINS,DESTROYED_MEGUMINS])

# broadcast funcs ========================

func broadcast_point_time(idx, time):
	var data = [POINT, POINT_CAP, time, idx]
	for client in connection:
		connection[client].peer.put_var(data)

func broadcast_point_hp(idx, hp):
	var data = [POINT, POINT_HP, hp, idx]
	for client in connection:
		connection[client].peer.put_var(data)

func broadcast_point_state(idx, state):
	var data = [POINT, POINT_STATE, state, idx]
	for client in connection:
		connection[client].peer.put_var(data)

func broadcast_timer():
	var data = [TIMER, timer, game_mode]
	for client in connection:
		connection[client].peer.put_var(data)

func broadcast_delete_prop(what=""):
	
	var data = [PROP,"del"]
	
	if what in ["","all"]:
		
		for prop_obj in $"../map".get_children():
			if prop_obj.name in ["Camera2D","structures", "seats"]: continue
			
			data.append(prop_obj.name)
			prop_obj.queue_free()
			if what == "":
				break
	
	else:
		
		if $"../map".has_node(what):
			data.append(what)
			$"../map".get_node(what).queue_free()
		else:
			return
	
	for client in connection:
		connection[client].peer.put_var(data)

func broadcast_prop(prop_name,pos,rot,vel,ang,tex,col):
	var data = [PROP,prop_name,pos,rot,vel,ang,tex,col]
	for client in connection:
		connection[client].peer.put_var(data)

func broadcast_tilemap_hit(cell):
	var data = [TILEMAP_HIT, cell, tilemap_hit[cell]]
	for client in connection:
		connection[client].peer.put_var(data)

func broadcast_heal(who, part, amount):
	var data = [PLAYER_DATA, PD_HEAL, part, amount, connection[who].id]
	for client in connection:
		connection[client].peer.put_var(data)

func broadcast_hit(who,id,part,dmg):
	var data = [HIT, id, part, dmg, connection[who].id]
	for client in connection:
		connection[client].peer.put_var(data)

func broadcast_kill(who):
	var data = [PLAYER_DATA, PD_KILL, connection[who].id]
	for client in connection:
		connection[client].peer.put_var(data)
	connection[who].alive_time = -1
	server_player_connect(who,false)
	$"../modules".server_player_connect(who,false)

func broadcast_respawn(who):
	var data = [PLAYER_DATA, PD_RESPAWN, connection[who].id]
	for client in connection:
		if client == who:
			continue
		connection[client].peer.put_var(data)

func broadcast_seat(who):
	var data = [PLAYER_DATA, PD_SEAT, connection[who].id, connection[who].seat.position]
	for client in connection:
		connection[client].peer.put_var(data)

# send funcs ============================

func send_cfg(who):
	var data = [CFG]
	
	var f = File.new()
	if f.open("res://assets/json/main.json",File.READ) == OK:
		var cfg = f.get_as_text()
		data.append(cfg)
		connection[who].peer.put_var(data)
	

func send_points(who):
	var data
	for i in points:
		data = [POINT, POINT_STATE, points[i].state, i]
		connection[who].peer.put_var(data)
		data = [POINT, POINT_CAP, points[i].time, i]
		connection[who].peer.put_var(data)
		data = [POINT, POINT_HP, points[i].hp, i]
		connection[who].peer.put_var(data)

func send_tilemap_hits(who):
	var data = [TILEMAP_HIT, tilemap_hit]
	connection[who].peer.put_var(data)
	connection[who].peer.put_var([MEGUMINS,DESTROYED_MEGUMINS,42])

func send_seats(who):
	var data = [PLAYER_DATA, PD_SEAT, []]
	for client in connection:
		if client == who:
			continue
		data[2].append([connection[client].id, connection[client].seat.position])
	connection[who].peer.put_var(data)

func send_props(who):
	var data = [PROP,"init",[]]
	
	for p in $"../map".get_children():
		
		if p.name in ["Camera2D", "structures", "seats"]: continue
		
		data[2].append([p.name,p.position,p.rotation,p.linear_velocity,p.angular_velocity,p.data_tex,p.data_col])
	
	connection[who].peer.put_var(data)

func assign_seat(who,delete=false):
	
	if !delete:
		for i in seats:
			if i.team == connection[who].team and i.placed == null:
				i.placed = connection[who]
				connection[who].seat = i
				break
		
		var s = load("res://assets/scenes/seat.tscn")
		s = s.instance()
		
		s.name = str(connection[who].id)
		s.position = connection[who].seat.position
		
		$"../map/seats".add_child(s)
		
	else:
		for i in seats:
			if i.placed == connection[who]:
				i.placed = null
		
		if $"../map/seats".has_node(str(connection[who].id)):
			$"../map/seats".get_node(str(connection[who].id)).queue_free()

func assign_team(who,delete=false):
	
	if delete:
		for i in teams:
			if i.has(who):
				i.erase(who)
		
		return
	
	var new = true
	
	for i in teams:
		if i.has(who):
			new = false
			connection[who].team = teams.find(i)
	
	if new:
		if teams[TEAM_RED].size() > teams[TEAM_BLU].size():
			teams[TEAM_BLU].append(who)
			connection[who].team = TEAM_BLU
		elif teams[TEAM_BLU].size() > teams[TEAM_RED].size():
			teams[TEAM_RED].append(who)
			connection[who].team = TEAM_RED
		else:
			var t = randi()%2
			teams[t].append(who)
			connection[who].team = t
	
	# else: here goes team swap due low performance
	
	var data = [TEAM, connection[who].id, connection[who].team]
	
	for client in connection:
		connection[client].peer.put_var(data)

func set_cell(x,y):
	process_packet([TILEMAP_HIT,Vector2(x,y),TILEMAP_HP - randi()%(TILEMAP_HP/2)],null)

func circle_for_explosion(x0, y0, radius, d=1):
	var x = radius-1
	var y = 0
	var dx = d
	var dy = d
	var err = dx - (radius << 1)

	while x >= y:
		
		for i in range(x):
			set_cell(x0 + i, y0 + y)
			set_cell(x0 + y, y0 + i)
			set_cell(x0 - y, y0 + i)
			set_cell(x0 - i, y0 + y)
			set_cell(x0 - i, y0 - y)
			set_cell(x0 - y, y0 - i)
			set_cell(x0 + y, y0 - i)
			set_cell(x0 + i, y0 - y)
			
		if err <= 0:
			y+=1
			err += dy
			dy += 2
        
		if (err > 0):
			x-=1
			dx += 2
			err += dx - (radius << 1)

# server funcs ==============================

func server_player_connect(who,first=true):
	
	if first:
		assign_team(who)
		send_cfg(who)
		assign_seat(who)
		broadcast_seat(who)
		send_tilemap_hits(who)
		send_seats(who)
		send_props(who)
		send_points(who)
	
	if game_mode == GAME_MODE_WAIT:
		respawn_timers[who] = timer
		if first:
			connection[who].mony = 400
			$"../modules".send_mony_data(who)
	elif game_mode == GAME_MODE_NONE:
		respawn_timers[who] = 1
	else:
		
		if first:
			
			var reward = 0
			
			match game_stage:
				1: reward = 400
				2: reward = 800
				3: reward = 1600
				4: reward = 3200
			
			connection[who].mony = reward
			$"../modules".send_mony_data(who)
		
		respawn_timers[who] = DEFAULT_PLAYER_RESPAWN_TIME
	connection[who].mech = DEFAULT_MECH.duplicate(true)
	
	if (connection.size() >= MIN_PLAYERS_FOR_START and game_mode == GAME_MODE_NONE and first):
		game_mode = GAME_MODE_WAIT
		timer = TIMER_WAIT_TIME
		soft_reset()
		for client in connection:
			broadcast_kill(client)
			connection[client].mony = 400
			$"../modules".send_mony_data(client)
		
	broadcast_timer()
	update_game_stage()

func server_player_disconnect(who):
	
	assign_seat(who,true)
	assign_team(who,true)
	
	if respawn_timers.has(who): respawn_timers.erase(who)
	
	if (connection.size() <= MIN_PLAYERS_FOR_START):
		for client in connection:
			connection[client].peer.put_var([FEED, FEED_WIN, [NO_PLAYERS]])
		$"..".exit()

func server_reset():
	for client in connection:
		connection[client].peer.put_var([RESPAWN, 42]) # tell client about hot restart
		broadcast_kill(client)
	broadcast_delete_prop("all")
	
	update_game_stage()

func server_restart():
	
	seats = []
	tilemap_hit = {}
	
	for i in $"../map".seats:
		var s = seat.new()
		s.position = i[0]*32
		s.team = i[1]
		seats.append(s)
	
	for client in connection:
		assign_seat(client)
		broadcast_seat(client)
	
	prop_name_counter += 1
	
	points = {}
	for p in $"../map".structures_places:
		points[p[5]] = point.new()
		points[p[5]].state = POINT_STATE_DEACTIVE
		broadcast_point_state(p[5], points[p[5]].state)
	
	soft_reset()
	if connection.size() >= MIN_PLAYERS_FOR_START:
		game_mode = GAME_MODE_WAIT

func _ready():
	connect("feed",self,"update_feed")
	$"..".connect("player_connect",self,"server_player_connect")
	$"..".connect("player_disconnect",self,"server_player_disconnect")
	$"..".connect("reset",self,"server_reset")
	$"..".connect("restart",self,"server_restart")
	$"..".connect("other_packet",self,"process_packet")
