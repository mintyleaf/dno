extends Line2D

const RAY_MAX_DISTANCE = 16384

var ray = null
var out = Vector2(0,0)
var alive_time = -1

var first_frame = false
var exceptions = []

func _ready():
	$light.hide()

func _process(delta):
	
	if !first_frame:
		first_frame = true
	else:
		$light.show()
	
	if ray != null:
		
		points[0] = out
		
		if ray.is_colliding():
			points[1] = Vector2(ray.get_collision_point().distance_to(ray.global_position) + 8,out.y)
		else:
			points[1] = Vector2(RAY_MAX_DISTANCE,out.y)
		
		$light.position = points[0].linear_interpolate(points[1],0.5)
		$light.scale.x = points[0].distance_to(points[1])/64
		$light.rotation = points[0].angle_to_point(points[1])
		
		if (points[1].x - points[0].x) < 0: hide()
		else: show()
	
	if alive_time > 0:
		alive_time -= delta
		width-=delta*20
	else:
		for ex in exceptions:
			ray.remove_exception(ex)
		queue_free()

