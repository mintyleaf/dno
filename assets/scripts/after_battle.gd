extends Panel

const NO_ONE = -1
const TEAM_RED = 0
const TEAM_BLU = 1
const NO_PLAYERS = 2

const RETRY_TIME = 3.5
const MAX_RETRIES = 5

var who_win = -1

var ip = ""
var port = 10568
var nickname = "null"

var timer = RETRY_TIME
var retr = 0

func try_connect():
	OS.set_window_title("DNO aftermatch")
	var game = load("res://assets/scenes/game.tscn").instance()
	game.loaded_ip = ip
	game.loaded_port = port
	game.loaded_nickname = nickname
	game.after_battle = true
	$"/root".add_child(game)

func _process(delta):
	timer -= delta
	if timer < 0:
		try_connect()
		retr+=1
		if retr > 2:
			$rec.text = "seems like the server is lazy one. trying again..."
		if retr == MAX_RETRIES - 1:
			$rec.text = "okay. last try..."
		if retr == MAX_RETRIES:
			var main = load("res://assets/scenes/menu.tscn").instance()
			$"/root".add_child(main)
			queue_free()
		timer = RETRY_TIME

func _ready():
	OS.set_window_title("DNO aftermatch")
	$rec.text = "u'll be reconnected asap..."
	match who_win:
		NO_ONE:
			$cond.text = "spare?"
		TEAM_RED:
			$cond.text = "team red wins!"
		TEAM_BLU:
			$cond.text = "team blu wins!"
		NO_PLAYERS:
			$cond.text = "req minimum disconnected!"
	set_process(true)
	