extends Panel

func _ready():
	$"/root/game".connect("connecting",self,"connecting")
	$"/root/game".connect("connected",self,"connected")
	$"/root/game".connect("cantconnect",self,"cantconnect")
	$"/root/game".connect("disconnected",self,"disconnected")
	$"/root/game".connect("players",self,"players")
	$"/root/game".connect("map",self,"map")
	$"/root/game".connect("loading_end",self,"loading_end")
	show()

func connecting():
	$info.text = "Connecting to server..."

func connected():
	$info.text = "Connected to server!"

func cantconnect():
	$info.text = "Can't connect to server!"

func disconnected():
	show()
	$info.text = "Server disconnected!"

func players():
	$info.text = "Receiving players..."

func map(stage):
	
	var text_stage = ""
	
	match stage:
		"cells":
			text_stage = "receiving tiles..."
		"bg_cells":
			text_stage = "receiving background tiles..."
		"structure_cells":
			text_stage = "receiving structures..."
		"ore":
			text_stage = "receiving misc stuff..."
		"tnt":
			text_stage = "receiving misc stuff..."
		"end":
			text_stage = "drawing everything..."
		
	$info.text = "Receiving map: " + text_stage

func loading_end():
	hide()