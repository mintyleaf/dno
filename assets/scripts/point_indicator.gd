extends Node2D

var cur_point = null

const consts = preload("res://assets/scripts/net_constants.gd")

func get_indicator_pos(point: Object):
	#Center of the Rectangle
	var Cx = $"/root/game/camera".get_camera_screen_center().x
	var Cy = $"/root/game/camera".get_camera_screen_center().y
	#Width
	var w = get_viewport_rect().size.x * $"/root/game/camera".zoom.x
	# Height
	var h = get_viewport_rect().size.y * $"/root/game/camera".zoom.y

	# Other Point
	var Ax = point.position.x
	var Ay = point.position.y

	#Now translate point A by the center of the rectangle so the rect is centered in O(0,0) and consider the problem in the first quarter (i.e. x > 0 and y > 0).

	# Coordinates Translated
	var Px = abs(Ax - Cx)
	var Py = abs(Ay - Cy)

	# Slope of line from Point P to Center
	if Px == 0: # not( x / 0)
		return Vector2(NAN, NAN)
	var Pm = Py / Px

	# Slope of rectangle Diagonal
	var Rm = h / w

	# If the point is inside the rectangle, return the center
	var res = Vector2(NAN, NAN)

	# Check if the point is inside and if so do not calculate
	if (!(Px < w / 2 && Py < h / 2)):
		# Calculate point in first quarter: Px >= 0 && Py >= 0
		if (Pm <= Rm):
			res.x = w / 2
			res.y = (w * Pm) / 2
		else:
			res.x = h / (Pm * 2)
			res.y = h / 2

	# Set original sign 
	if (Ax - Cx < 0): res.x *= -1
	if (Ay - Cy < 0): res.y *= -1
	return Vector2(res.x + Cx, res.y + Cy)


func _process(delta):
	if $"/root/game".me != null:
		for indicator in $"/root/game/game_indicators".get_children():
			if indicator.name.begins_with('point_indicator'):
				cur_point = indicator.get_meta('point')
				if is_instance_valid(cur_point):
					if cur_point.get_parent() == $"/root/game/points":
						var pos = get_indicator_pos(cur_point) 
						if pos in [Vector2(NAN, NAN)]:
							indicator.hide()
						else:
							if cur_point.state != cur_point.POINT_STATE_DEACTIVE:

								if $"/root/game".me.team == consts.TEAM_RED:
									if cur_point.state in [cur_point.POINT_STATE_CAP_RED, cur_point.POINT_STATE_CAPPED_RED]:
										indicator.hide()
										return
									elif cur_point.state == cur_point.POINT_STATE_CAP_BLU:
										indicator.modulate = Color("#4358d4").lightened(0.5)
									elif cur_point.state == cur_point.POINT_STATE_CAPPED_BLU:
										indicator.modulate = Color("#4358d4")
									else:
										indicator.modulate = Color.white
										
								elif $"/root/game".me.team == consts.TEAM_BLU:
									if cur_point.state in [cur_point.POINT_STATE_CAP_BLU, cur_point.POINT_STATE_CAPPED_BLU]:
										indicator.hide()
										return
									elif cur_point.state == cur_point.POINT_STATE_CAP_RED:
										indicator.modulate = Color("#f34c4c").lightened(0.5)
									elif cur_point.state == cur_point.POINT_STATE_CAPPED_RED:
										indicator.modulate = Color("#f34c4c")
									else:
										indicator.modulate = Color.white

								indicator.show()
								indicator.position = pos
							else:
								indicator.hide()
					#else:
						#prints(cur_point.get_parent().name, "/", cur_point.name)
						
				else:
					indicator.hide()