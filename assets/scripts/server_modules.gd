extends "res://assets/scripts/net_constants.gd"

# server vars

var connection
var connection_by_id
var client_by_id

# objects

var modules_data = []
var modules_data_dicts = []

class module:
	var id
	var name
	var desc
	var type
	var pric
	var conf
	var prir
	var vals

func process_packet(data,client):
		
		match data[0]:
			
			MODULE:
				
				var module_id = int(data[2])
				var module_lvl = 0
				
				var requested_module = null
				
				for m in modules_data:
					if module_id == m.id:
						requested_module = m
						break
				
				if requested_module == null: return
				
				match data[1]:
					
					MODULE_BUY, MODULE_BUYC:
							
						var is_bought = false
							
						if connection[client].modules.keys().has(module_id):
							module_lvl = connection[client].modules[module_id]
							if requested_module.pric.size() > module_lvl:
								
								if !mony_check(client, requested_module, module_lvl, data[1]): return
								
								connection[client].modules[module_id] += 1
								connection[client].modules_order.erase(module_id)
								connection[client].modules_order.append(module_id)
								is_bought = true
								
						else:
							
							if !mony_check(client, requested_module, module_lvl, data[1]): return
							
							connection[client].modules[module_id] = 1
							connection[client].modules_order.append(module_id)
							is_bought = true
							
						if is_bought:
							
							for c in connection:
								connection[c].peer.put_var([PLAYER_DATA, PD_MODULES_ORDER, connection[client].id, connection[client].modules_order.duplicate()])
								connection[c].peer.put_var([PLAYER_DATA, PD_MODULE, connection[client].id, connection[client].modules.duplicate()])
							
						else: return
					
					MODULE_SELL:
						
						if (connection[client].modules.keys().has(module_id) and
						!connection[client].modules_c.keys().has(module_id)):
							module_lvl = connection[client].modules[module_id]
							connection[client].modules[module_id] -= 1
							
							if connection[client].modules[module_id] < 1:
								connection[client].modules.erase(module_id)
								connection[client].modules_order.erase(module_id)
								
							for c in connection:
								connection[c].peer.put_var([PLAYER_DATA, PD_MODULES_ORDER, connection[client].id, connection[client].modules_order.duplicate()])
								connection[c].peer.put_var([PLAYER_DATA, PD_MODULE, connection[client].id, connection[client].modules.duplicate()])
							
							mony_check(client, requested_module, module_lvl, data[1])
					
					MODULE_UP:
						
						if connection[client].modules_order.has(module_id):
							connection[client].modules_order.erase(module_id)
							connection[client].modules_order.append(module_id)
							
							for c in connection:
								connection[c].peer.put_var([PLAYER_DATA, PD_MODULES_ORDER, connection[client].id, connection[client].modules_order.duplicate()])
								connection[c].peer.put_var([PLAYER_DATA, PD_MODULE, connection[client].id, connection[client].modules.duplicate()])
				
				if data[1] == MODULE_BUYC:
					
					if connection[client].modules_c.keys().has(module_id):
						connection[client].modules_c[module_id] += 1
					else:
						connection[client].modules_c[module_id] = 1

func mony_check(client, module, lvl, mode):
	
	match mode:
		
		MODULE_BUY:
			
			if connection[client].mony >= module.pric[lvl] and module.pric[lvl] < MAX_MONY.get($"../game".game_stage,MAX_MONY[1]):
				connection[client].mony -= module.pric[lvl]
				
				send_mony_data(client)
				return true
				
			else: return false
			
		MODULE_BUYC:
			
			if connection[client].cred >= module.pric[lvl]:
				connection[client].cred -= module.pric[lvl]
				send_mony_data(client)
				return true
				
			else: return false
			
		MODULE_SELL:
			
			if lvl < 1: return
			var sell_price = int(float(module.pric[lvl-1])*MODULE_SELL_RATE) 
			
			connection[client].mony += sell_price
			send_mony_data(client)
			

func send_mony_data(client):
	
	# clamp mony to max
	connection[client].mony = clamp(connection[client].mony,0,MAX_MONY.get($"../game".game_stage,MAX_MONY[1]))
	
	for c in connection:
		connection[c].peer.put_var([PLAYER_DATA, PD_MONY, connection[client].id, [connection[client].mony, connection[client].cred]])

func build_module_dict(m):
	
	var module_dict = {}
	
	for val in m.get_property_list():
		if val.name in ["Reference","Script","script","Script Variables"]: continue
		module_dict[val.name] = m.get(val.name)
	
	modules_data_dicts.append(module_dict)

func load_json_module(path):
	
	var f = File.new()
	if f.open(path,File.READ) == OK:
		var data = f.get_as_text()
		f.close()
		
		var err = validate_json(data)
		if !err:
			
			var m = module.new()
			var jm = parse_json(data)
			
			for val in m.get_property_list():
				if val.name in ["Reference","Script","script","Script Variables"]: continue
				if !jm.keys().has(val.name):
					state.print_log("[MODULES] Can't load json "+path+", no key: "+val.name)
					return
				m.set(val.name,jm[val.name])
			
			modules_data.append(m)
			build_module_dict(m)
			
		else:
			state.print_log("[MODULES] Can't parse file "+path+", error: "+err)
		
	else:
		state.print_log("[MODULES] Can't open file "+path)

func read_json_modules():
	
	var dir = Directory.new()
	if dir.open(MODULES_PATH) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if !dir.current_is_dir():
				load_json_module(MODULES_PATH + "/" + file_name)
			file_name = dir.get_next()
	else:
		state.print_log("[MODULES] Can't open dir "+MODULES_PATH)


func server_player_connect(who,first=true):
	
	# delete credit modules
	
	for module_c_id in connection[who].modules_c:
		
		var module_c_lvl = connection[who].modules_c[module_c_id]
		
		# subtract lvl
		if connection[who].modules.keys().has(module_c_id):
			connection[who].modules[module_c_id] -= module_c_lvl
			
			# delete completly if no lvl
			if connection[who].modules[module_c_id] < 1:
				connection[who].modules.erase(module_c_id)
				connection[who].modules_order.erase(module_c_id)
		
	connection[who].modules_c = {}
	
	# send edited modules
	
	for c in connection:
		connection[c].peer.put_var([PLAYER_DATA, PD_MODULES_ORDER, connection[who].id, connection[who].modules_order.duplicate()])
		connection[c].peer.put_var([PLAYER_DATA, PD_MODULE, connection[who].id, connection[who].modules.duplicate()])
		
	if !first: return
	
	connection[who].peer.put_var([MODULES_DATA,modules_data_dicts.duplicate(true)])
	
	for client in connection:
		connection[who].peer.put_var([PLAYER_DATA, PD_MODULES_ORDER, connection[client].id, connection[client].modules_order.duplicate()])
		connection[who].peer.put_var([PLAYER_DATA, PD_MODULE, connection[client].id, connection[client].modules.duplicate()])
		connection[who].peer.put_var([PLAYER_DATA, PD_MONY, connection[client].id, [connection[client].mony, connection[client].cred]])

func _ready():
	$"..".connect("player_connect",self,"server_player_connect")
	$"..".connect("other_packet",self,"process_packet")
	read_json_modules()