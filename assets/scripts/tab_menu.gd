extends Panel

# FEED 
const FEED_RESPAWN = 0
const FEED_KILL = 1
# TEAMS
const TEAM_RED = 0
const TEAM_BLU = 1
# STATS
const STATS_KILLS = 0
const STATS_DEATHS = 1
const STATS_CAPS = 2

const COLOR_DEAD = Color(0.2, 0.2, 0.2)

var hidden = true
var hide_header = false

var invert 

var tab_line_pattern = ['avatar', 'nick', 'sep', 'id', 'k','d','c','rang', 'clan'] # что отображать и в каком порядке

onready var unsort_line = { # реализации
		"avatar": TabIcon.new(load('res://icon.png')), 
		"nick": Label.new(),
		"id": Label.new(),
		"sep": Control.new(),
		"rang": TabIcon.new(load('res://icon.png')), #load('res://assets/graphics/rangs/' + str(rang) + '.png')), 
		"k": Label.new(),
		"d": Label.new(),
		"c": Label.new(),
		"clan": TabIcon.new(load('res://icon.png')) #load('res://assets/graphics/clans/' + str(clan) + '.png'))
}

var tab = {TEAM_BLU:{}, TEAM_RED:{}}

onready var players = $"/root/game".players

class TabIcon extends TextureRect:
	var icon_size = Vector2(32, 32)
	func _init(tex = null):
		expand = true
		stretch_mode = TextureRect.STRETCH_KEEP_ASPECT_CENTERED
		rect_min_size = icon_size
		rect_size = icon_size
		texture = tex


func new_tab_line(player):
	unsort_line["nick"].text = player.nickname
	unsort_line["id"].text = str(player.id)
	
	var grid = null
	var team
	match player.team:
		TEAM_RED:
			team = TEAM_RED
			grid = $"Panel_Red/Grid"
		TEAM_BLU:
			team = TEAM_BLU
			grid = $"Panel_Blue/Grid"
		-1:
			return
	
	grid.columns = tab_line_pattern.size()
	tab[team][player.id] = {}    # {team: {id: {"nick":?, ...}}}
	
	var d_tab_line_pattern = tab_line_pattern.duplicate()
	
	if invert == team:
		d_tab_line_pattern.invert()
	
	for field in d_tab_line_pattern:
		var control = unsort_line[field].duplicate()
		tab[team][player.id][field] = control
		grid.add_child(control)


func upd_tab_line(player):
	tab[player.team][player.id]["k"].text = str(player.stats[STATS_KILLS])
	tab[player.team][player.id]["d"].text = str(player.stats[STATS_DEATHS])
	tab[player.team][player.id]["c"].text = str(player.stats[STATS_CAPS])
	
	var color = Color.white
	if player.mech == null:
		color = COLOR_DEAD
	for control in tab[player.team][player.id]:
		tab[player.team][player.id][control].self_modulate = color


func get_cur_players_ids():
	var ids = []
	for player in players.values():
		ids.append(player.id)
	return ids


func del_line_players_not_in_game():
	for team in [TEAM_RED, TEAM_BLU]:
		for id in tab[team]:
			if not(id in get_cur_players_ids()):
				for control in tab[team][id].values():
					control.queue_free()
				tab[team].erase(id)							


func update_tab_menu():
	del_line_players_not_in_game()
	
	for player in players.values():
		if player.team != -1:
			#prints(str(player.team), player.mech)
			if player.id in tab[player.team].keys(): # проверяем соответствует ли текущая команда игрока нашему табу, если нет, то удалляем и перемещаем в противоположную
				upd_tab_line(player)
			else:
				new_tab_line(player)
	

func feed_gate(feed_type, feed_data):
	if feed_type in [FEED_RESPAWN, FEED_KILL]:
		update_tab_menu()


func _process(delta):
	if Input.is_action_just_pressed("tab"):
		hidden = false
		toggle()
	if Input.is_action_just_released("tab"):
		hidden = true
		toggle()


func toggle():
	if hidden:
		hide()
		# animation stuff goes here
	else:
		show()
		# here too

func _ready():
	set_process(true)
	toggle()
	$"/root/game".connect("update_tab_menu",self,"update_tab_menu")
	$"/root/game".connect("update_feed",self,"feed_gate")
	
	unsort_line["sep"].size_flags_horizontal = SIZE_EXPAND_FILL
	unsort_line["k"].text = '0'
	unsort_line["d"].text = '0'
	unsort_line["c"].text = '0'
	
	for control in unsort_line:
		unsort_line[control].name = control
	
	if !hide_header: 
		for team in [TEAM_BLU, TEAM_RED]:
			var grid
			match team:
				TEAM_RED:
					grid = $"Panel_Red/Grid"
				TEAM_BLU:
					grid = $"Panel_Blue/Grid"
			
			grid.columns = tab_line_pattern.size()
			var d_tab_line_pattern = tab_line_pattern.duplicate()
			if invert == team:
				d_tab_line_pattern.invert()
		
			for control in d_tab_line_pattern:
				var l = Label.new()
				l.name = 'l_' + control
				l.valign = Label.VALIGN_CENTER
				if control != 'sep':
					l.text = control
				else:
					l.size_flags_horizontal = SIZE_EXPAND_FILL
				l.rect_min_size.x = unsort_line[control].rect_min_size.x
				grid.add_child(l)
	
	

	# update_tab_menu() - здесь нужно заносить игроков в таблицу на две команды 
	# (players[id].team in [TEAM_RED, TEAM_BLU]),
	# показывать их статистику
	# (players[id].stats[STATS_*]),
	# показывать цветом (?) живы они или нет (затемнять например)
	# (players[id].mech == null)
	#
	# нужно сообразить через что рисовать таблицу с расчетом на то,
	# что в дальнейшем нужно будет показывать звание игрока (иконку),
	# его клан (иконка), аватарка (иконка)
	
	#class player:
	#	var id = -1
	#	var nickname = ""
	#	var team = -1
	#	var mech = null
	#	var seat = null
	#	var modules = {}
	#	var modules_order = []
	#	var mony = 0
	#	var cred = 0
	#	var stats = [0,0,0]