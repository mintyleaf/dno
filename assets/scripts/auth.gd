extends Reference

const COOKIE_PATH := "user://cookie"
const COOKIE_KEY := "BVB_PWNED_BITCH777"

var auth_srv := "https://homelessgunners.tk"

#var enter_by_token = false
var logged_in := false

func auth_post(username: String, password: String) -> bool:
	if !logged_in:
		var token := ""
		var token_cookie = File.new()
		if token_cookie.open_encrypted_with_pass(COOKIE_PATH, File.READ, COOKIE_KEY) == OK:
			token = str(token_cookie.get_var())
			token_cookie.close()
			var query = "username=%s&password=%s&token=%s" % [username, password, token]
			var headers = ["Content-Length: " + str(query.length())]
			if state.http.request(auth_srv + "/auth?" + query, headers, false, HTTPClient.METHOD_POST, query) == OK:
				return true
		else:
			token_cookie.close()
	return false
			#$login/username.editable = true
			#$login/password.editable = true


func logout_post():
	if logged_in:
		var token := ""
		var token_cookie = File.new()
		if token_cookie.open_encrypted_with_pass(COOKIE_PATH, File.READ, COOKIE_KEY) == OK:
			token = str(token_cookie.get_var())
			token_cookie.close()
		var query = "token=" + token
		var headers = ["Content-Length: " + str(query.length())]
		state.http.request(auth_srv + "/exit?" + query, headers, false, HTTPClient.METHOD_POST, query)