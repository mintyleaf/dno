extends TileMap

const MEGUMIN_PATH = "res://assets/scenes/megumin.tscn"

var CELLS = range(8)
var BG_CELLS = range(16,56)
var HARD_CELLS = range(8,16)

var DESTROYED_MEGUMINS = []

var cells = []
var bg_cells = []
var structure_cells = {}
var ore = []
var tnt = []
var seats = []

var structures_places = []


signal map_ready

func spawn_points():
	
	for p in $"/root/game/points".get_children():
		p.queue_free()
	
	yield(get_tree(),"idle_frame")
	
	for point in structures_places:
		
		if point.size() == 4: return
		
		var point_obj = load("res://assets/scenes/point.tscn").instance()
		
		point_obj.name = str(point[5])
		point_obj.type = point[4]
		point_obj.orientation_type = point[0]
		point_obj.root_point = point[6]
		point_obj.length = point[1]
		
		point_obj.position = map_to_world(point[2])
		
		$"../points".add_child(point_obj)
	
	#points indicator

	for indicator in $"/root/game/game_indicators".get_children():
		if indicator.name.begins_with("point_indicator"):
			#indicator.set_meta('point', null)
			indicator.queue_free()
	
	var i = 1
	for p in $"/root/game/points".get_children():
		if p.get_parent() == $"/root/game/points":
			var point_indicator = Sprite.new()
			point_indicator.name = "point_indicator_" + str(i)
			point_indicator.texture = load("res://assets/graphics/circle.png")
			point_indicator.use_parent_material = true
			point_indicator.set_meta('point', p)
			$"/root/game/game_indicators".add_child(point_indicator)
			i += 1
		else:
			breakpoint
		

func spawn_anime_girls():
	
	for p in $"../prop".get_children():
		p.queue_free()
	
	yield(get_tree(),"idle_frame")
	
	for megumin in tnt:
		
		if "MEGUMIN_" + str(tnt.find(megumin)) in DESTROYED_MEGUMINS: continue
		
		var m_scn = load(MEGUMIN_PATH).instance()
		
		m_scn.name = "MEGUMIN_" + str(tnt.find(megumin))
		m_scn.position = map_to_world(megumin) + Vector2(16,16)
		m_scn.rotation_degrees = [0,90,-90,180][randi()%4]
		
		$"../prop".add_child(m_scn)
	
func pick_packet(data):
	var type = data[1]
	var cell_id = null
	var points = null
	if type == "start":
		cells = []
		bg_cells = []
		structure_cells = {}
		structures_places = []
		ore = []
		tnt = []
		$"/root/game".tilemap_hit = {}
		return
	if type == "structure_cells":
		cell_id = data[2]
		points = data[3]
	elif type != "end":
		points = data[2]
	
	match type:
		"cells":
			for p in points:
				cells.append(p)
		"bg_cells":
			for p in points:
				bg_cells.append(p)
		"structure_cells":
			for p in points:
				if !structure_cells.has(cell_id):
					structure_cells[cell_id] = [p]
				else:
					structure_cells[cell_id].append(p)
		"structures_places":
			structures_places = points
		"ore":
			for p in points:
				ore.append(p)
		"tnt":
			for p in points:
				tnt.append(p)
		"seats":
			seats = points
		"end":
			
			$destruct.clear()
			clear()
			
			for i in structure_cells.keys():
				for j in structure_cells[i]:
					if get_cellv(j) == -1:
						set_cellv(j,i)
			
			for i in cells:
				if get_cellv(i) == -1:
					set_cellv(i,CELLS[randi()%CELLS.size()])
			
			for i in bg_cells:
				if get_cellv(i) == -1:
					set_cellv(i,BG_CELLS[randi()%BG_CELLS.size()])
			
			emit_signal("map_ready", self)
			spawn_points()