extends Camera2D

const TEAM_RED = 0
const TEAM_BLU = 1

const MIN_ZOOM_MOUSE_DISTANCE = 300.0
const MAX_ZOOM_MOUSE_DISTANCE = 500.0

const FOLLOW_SPEED = 0.1
const ZOOM_SPEED = 0.025

const MODE_RESPAWN = 0
const MODE_DEAD = 1

const AIM_DISTANCE_BIAS = 256

### modules

var aim = false
var aim_point = Vector2(0,0)
var aim_active = false

###

var seat_obj = null
var player_obj = null

var first = true

var follow_mode = -1

var network_helper

func _ready():
	set_physics_process(false)
	$"..".connect("me_respawn",self,"mode",[MODE_RESPAWN])
	$"..".connect("me_dead",self,"mode",[MODE_DEAD])

func mode(mode):
	
	match mode:
		
		MODE_RESPAWN:
			
			player_obj = $"../players".get_node(str(network_helper.enet_id))
			if player_obj == null: 
				follow_mode = -1
				return
			
			follow_mode = mode
			set_physics_process(true)
			drag_margin_h_enabled = true
			drag_margin_v_enabled = true
			
		MODE_DEAD:
			
			aim = false
			
			seat_obj = $"../seats".get_node(str(network_helper.enet_id))
			if seat_obj == null:
				follow_mode = -1
				return
			
			follow_mode = mode
			set_physics_process(false)
			drag_margin_h_enabled = false
			drag_margin_v_enabled = false
			
			var team_offset = Vector2(0,0)
			if $"..".me != null:
				match $"..".me.team:
					TEAM_RED:
						team_offset.x += 128
					TEAM_BLU:
						team_offset.x -= 128
			
			if first:
				position = seat_obj.global_position + team_offset
				first = false
			
			$tw.interpolate_property(self,"position",position,seat_obj.global_position + team_offset,2,Tween.TRANS_SINE,Tween.EASE_OUT)
			$tw.interpolate_property(self,"zoom",zoom,Vector2(0.5,0.5),4,Tween.TRANS_SINE,Tween.EASE_OUT)
			$tw.start()
			

func _process(delta):
	
	if follow_mode != 0: return
	
	var mouse_pos = get_global_mouse_position()
	var player_pos = player_obj.global_position
	
	var distance = clamp(player_pos.distance_to(mouse_pos),MIN_ZOOM_MOUSE_DISTANCE,MAX_ZOOM_MOUSE_DISTANCE)
	
	zoom = zoom.linear_interpolate( Vector2(1,1) * distance/MIN_ZOOM_MOUSE_DISTANCE, ZOOM_SPEED )
	
	if is_instance_valid(player_obj):
		
		if aim:
			aim_point = player_obj.get_node("weapon/aim").get_collision_point()
			
			if Input.is_action_pressed("aim"):
				aim_active = true
			else:
				aim_active = false
		
		player_obj.aim = aim_active
		
	if aim_active:
		if player_pos.distance_to(aim_point) > AIM_DISTANCE_BIAS and player_pos.distance_to(get_global_mouse_position()) > AIM_DISTANCE_BIAS:
			var viewport_mouse = get_viewport().get_mouse_position().x
			if player_pos.x > get_global_mouse_position().x:
				viewport_mouse = get_viewport_rect().size.x - get_viewport().get_mouse_position().x
			var scroll = viewport_mouse / get_viewport_rect().size.x
			position = position.linear_interpolate(player_pos.linear_interpolate( aim_point, scroll ),FOLLOW_SPEED*0.5)
	else:
		position = position.linear_interpolate( player_pos.linear_interpolate(mouse_pos,0.5), FOLLOW_SPEED )