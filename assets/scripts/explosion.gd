extends Area2D

var is_master = false
var radius = 0
var dmg = 0

var do_shit = true
var damage_everything = false

func _ready():
	
	$col.shape.radius = radius
	
	$tw.interpolate_property(
	material,"shader_param/abX",material.get_shader_param("abX"),
	randf()/10,0.1,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
	$tw.interpolate_property(
	material,"shader_param/abY",material.get_shader_param("abY"),
	randf()/10,0.1,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
	$tw.interpolate_property(
	material,"shader_param/disp_size",material.get_shader_param("disp_size"),
	clamp(randf(),0.1,1),0.1,Tween.TRANS_LINEAR,Tween.EASE_OUT)
	
	$tw.start()
	$anim.play("def")
	
	yield(get_tree().create_timer(0.3),"timeout")
	
	do_shit = false
	
	yield(get_tree().create_timer(0.7),"timeout")
	
	queue_free()

func _draw():
	draw_circle(Vector2(0,0),radius,Color(1,1,1))


# ХУАН ПЕТУч (где reliable updates of get_overlapping_bodies/areas?!?!?!?)
# #3dненужно
func _on_explode_area_entered(area):
	if is_master and do_shit:
		var pos = area.global_position
		if area.get_node("../../..").name == "players":
			var part = -1
			match area.name:
				"lt" : part = 0
				"rt" : part = 1
				"lb" : part = 2
				"rb" : part = 3
				"c" : part = 4
			if part > -1:
				pos = area.get_node("../..").hitbox_graphics[part].global_position
		var force = clamp(radius - position.distance_to(pos), 0, INF) * 1.0/radius
		if force > 0:
			$"/root/game".process_explode_area(area, int(dmg * force), damage_everything)

func _on_explode_body_entered(body):
	if is_master and do_shit:
		var pos = body.global_position
		var force = clamp(radius - position.distance_to(pos), 0, INF) * 1.0/radius
		if force > 0:
			$"/root/game".process_explode_area(body, int(dmg * force), damage_everything)

func _on_tw_tween_all_completed():
	$tw.interpolate_property(
	material,"shader_param/abX",material.get_shader_param("abX"),
	randf()/10,0.1,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
	$tw.interpolate_property(
	material,"shader_param/abY",material.get_shader_param("abY"),
	randf()/10,0.1,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
	$tw.interpolate_property(
	material,"shader_param/disp_size",material.get_shader_param("disp_size"),
	clamp(randf(),0.1,1),0.1,Tween.TRANS_LINEAR,Tween.EASE_OUT)

	$tw.start()
