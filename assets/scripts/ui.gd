extends Control

var respawn = true
var respawn_timer = 0.0

var dammo = 0
var dprop_ammo = 0
var dcweapon_id = null
var texture_key = null

var dred_v = 0
var dblue_v = 0
var dred_mv = 0
var dblue_mv = 0
var dprivate_score = 0
var dupgrade_points = 0
var dupgrades_conf = {}

var selected_score = 0
var winning = false

var prev_private_score = -1
var prev_upgrade_points = -1

var prev_sound = ""
var sounds = {
	"hover" : preload("res://assets/sounds/HOVER.ogg"),
	"click" : preload("res://assets/sounds/CLICK.ogg")
}

var debug = false
var escape_menu = false

func hud_ammo_update_weapon():
	if dcweapon_id == null: return
	
	if int(dcweapon_id) < 3:
		texture_key = "1"
	else:
		texture_key = str(int(dcweapon_id)-1)
	
	for i in $hud/ammo.get_children():
		i.queue_free()
	
	for i in range(dprop_ammo):
		var ammo_cell = TextureRect.new()
		ammo_cell.name = str(i)
		ammo_cell.texture = load("res://assets/graphics/ui/ammo/ae"+texture_key+".png")
		$hud/ammo.add_child(ammo_cell)
	
	hud_ammo_update_ammo()
	
func hud_ammo_update_ammo():
	for i in range(dprop_ammo):
		var tex
		if dammo > i:
			tex = load("res://assets/graphics/ui/ammo/af"+texture_key+".png")
		else:
			tex = load("res://assets/graphics/ui/ammo/ae"+texture_key+".png")
		$hud/ammo.get_child(i).texture = tex
		


func _process(delta):
	if Input.is_action_just_pressed("debug"):
		if !debug and !$debug.visible:
			debug = true
			$debug.show()
		elif debug and $debug.visible:
			debug = false
			$debug.hide()
	
	if Input.is_action_just_pressed("escape"):
		if !escape_menu and !$escape_menu.visible:
			escape_menu = true
			$escape_menu/anim.play("show")
		elif escape_menu and $escape_menu.visible:
			escape_menu = false
			$escape_menu/anim.play("hide")
	
	if !$"/root/game".loaded:
		$loading_screen.show()
		$loading_screen/status.text = $"/root/game".status
		return
	if get_tree().has_network_peer():
		if network.current_tree_peer.get_connection_status() < 2: return
		
		if $"/root/game".start_cap_timer:
			$points/score/timer.text = str(int($"/root/game".cap_timer))
			$points/score/timer.modulate = Color(1,0.3,0.3)
		else:
			if $"/root/game".main_timer < 11:
				$points/score/timer.modulate = Color(1,0.3,0.3)
			else:
				$points/score/timer.modulate = Color(1,1,1)
			$points/score/timer.text = str((int($"/root/game".main_timer)%3600)/60) + ":" + str(int($"/root/game".main_timer)%60).pad_zeros(2)
		
		if $"/root/game".win_condition != null:
			if $"/root/game".win_condition == $"/root/game".team and !winning:
				$F_TO_WIN_PANEL/anim.play("show")
				winning = true
		else:
			if winning:
				$F_TO_WIN_PANEL/anim.play("hide")
				winning = false
		
		if winning:
			if Input.is_action_just_pressed("win"):
				network.rpc_id(1,"f_pressed_to_win")
		
		if has_node("/root/game/players/"+str(get_tree().get_network_unique_id())+"/weapon"):
			if get_node("/root/game/players/"+str(get_tree().get_network_unique_id())+"/weapon").cweapon_id != dcweapon_id:
				dcweapon_id = get_node("/root/game/players/"+str(get_tree().get_network_unique_id())+"/weapon").cweapon_id
				hud_ammo_update_weapon()
			if get_node("/root/game/players/"+str(get_tree().get_network_unique_id())+"/weapon").prop_ammo != dprop_ammo:
				dprop_ammo = get_node("/root/game/players/"+str(get_tree().get_network_unique_id())+"/weapon").prop_ammo
				hud_ammo_update_weapon()
			if get_node("/root/game/players/"+str(get_tree().get_network_unique_id())+"/weapon").ammo != dammo:
				dammo = get_node("/root/game/players/"+str(get_tree().get_network_unique_id())+"/weapon").ammo
				hud_ammo_update_ammo()
		
		if has_node("/root/game/players/"+str(get_tree().get_network_unique_id())):
			$hud/fuel.max_value = get_node("/root/game/players/"+str(get_tree().get_network_unique_id())).JET_FUEL_MAX
			$hud/fuel.value = get_node("/root/game/players/"+str(get_tree().get_network_unique_id())).jet_fuel
		
		if respawn_timer > 0:
			if Rect2($death_screen/go.get_rect().position,$death_screen/go.get_rect().size*2 * 720/OS.get_window_size().y).has_point(get_global_mouse_position() * 720/OS.get_window_size().y) and respawn_timer < 11:
				$death_screen/go.modulate.a = 1.0
				if prev_sound != "click":
					play_sound("hover")
				if Input.is_action_just_pressed("shoot"):
					play_sound("click")
					respawn_timer = 11
			else:
				play_sound("")
				$death_screen/go.modulate.a = 0.8
		else:
			$death_screen/go.modulate.a = 0.4
			
			
		if respawn and $"/root/game".loaded:
			if $loading_screen.visible:
				$loading_screen.hide()
			if $cursor.active:
				$cursor.active = false
			if Rect2($death_screen/private_score.rect_global_position,$death_screen/private_score.rect_size*2).has_point(get_global_mouse_position()) and !escape_menu:
				if Input.is_action_just_pressed("shoot"):
					$death_screen/private_score.modulate.a = 1
					$death_screen/private_score/pointer.show()
					$death_screen/upgrade_points.modulate.a = 0.5
					$death_screen/upgrade_points/pointer.hide()
					selected_score = 0
					play_sound("click")
			else:
				if prev_sound != "hover":
					play_sound("")
			if Rect2($death_screen/upgrade_points.rect_global_position,$death_screen/upgrade_points.rect_size*2).has_point(get_global_mouse_position()) and !escape_menu:
				if Input.is_action_just_pressed("shoot"):
					$death_screen/upgrade_points.modulate.a = 1
					$death_screen/upgrade_points/pointer.show()
					$death_screen/private_score.modulate.a = 0.5
					$death_screen/private_score/pointer.hide()
					selected_score = 1
					play_sound("click")
			else:
				if prev_sound != "hover":
					play_sound("")

			if !$death_screen.visible: 
				$death_screen.show()
				$"/root/game/bg".scale = Vector2(0,0)
				$map_c.hide()
				$map_panel.hide()
				$hud.hide()
				$death_screen/respawn.modulate = Color(1,1,1)
				$"/root/game/camera".respawn = true
				$"/root/game/camera".respawning = false
			respawn_timer+=delta
			$death_screen/respawn.text = str(int(15-respawn_timer))
			if respawn_timer > 11 and !$start.is_playing():
				$death_screen/respawn.modulate = Color(1,0.5,0.5)
				$start.play()
				$"/root/game/camera".respawning = true
			if respawn_timer > 14:
				$"/root/game".respawn()
				$death_screen.hide()
				$"/root/game/bg".scale = Vector2(1,1)
				$map_c.show()
				$map_panel.show()
				$hud.show()
				$"/root/game/camera".respawn = false
				respawn_timer = 0.0
				respawn = false
			
			if "private_score" in  $"/root/game".player_stat.keys():
				if $"/root/game".player_stat["private_score"] != prev_private_score or $"/root/game".player_stat["upgrade_points"] != prev_upgrade_points:
					prev_private_score = $"/root/game".player_stat["private_score"]
					prev_upgrade_points = $"/root/game".player_stat["upgrade_points"]
					for i in $death_screen/upgrade.get_children():
						i.update_button()
		else:
			if !$cursor.active:
				$cursor.active = true
		
		if $"/root/game".team_points_size != 0:
			if dred_v != $"/root/game".team_points_score["left"]:
				$points/score/red_v.interpolate_property($points/score/red,"value",
				$points/score/red.value,float($"/root/game".team_points_score["left"])/$"/root/game".team_points_size*100+1,0.5,Tween.TRANS_QUAD,Tween.EASE_OUT)
				$points/score/red_v.start()
				dred_v = $"/root/game".team_points_score["left"]
			
			if dblue_v != $"/root/game".team_points_score["right"]:
				$points/score/blue_v.interpolate_property($points/score/blue,"value",
				$points/score/blue.value,float($"/root/game".team_points_score["right"])/$"/root/game".team_points_size*100,0.5,Tween.TRANS_QUAD,Tween.EASE_OUT)
				$points/score/blue_v.start()
				dblue_v = $"/root/game".team_points_score["right"]
				
			if dred_mv != $"/root/game".team_points_size:
				$death_screen/upgrade_points/upgrade_points.max_value = $"/root/game".team_points_size
				$points/score/red_v.interpolate_property($points/score/red,"value",
				$points/score/red.value,float($"/root/game".team_points_score["left"])/$"/root/game".team_points_size*100+1,0.5,Tween.TRANS_QUAD,Tween.EASE_OUT)
				$points/score/red_v.start()
				dred_mv = $"/root/game".team_points_size
			
			if dblue_mv != $"/root/game".team_points_size:
				$death_screen/upgrade_points/upgrade_points.max_value = $"/root/game".team_points_size
				$points/score/blue_v.interpolate_property($points/score/blue,"value",
				$points/score/blue.value,float($"/root/game".team_points_score["right"])/$"/root/game".team_points_size*100,0.5,Tween.TRANS_QUAD,Tween.EASE_OUT)
				$points/score/blue_v.start()
				dblue_mv = $"/root/game".team_points_size
				
		if "private_score" in $"/root/game".player_stat.keys():
			if dprivate_score != $"/root/game".player_stat["private_score"]:
				$death_screen/private_score/private_score_v.interpolate_property($death_screen/private_score/private_score,"value",
				$death_screen/private_score/private_score.value,$"/root/game".player_stat["private_score"],0.5,Tween.TRANS_QUAD,Tween.EASE_OUT)
				$death_screen/private_score/private_score_v.start()
				dprivate_score = $"/root/game".player_stat["private_score"]
				
			if dupgrade_points != $"/root/game".player_stat["upgrade_points"]:
				$death_screen/upgrade_points/upgrade_points_v.interpolate_property($death_screen/upgrade_points/upgrade_points,"value",
				$death_screen/upgrade_points/upgrade_points.value,$"/root/game".player_stat["upgrade_points"],0.5,Tween.TRANS_QUAD,Tween.EASE_OUT)
				$death_screen/upgrade_points/upgrade_points_v.start()
				dprivate_score = $"/root/game".player_stat["private_score"]
			$death_screen/private_score/amount.text = str($"/root/game".player_stat["private_score"])
			$death_screen/upgrade_points/amount.text = str($"/root/game".player_stat["upgrade_points"])
		
		$points/score/red_text.text = str($"/root/game".team_points_score["left"])
		$points/score/blue_text.text = str($"/root/game".team_points_score["right"])
		
		if dupgrades_conf != $"/root/game".upgrades_conf:
			for i in $"/root/game".upgrades_conf.keys():
				get_node("death_screen/upgrade/"+i).max_count = $"/root/game".upgrades_conf[i]["price"].size()
				get_node("death_screen/upgrade/"+i).prices = $"/root/game".upgrades_conf[i]["price"]
				get_node("death_screen/upgrade/"+i).reset()
			dupgrades_conf = $"/root/game".upgrades_conf
		
		# debug_text
		if debug:
			var fps = Performance.get_monitor(Performance.TIME_FPS)
			var ft = Performance.get_monitor(Performance.TIME_PROCESS)
			var player_pos = Vector2(0,0)
			if has_node("/root/game/players/"+str(get_tree().get_network_unique_id())):
				player_pos = get_node("/root/game/players/"+str(get_tree().get_network_unique_id())).position.snapped(Vector2(1,1))
			$debug.text = "FPS: %s, Frame time: %s, Position: %s" % [str(fps),str(ft),str(player_pos)]
		
		if escape_menu:
			if $cursor.active:
				$cursor.active = false
			if Rect2($escape_menu/exit.rect_global_position,$escape_menu/exit.rect_size).has_point(get_global_mouse_position()):
				$escape_menu/exit.modulate.a = 1.0
				if prev_sound != "click":
					play_sound("hover")
				if Input.is_action_just_pressed("shoot"):
					play_sound("click")
					exit()
			else:
				$escape_menu/exit.modulate.a = 0.8

func exit():
	network.current_tree_peer.close_connection()
	$"/root/game"._disconnected_from_server()

func play_sound(what):
	if what != prev_sound:
		if sounds.has(what):
			$ifx.stream = sounds[what]
			$ifx.play(0)
		prev_sound = what