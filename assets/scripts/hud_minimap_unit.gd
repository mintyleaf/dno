extends Node2D

onready var networkv2 = $"/root/networkv2"

var team = -1
var state = -1
var crouch = false

func _ready():
	u()

func u():
	update()

func _draw():
	if team > -1:
		var unit_color
		if name == str(networkv2.info["id"]): # if this current player
			unit_color = Color(1, 1, 1)
		else:
			match team:
				0: unit_color = Color(1, 0.3, 0.3)
				1: unit_color = Color(0.5, 0.5, 1)
		draw_rect(Rect2(-1,-1,3,3), unit_color)
	elif state > -1:
		match state:
			1: draw_circle(Vector2(0,0),2,Color(0.7,0.7,0.7))
			2: draw_circle(Vector2(0,0),2,Color(1,0,0))
			3: draw_circle(Vector2(0,0),2,Color(0.3,0.3,1))