extends Control

const GAME_MODE_NONE = 0
const GAME_MODE_WAIT = 1
const GAME_MODE_MATCH = 2
const GAME_MODE_PAY_RESPECT = 3

func _ready():
	hide()
	$panel/team_points.hide()
	$"/root/game".connect("me_respawn",self,"toggle",[true])
	$"/root/game".connect("me_dead",self,"toggle",[false])
	$"/root/game".connect("timer",self,"timer")
	$"/root/game".connect("team_points_update",self,"team_points_update")

func team_points_update(points):
	
	if points.size() != 5: $panel/team_points.hide()
	if points[4] == 0: $panel/team_points.hide()
	else: $panel/team_points.show()
	
	$panel/team_points/red.max_value = points[4]
	$panel/team_points/blu.max_value = points[4]
	
	$panel/team_points/red.value = points[0]
	$panel/team_points/red/score.text = str(points[0])
	$panel/team_points/blu.value = points[1]
	$panel/team_points/blu/score.text = str(points[1])
	

func timer(time,mode):
	
	var time_format = ""
	if time != -1: 
		time_format = str((int(time+1)%3600)/60) + ":" + str(int(time+1)%60).pad_zeros(2)
	
	match mode:
		
		GAME_MODE_NONE:
			
			$"msg_panel/Text_Label".current_state = "waiting"   
			
		GAME_MODE_WAIT:
			
			$"msg_panel/Text_Label".current_state = "waiting" 
			$panel/timer.text = time_format
			
		GAME_MODE_MATCH:
			
			$"msg_panel/Text_Label".current_state = "ingame" 
			$panel/timer.text = time_format
			
		GAME_MODE_PAY_RESPECT:
			
			$"msg_panel/Text_Label".current_state = "press_F"

func toggle(on):
	
	match on:
		
		true:
			
			show()
			
		false:
			
			hide()