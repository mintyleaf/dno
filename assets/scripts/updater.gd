extends Panel

var upd_server := "https://homelessgunners.tk"
var updater_link := upd_server + "/download"

onready var log_text = $"log"

var temp_text = ""
var is_exit = false

signal process_ready

func _ready():
	
	OS.set_window_title("DNO updater")
	
	var args = OS.get_cmdline_args()
	if "--server" in args:
		for arg in args:
			if arg.begins_with("--port="):
				state.server_port = int(arg.replace("--port=",""))
		start_server()
		return
	yield(get_tree().create_timer(1.0), "timeout")
	if !OS.is_debug_build() and !Input.is_key_pressed(KEY_F8):
		update()
	else:
		log_text.text += "\nskipping updates"
		ProjectSettings.load_resource_pack("res://menu.pck")
		ProjectSettings.load_resource_pack("res://game.pck")
		start_game()


func update():
	$http.use_threads = true
	
	$http.download_file = "user://pcks.list"
	if $http.request(updater_link+"/pcks.list") == OK:
		log_text.text += "\nretrieving packs list..."
	else:
		log_text.text += "\ncan't get packs list from server!"
		exit()
		return
	
	yield($http,"request_completed")
	
	var packs_list_f = File.new()
	packs_list_f.open("user://pcks.list",File.READ)
	var packs_list = packs_list_f.get_as_text()
	packs_list = packs_list.replace("\n","")
	packs_list = packs_list.split(" ")
	packs_list_f.close()
	state.ver = packs_list[packs_list.size()-1]
	packs_list.remove(packs_list.size()-1)
	
	for i in packs_list:
		process_pck(i.replace("\n",""))
		yield(self,"process_ready")
	
	cleanup()
	start_game()


func process_pck(pck_name):
	
	# verify md5
	
	$http.download_file = "user://temp.md5"
	if $http.request(updater_link+"/"+pck_name+".pck.md5") == OK:
		log_text.text += "\nretrieving " + pck_name + " md5..."
	else:
		log_text.text += "\ncan't get md5 from server!"
		exit()
		return
	
	yield($http,"request_completed")
	
	log_text.text += "\nverifying " + pck_name + " md5..."
	
	var temp_server_md5_f = File.new()
	temp_server_md5_f.open("user://temp.md5",File.READ)
	var temp_server_md5 = temp_server_md5_f.get_as_text()
	temp_server_md5 = temp_server_md5.split(" ")
	temp_server_md5_f.close()
	var temp_client_md5 = File.new().get_md5("res://" + pck_name + ".pck")
	var pck_size = int(temp_server_md5[2])
	
	if temp_server_md5[0] != temp_client_md5:
		
		# downloading file
		
		var s_counter = 0
		var prev_s = -1
		
		log_text.text += "\n" + pck_name + ".pck md5 mismatch!"
		
		$http.download_file = "res://" + pck_name + ".pck"
		if $http.request(updater_link+"/" + pck_name + ".pck") == OK:
			log_text.text += "\nretrieving " + pck_name + ".pck..."
		else:
			log_text.text += "\ncan't get " + pck_name + ".pck from server!"
			exit()
			return
		
		log_text.text += "\ndownloading..."
		temp_text = log_text.text
		
		while s_counter < 2:
			if prev_s != $http.get_http_client_status():
				if prev_s == 7 and $http.get_http_client_status() == 0:
					s_counter+=1
				prev_s = $http.get_http_client_status()
				if prev_s == 7:
					s_counter+=1
			
			log_text.text = ""
			
			temp_text = temp_text.split("\n")
			temp_text.remove(temp_text.size()-1)
			for i in temp_text:
				log_text.text += i+"\n"
			
			log_text.text += "downloading... " + str(int(float($http.get_downloaded_bytes()) / pck_size * 100)) + "%"
			temp_text = log_text.text
			
			yield(get_tree(),"idle_frame")

		log_text.text = ""
		
		temp_text = temp_text.split("\n")
		temp_text.remove(temp_text.size()-1)
		for i in temp_text:
			log_text.text += i+"\n"
		
		log_text.text += "downloading... 100%"
		temp_text = log_text.text

	if ProjectSettings.load_resource_pack("res://" + pck_name + ".pck"):
		log_text.text += "\nloaded " + pck_name + ".pck!"
		emit_signal("process_ready")
	else:
		log_text.text += "\ncan't load " + pck_name + ".pck!"
		exit()
		return


func cleanup():
	var dir = Directory.new()
	dir.remove("user://temp.md5")
	dir.remove("user://packs.list")


func exit():
	is_exit = true
	yield(get_tree().create_timer(3.0),"timeout")
	get_tree().quit()


func start_game():
	if is_exit: return
	yield(get_tree().create_timer(1.0),"timeout")
	get_tree().change_scene("res://assets/scenes/menu.tscn")


func start_server():
	get_tree().change_scene("res://assets/scenes/server.tscn")


func update_dno_pck():
	state.http.use_threads = true
	state.http.download_file = "res://dno.pck"
	if state.http.request(updater_link + "/dno.pck") == OK:
		print("Updating updater...")
	else:
		print("Can't update updater!")
		return
	
	yield(state.http,"request_completed")
	print("Updated updater!")
