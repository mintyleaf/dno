extends Control

const CONFIG_PATH = "user://dno.cfg"
const COOKIE_PATH = "user://cookie"
const COOKIE_KEY = "BVB_PWNED_BITCH777"

onready var logo = $"CenterContainer/VBox/logo"
onready var sound = $"CenterContainer/VBox/sound"
onready var fullscreen = $"CenterContainer/VBox/fullscreen"
onready var nickname = $"CenterContainer/VBox/nickname"
onready var ip = $"CenterContainer/VBox/HBox/ip"
onready var port = $"CenterContainer/VBox/HBox/port"

var enter_by_token = false
var logged_in = false

var is_ready = false

func auth_post(username,password):
	
	var token = ""
	
	if enter_by_token:
		
		var token_cookie = File.new()
		
		if token_cookie.open_encrypted_with_pass(COOKIE_PATH,File.READ,COOKIE_KEY) == OK:
			token = str(token_cookie.get_var())
			token_cookie.close()
			
		else:
			enter_by_token = false
			$login/username.editable = true
			$login/password.editable = true
			token_cookie.close()
			
			return
	
	var query = "username="+username+"&password="+password+"&token="+token
	var headers = ["Content-Length: " + str(query.length())]
	$http.request("https://homelessgunners.tk/auth?"+query,headers,false,HTTPClient.METHOD_POST,query)

func logout_post():
	if logged_in:
		
		var token = ""
		
		var token_cookie = File.new()
		
		if token_cookie.open_encrypted_with_pass(COOKIE_PATH,File.READ,COOKIE_KEY) == OK:
			token = str(token_cookie.get_var())
			token_cookie.close()
		
		var query = "token="+token
		var headers = ["Content-Length: " + str(query.length())]
		$http.request("https://homelessgunners.tk/exit?"+query,headers,false,HTTPClient.METHOD_POST,query)

func _on_http_request_completed(result, response_code, headers, body):
	var d = parse_json(body.get_string_from_utf8())
	if d == null:
		return
	print("responce: "+d["code"])
	match d["code"]:
		"password_login_ok":
			var token = d["token"]
			var token_cookie = File.new()
			
			if token_cookie.open_encrypted_with_pass(COOKIE_PATH,File.WRITE,COOKIE_KEY) != OK:
				print("Can't write cookie!")
				token_cookie.close()
				enter_by_token = false
				$login/username.editable = true
				$login/password.editable = true
			else:
				token_cookie.store_var(token)
				token_cookie.close()
				enter_by_token = true
				$login/username.editable = false
				$login/password.editable = false
			
			logged_in = true
			$login/logout.disabled = false
			$login/login.disabled = true
		
		"token_login_ok":
			logged_in = true
			$login/logout.disabled = false
			$login/login.disabled = true
		
		"invalid_token":
			var token_cookie = File.new()
			enter_by_token = false
			$login/username.editable = true
			$login/password.editable = true
			if token_cookie.open_encrypted_with_pass(COOKIE_PATH,File.WRITE,COOKIE_KEY) == OK:
				token_cookie.store_var("")
				token_cookie.close()
		
		"no_token":
			logged_in = false
			$login/logout.disabled = true
			$login/login.disabled = false
			enter_by_token = false
			$login/username.editable = true
			$login/password.editable = true
		
		"user_offline_ok":
			logged_in = false
			$login/logout.disabled = true
			$login/login.disabled = false

func _ready():
	
	OS.set_window_title("DNO menu")
	
	$CenterContainer/VBox/version.text = "build: "+state.ver
	
	var config_file = File.new()
	if config_file.open(CONFIG_PATH,File.READ) == OK:
		read_cfg(config_file)
	else:
		new_cfg(config_file)
		
	if !OS.is_debug_build():
		update_dno_pck()
	
	var token_cookie = File.new()
	if token_cookie.open_encrypted_with_pass(COOKIE_PATH,File.READ,COOKIE_KEY) == OK:
		if token_cookie.get_var() == "":
			enter_by_token = false
			$login/username.editable = true
			$login/password.editable = true
		else:
			enter_by_token = true
			$login/username.editable = false
			$login/password.editable = false
		token_cookie.close()
	else:
		enter_by_token = false
		$login/username.editable = true
		$login/password.editable = true
	
	VisualServer.set_default_clear_color(Color(0.5,0.5,0.5))
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	is_ready = true
	for menu_ui_node in get_tree().get_nodes_in_group('menu_ui'):
		calc_min_width(menu_ui_node)

func _on_c_pressed():
	var game_scn = load("res://assets/scenes/game.tscn").instance()
	game_scn.loaded_ip = ip.text
	game_scn.loaded_port = int(port.text)
	game_scn.loaded_nickname = nickname.text
	$"/root".add_child(game_scn)
	queue_free()


func _on_fullscreen_pressed():
	state.fullscreen = fullscreen.pressed
	OS.window_fullscreen = fullscreen.pressed
	state.resize()
	state.cfg["fullscreen"] = fullscreen.pressed
	write_cfg()


func _on_sound_pressed():
	state.sound = sound.pressed
	state.update_sound()
	state.cfg["sound"] = sound.pressed
	write_cfg()


func calc_font_size(min_font_size=150, max_font_size=250, min_width=1280, max_width=2560):
	return (min_font_size + (max_font_size - min_font_size) * ((get_viewport_rect().size.x - min_width) / (max_width - min_width)))


func calc_min_width(ui_node):
	var font_size = ui_node.get('custom_fonts/font').get_size()
	if !ui_node.has_meta('min_chars'):
		ui_node.set_meta('min_chars', ui_node.rect_size.x / (font_size/2))
		return ui_node.rect_size.x
	else:
		return (font_size / 2) * ui_node.get_meta('min_chars')


func _on_main_resized():
	if is_ready:
		logo.get('custom_fonts/font').set_size(calc_font_size())
		for menu_ui_node in get_tree().get_nodes_in_group('menu_ui'):
			menu_ui_node.get('custom_fonts/font').set_size(calc_font_size(16, 26))
			menu_ui_node.rect_min_size.x = calc_min_width(menu_ui_node)


func read_cfg(c):
	var config_file_data = c.get_as_text()
	if validate_json(config_file_data) != "":
		new_cfg(c)
	else:
		config_file_data = parse_json(config_file_data)
		if !(config_file_data is Dictionary):
			new_cfg(c)
			return
		for i in config_file_data.keys():
			if !state.cfg.has(i):
				new_cfg(c)
				return
		
		for i in config_file_data.keys():
			if state.cfg.has(i):
				state.cfg[i] = config_file_data[i]
		
		c.close()
	
	for i in state.cfg.keys():
		match i:
			"nickname":
				if state.cfg[i].length() > 24: 
					state.cfg[i] = "newbe"
					write_cfg()
				$CenterContainer/VBox/nickname.text = state.cfg[i]
			"ip":
				$CenterContainer/VBox/HBox/ip.text = state.cfg[i]
			"port":
				$CenterContainer/VBox/HBox/port.text = str(state.cfg[i])
			"fullscreen":
				$CenterContainer/VBox/fullscreen.pressed = state.cfg[i]
				_on_fullscreen_pressed()
			"sound":
				$CenterContainer/VBox/sound.pressed = state.cfg[i]
				_on_sound_pressed()

func new_cfg(c):
	if c.open(CONFIG_PATH,File.WRITE) != OK:
		print("Can't make config file!")
		c.close()
	else:
		c.store_string(to_json(state.cfg))
		c.close()

func write_cfg():
	var c = File.new()
	
	if c.open(CONFIG_PATH,File.WRITE) != OK:
		print("Can't write to config file!")
		c.close()
	else:
		c.store_string(to_json(state.cfg))
		c.close()

func update_dno_pck():
	
	$http.use_threads = true
	
	$http.download_file = "res://dno.pck"
	if $http.request("http://homelessgunners.tk/download/dno.pck") == OK:
		print("Updating updater...")
	else:
		print("Can't update updater!")
		return
	
	yield($http,"request_completed")
	print("Updated updater!")

func _on_ip_text_changed(new_text):
	state.cfg["ip"] = new_text
	write_cfg()

func _on_port_text_changed(new_text):
	state.cfg["port"] = int(new_text)
	write_cfg()

func _on_nickname_text_changed(new_text):
	state.cfg["nickname"] = new_text
	write_cfg()

func _on_login_pressed():
	auth_post($login/username.text,$login/password.text)

func _on_logout_pressed():
	logout_post()
