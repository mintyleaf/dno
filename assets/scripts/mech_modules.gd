extends Node

# wtypes

const WTYPE_NO_BOOST = 0
const WTYPE_BOOST = 1
const WTYPE_SHOTGUN = 2
const WTYPE_SNIPEGUN = 3
const WTYPE_ROCKET = 4
const WTYPE_LASER = 5

### 

var active_modules = []

signal end

func update_modules(mech,is_master,modules_list,modules_order,modules_data):
	
	active_modules = []
	
	for module in modules_data:
		if int(module.id) in modules_list.keys():
			update_active_modules(int(module.id),module.conf,modules_order)
	
	sort_active_modules(modules_data)
	
	for id in active_modules:
		for module in modules_data:
			if int(module.id) == id:
				process_module(module, modules_list[int(module.id)], mech)
	
	emit_signal("end")

func sort_active_modules(modules_data):
	
	var new_active_modules = []
	var prirs = []
	
	for module in modules_data:
		if int(module.id) in active_modules:
			if !(int(module.prir) in prirs):
				prirs.append(int(module.prir)) 
	
	prirs.sort()
	
	for prir in prirs:
		for module in modules_data:
			if int(module.id) in active_modules:
				if int(module.prir) == prir:
					new_active_modules.append(int(module.id))
	
	active_modules = new_active_modules

func update_active_modules(id, conf, order):
	var cur_order_idx = order.find(id)
	
	if cur_order_idx > -1:
		
		var can_add = true
		
		for conf_id in conf:
			var conf_order_idx = order.find(int(conf_id))
			if conf_order_idx > -1:
				if conf_order_idx > cur_order_idx:
					can_add = false
		
		if can_add: active_modules.append(id)
	


func process_module(module, lvl, mech):
	
	var values = module.vals
#
#	print("=====================")
#	print(module.name)
#	print(module.type)
#	print(values)
	
	match module.type:
		
		"mech":
			
			for val_key in values.keys():
				
				match val_key:
					
					"all":
						for part_val_key in values[val_key]:
							match part_val_key:
								"+hp" : 
									for part in ["lt","rt","lb","rb"]:
										mech_part_plus_hp(part, values[val_key][part_val_key], lvl, mech)
								"+weight" :
									mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					"lt" : 
						for part_val_key in values[val_key]:
							match part_val_key:
								"+hp" : mech_part_plus_hp(val_key, values[val_key][part_val_key], lvl, mech)
								"+weight" : mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					"rt" : 
						for part_val_key in values[val_key]:
							match part_val_key:
								"+hp" : mech_part_plus_hp(val_key, values[val_key][part_val_key], lvl, mech)
								"+weight" : mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					"lb" : 
						for part_val_key in values[val_key]:
							match part_val_key:
								"+hp" : mech_part_plus_hp(val_key, values[val_key][part_val_key], lvl, mech)
								"+weight" : mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					"rb" : 
						for part_val_key in values[val_key]:
							match part_val_key:
								"+hp" : mech_part_plus_hp(val_key, values[val_key][part_val_key], lvl, mech)
								"+weight" : mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					"legs" : 
							
						$"../graphics/legs/leg01".texture = load("res://assets/graphics/chars/body_light/Foot0" + str(lvl+1) + "_Left.png")
						$"../graphics/legs/leg02".texture = load("res://assets/graphics/chars/body_light/Foot0" + str(lvl+1) + "_Right.png")
							
						for part_val_key in values[val_key]:
							
							match part_val_key:
								
								"+fuel" : mech_plus_fuel(values[val_key][part_val_key], lvl, mech)
								"+speed" : mech_plus_speed(values[val_key][part_val_key], lvl, mech)
					
					"stealth" :
						
						for part_val_key in values[val_key]:
							
							$"../graphics/base/center".texture = load("res://assets/graphics/chars/body_light/heart02.png")
							$"../graphics/base/center/det".texture = load("res://assets/graphics/chars/body_light/heart02_d.png")
							
							match part_val_key:
								
								"+time":
									mech_plus_stealth_time(values[val_key][part_val_key], lvl, mech)
								"delay":
									mech_set_stealth_delay(values[val_key][part_val_key], mech)
						
					"dash" : 
						
						for part_val_key in values[val_key]:
							
							match part_val_key:
								
								"distance":
									mech_set_dash_distance(values[val_key][part_val_key][lvl-1], mech)
								"reload":
									mech_set_dash_reload(values[val_key][part_val_key][lvl-1], mech)
						
					"shield" : 
						
						for part_val_key in values[val_key]:
							
							match part_val_key:
								"type":
									mech_set_shield_type(values[val_key][part_val_key], mech)
								"hp":
									mech_set_shield_hp(values[val_key][part_val_key][lvl-1], mech)
								"regen_hp":
									mech_set_shield_regen_hp(values[val_key][part_val_key], mech)
								"regen_delay":
									mech_set_shield_regen_delay(values[val_key][part_val_key], mech)
					
		"weapon":
			
			for val_key in values.keys():
				
				match val_key:
						
					"laser" : 
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"init" : 
						
									$"../weapon".WTYPE = WTYPE_LASER
									$"../weapon".GUN_LVL = lvl-1
									$"../weapon/visual/base".texture = load("res://assets/graphics/weapon/base/laser_"+ str(lvl) +".png")
									$"../weapon/visual/bolt".texture = null
									$"../weapon/visual/clip".texture = load("res://assets/graphics/weapon/clip/laser_1.png")
									$"../weapon/visual/barrel".texture = load("res://assets/graphics/weapon/barrels/laser_1.png")
									if $"../weapon".SECOND_BASE:
										$"../weapon/visual/base_s".texture = load("res://assets/graphics/weapon/base/laser_s_" + str(lvl) + ".png")
										$"../weapon/visual/barrel".texture = load("res://assets/graphics/weapon/barrels/laser_s_1.png")
									
								"dmg_init" : $"../weapon".DMG = values[val_key][part_val_key][lvl-1]
								"shield_dmg" : $"../weapon".SHIELD_DMG = values[val_key][part_val_key][lvl-1]
					
					"rocket" : 
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"init" : 
									
									$"../weapon".WTYPE = WTYPE_ROCKET
									$"../weapon/visual/base".texture = load("res://assets/graphics/weapon/base/rocket.png")
									$"../weapon/visual/bolt".texture = null
									$"../weapon/visual/clip".texture = load("res://assets/graphics/weapon/clip/rocket_1.png")
									$"../weapon/visual/barrel".texture = load("res://assets/graphics/weapon/barrels/rocket_1.png")
								
								"dmg_init" : $"../weapon".DMG = values[val_key][part_val_key]
								"radius" : $"../weapon".ROCKET_RAD = values[val_key][part_val_key]
					
					"shotgun" :
						
						if values[val_key] == null:
							
							$"../weapon".WTYPE = WTYPE_SHOTGUN
							
							var second_base = ""
							
							if $"../weapon".SECOND_BASE:
								second_base = "s_"
							
							$"../weapon/visual/boost".texture = load("res://assets/graphics/weapon/boost/shotgun_" + second_base + str(lvl) + ".png")
					
					"snipegun" : 
						
						if values[val_key] == null:
							
							$"../weapon".WTYPE = WTYPE_SNIPEGUN
							
							var second_base = ""
							
							if $"../weapon".SECOND_BASE:
								second_base = "s_"
							
							$"../weapon/visual/boost".texture = load("res://assets/graphics/weapon/boost/snipegun_" + second_base + str(lvl) + ".png")
					
					"firearm" : 
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"spread" : $"../weapon".SPREAD = int(values[val_key][part_val_key])
								"recoil" : $"../weapon".MAIN_RECOIL = values[val_key][part_val_key]
								"penetrate" : $"../weapon".PENETRATE = values[val_key][part_val_key]
								"dmg" : $"../weapon".DMG = values[val_key][part_val_key][lvl-1]
								"+bullets" : $"../weapon".BULLETS = values[val_key][part_val_key]*lvl
								"interval" : $"../weapon".BULLETS_DELAY = values[val_key][part_val_key][lvl-1]
					
					"barrel" : 
						
						# visual
						var vis_name = "default_"
						var second_base = ""
						
						match $"../weapon".WTYPE:
							
							WTYPE_LASER:
								vis_name = "laser_"
							
							WTYPE_ROCKET:
								vis_name = "rocket_"
						
						if $"../weapon".SECOND_BASE:
							second_base = "s_"
							
						$"../weapon/visual/barrel".texture = load("res://assets/graphics/weapon/barrels/" + vis_name + second_base + str(lvl+1) + ".png")
						
					"clip" :
						
						$"../weapon".MAX_AMMO += lvl
						$"../weapon".CLIP_LVL = lvl
						
						var tex_path = "res://assets/graphics/weapon/clip/"
						
						match $"../weapon".WTYPE:
							
							WTYPE_LASER:
								
								tex_path += "laser_" + str(lvl+1) + ".png"
								
							WTYPE_ROCKET:
								
								tex_path += "rocket_" + str(lvl+1) + ".png"
								
							_:
								
								tex_path += "default_" + str(lvl+1) + ".png"
						
						$"../weapon/visual/clip".texture = load(tex_path)
					
					"grenade":
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"dmg" : $"../weapon".GRENADE_DMG = values[val_key][part_val_key]
								"radius" : $"../weapon".GRENADE_RAD = values[val_key][part_val_key]
						
						$"../weapon".GRENADE_LAUNCHER = true
						
						var tex_path = "res://assets/graphics/weapon/base/grenade_"
						
						if $"../weapon".WTYPE == WTYPE_ROCKET: tex_path += "3.png"
						else:
							
							if $"../weapon".WTYPE != WTYPE_LASER:
								
								if $"../weapon".CLIP_LVL > 1 and $"../weapon/visual/re_boost".texture == null:
									tex_path += "4.png"
								else:
									tex_path += "1.png"
						
						$"../weapon/visual/grenade_launcher".texture = load(tex_path)
					
					"second_base":
						
						$"../weapon".SECOND_BASE = true
						$"../weapon/visual/base_s".texture = load("res://assets/graphics/weapon/base/base_s.png")
						$"../weapon/visual/barrel".texture = load("res://assets/graphics/weapon/barrels/default_s_1.png")
						$"../weapon/visual/bolt".texture = load("res://assets/graphics/weapon/misc/bolt_2.png")
					
					"re_boost":
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"reload_mult":
									
									# visual
									var vis_name = "default"
									var second_base = ""
									var id = ""
									
									match $"../weapon".WTYPE:
										
										WTYPE_LASER:
											vis_name = "laser"
											id = "_" + str($"../weapon".GUN_LVL)
										
										WTYPE_ROCKET:
											vis_name = "rocket"
									
									if $"../weapon".SECOND_BASE:
										second_base = "_s"
									
									$"../weapon/visual/re_boost".texture = load("res://assets/graphics/weapon/re_boost/" + vis_name + second_base + id + ".png")
									# property
									$"../weapon".RELOAD_MULT = values[val_key][part_val_key]
					
					"rate_of_fire":
						
						$"../weapon".WTYPE = WTYPE_BOOST
						
						var second_base = ""
						
						if $"../weapon".SECOND_BASE:
							second_base = "s_"
						
						$"../weapon/visual/boost".texture = load("res://assets/graphics/weapon/boost/boost_"+second_base+str(lvl)+".png")
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"rate":
									# property
									var rate = values[val_key][part_val_key][lvl-1]
									$"../weapon".MAIN_RECOIL = rate
									
					
					"aim":
						
						$"../weapon/visual/aim".texture = load("res://assets/graphics/weapon/misc/scope_1.png")
						if $"..".is_master:
							$"/root/game/camera".aim = true
					
					"all":
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"+weight" : mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					"no_boost":
						
						if $"../weapon".WTYPE != WTYPE_NO_BOOST: continue
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"dmg" : $"../weapon".DMG *= (1 + values[val_key][part_val_key][lvl-1])
								"+weight" : mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					"boost":
						
						if $"../weapon".WTYPE != WTYPE_BOOST: continue
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"dmg" : $"../weapon".DMG *= (1 + values[val_key][part_val_key][lvl-1])
								"+weight" : mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					"shotgun":
						
						if $"../weapon".WTYPE != WTYPE_SHOTGUN: continue
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"dmg" : $"../weapon".DMG *= (1 + values[val_key][part_val_key][lvl-1])
								"+weight" : mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					"snipegun":
						
						if $"../weapon".WTYPE != WTYPE_SNIPEGUN: continue
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"dmg" : $"../weapon".DMG *= (1 + values[val_key][part_val_key][lvl-1])
								"+weight" : mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					"rocket":
						
						if $"../weapon".WTYPE != WTYPE_ROCKET: continue
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"dmg" : $"../weapon".DMG *= (1 + values[val_key][part_val_key][lvl-1])
								"+weight" : mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					"laser":
						
						if $"../weapon".WTYPE != WTYPE_LASER: continue
						
						for part_val_key in values[val_key]:
							match part_val_key:
								
								"dmg" : $"../weapon".DMG *= (1 + values[val_key][part_val_key][lvl-1])
								"+weight" : mech_plus_weight(values[val_key][part_val_key], lvl, mech)
					
					
func mech_set_shield_type(value, mech):
	mech.shield_type = int(value)

func mech_set_shield_hp(value, mech):
	mech.shield_max_hp = value
	mech.shield_hp = mech.shield_max_hp

func mech_set_shield_regen_hp(value, mech):
	mech.shield_hp_regen = value

func mech_set_shield_regen_delay(value, mech):
	mech.shield_regen_delay = value

func mech_plus_fuel(amount, lvl, mech):
	mech.JET_FUEL_MAX += amount*lvl
	mech.jet_fuel = mech.JET_FUEL_MAX

func mech_plus_speed(amount, lvl, mech):
	mech.WALK_SPEED_BONUS = amount*lvl

func mech_set_dash_distance(value, mech):
	mech.dash_distance = value

func mech_set_dash_reload(value, mech):
	mech.dash_reload = value

func mech_plus_stealth_time(amount, lvl, mech):
	mech.stealth = amount*lvl
	mech.stealth_mode = 0

func mech_set_stealth_delay(value, mech):
	mech.stealth_delay = value

func mech_plus_weight(amount, lvl, mech):
	mech.MASS += amount*lvl

func mech_part_plus_hp(part, amount, lvl, mech):
	
	match part:
		
		"lt": mech.heal_hitbox(0, amount*lvl)
		"rt": mech.heal_hitbox(1, amount*lvl)
		"lb": mech.heal_hitbox(2, amount*lvl)
		"rb": mech.heal_hitbox(3, amount*lvl)