extends Node

var fullscreen = false
var sound = true
var ver = "debug"

var server_port = 10568

var cfg = {
	"nickname" : "newbe",
	"ip" : "homelessgunners.tk",
	"port" : 10568,
	"fullscreen" : false,
	"sound" : true
}

onready var http := HTTPRequest.new() as HTTPRequest
onready var net

func _ready():
	http.name = 'http'
	add_child(http)
	
	get_tree().get_root().connect("size_changed", self, "resize")
	if fullscreen:
		OS.window_fullscreen = true
    

func print_log(text):
	var datetime = OS.get_datetime().duplicate()
	for i in datetime.keys():
		if i != "year" and i != "dst": datetime[i] = str(datetime[i]).pad_zeros(2)
	prints("{year}/{month}/{day} {hour}:{minute}:{second}".format(datetime),text)


func update_sound():
	AudioServer.set_bus_mute(0, !sound)

func resize():
	return
#	if has_node("/root/game/fg"):
#		for i in $"/root/game/fg".get_children():
#			if i is Control:
#				i.rect_size = OS.get_window_size()
#		$"/root/game/fg/tab_menu".update_tab_menu()
#		$"/root/game/fg/chat".redraw_chat()