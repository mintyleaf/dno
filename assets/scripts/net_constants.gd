extends Node

var PORT = 10568
var MAX_CLIENTS = 16
var MAX_REMOTES = 4

const PLAYER_CONNECT = 0
const PLAYER_DISCONNECT = 1
const PLAYER_DATA = 2
const MESSAGE = 3
const ID = 4
const MAP = 5
const HIT = 6
const POINT = 7
const FEED = 8
const TEAM_POINTS = 9
const TIMER = 10
const STATS = 11
const ALIVE = 12
const EXIT = 13
const F = 14
const MODULE = 15
const LOADING_END = 16
const RESPAWN = 17
const TEAM = 18
const MODULES_DATA = 19
const WEAPONS_DATA = 20
const TILEMAP_HIT = 21
const PROP = 22
const EXPLOSION = 23
const MEGUMINS = 24
const CFG = 25

const PD_RESPAWN = 0
const PD_DIE = 1
const PD_MOVE = 2
const PD_KILL = 3
const PD_SEAT = 4
const PD_HEAL = 5
const PD_MASS_RESET = 6
const PD_MODULE = 7
const PD_MODULES_ORDER = 8
const PD_MONY = 9

const UN_MECH_DATA = 0
const UN_WEAPON_DATA = 1
const UN_PROP_DATA = 2

const MECH_DATA_CROUCH = 0
const MECH_DATA_WALK_LEFT = 1
const MECH_DATA_WALK_RIGHT = 2
const MECH_DATA_JUMP = 3
const MECH_DATA_JET = 4
const MECH_DATA_MOUSE_POS = 5
const MECH_DATA_POS = 6
const MECH_DATA_VEL = 7
const MECH_DATA_JET_FUEL = 8
const MECH_DATA_DASH = 9
const MECH_DATA_SHIELD_HP = 10
const MECH_DATA_HIDDEN = 11
const MECH_DATA_STEALTH_MODE = 12
const MECH_DATA_DRILL = 13
const MECH_DATA_AIM = 14

const WEAPON_DATA_SHOOT = 0
const WEAPON_DATA_RECOIL = 1

const PROP_POS = 0
const PROP_ROT = 1
const PROP_VEL = 2
const PROP_ANG = 3

const FEED_RESPAWN = 0
const FEED_KILL = 1
const FEED_HIT = 2
const FEED_CAP = 3
const FEED_WIN = 4
const FEED_PDEST = 5

const STATS_KILLS = 0
const STATS_DEATHS = 1
const STATS_CAPS = 2

const REMOTE_AUTH = 0

# server

const ALIVE_TIMER_RATE = 5.0
const ALIVE_TIMER_TIMEOUT = 15.0
const MAX_ERRORS = 20

const UNRELIABLE_OWNER_ID = 0
const UNRELIABLE_KEY = 1
const UNERLIABLE_VAL = 2

# client

const CLIENT_CONNECTION_TIMEOUT = 5.0

# game

const TEAM_RED = 0
const TEAM_BLU = 1
const NO_PLAYERS = 2

const TEAM_POINTS_RED = 0
const TEAM_POINTS_BLU = 1
const TEAM_POINTS_RED_BONUS = 2
const TEAM_POINTS_BLU_BONUS = 3
const TEAM_POINTS_MAX = 4

const MAX_TEAM_POINTS = {
	1 : 1000,
	2 : 2000,
	3 : 4000,
	4 : 8000
}

const TEAM_POINTS_PLAYER_PRICE = 100

const DEFAULT_PLAYER_RESPAWN_TIME = 3.0
const MIN_CUP_TIME = 3.0

const TIMER_MATCH_TIME = 360.0
const TIMER_WAIT_TIME = 15.0
const TIMER_F_TIME = 7.5
const MIN_PLAYERS_FOR_START = 2

const GAME_STAGE_DURATION = 90
const GAME_STAGE_WEIGHT_PL = 4
const GAME_STAGE_POINTS_MIN_PERCENT = 6.25

const GAME_MODE_NONE = 0
const GAME_MODE_WAIT = 1
const GAME_MODE_MATCH = 2
const GAME_MODE_PAY_RESPECT = 3

const POINT_STATE_DEACTIVE = 0
const POINT_STATE_ACTIVE = 1 
const POINT_STATE_CAP_RED = 2
const POINT_STATE_CAP_BLU = 3
const POINT_STATE_CAPPED_RED = 4
const POINT_STATE_CAPPED_BLU = 5

const DEFAULT_POINT_HP = 20
const DEFAULT_POINT_CAP_TIME = 5.0

const POINT_ENTER = 0
const POINT_EXIT = 1
const POINT_HIT = 2

const POINT_STATE = 0
const POINT_CAP = 1
const POINT_HP = 2

const INV_TIME = 1.0
const VOID_Y_LEVEL = 3600

const HITBOX_DEFAULT_BIG = 10
const HITBOX_DEFAULT_CENTER = 4

const HB_LT = 0
const HB_RT = 1
const HB_LB = 2
const HB_RB = 3
const HB_CE = 4
const SHIELD = 5

const TILEMAP_HP = 10
var HARD_CELLS = range(8,16)

const MAX_PROPS = 16

const EXPLOSION_PATH = "res://assets/scenes/proj/explosion.tscn"
const PROJ_SCN_PATH = "/root/game/proj"

# modules

const MODULES_PATH = "res://assets/json/modules"

const MODULE_BUY = 0
const MODULE_BUYC = 1
const MODULE_SELL = 2
const MODULE_UP = 3

const MODULE_SELL_RATE = 0.75
const MAX_MONY = {
	1 : 500,
	2 : 1000,
	3 : 2000,
	4 : 4000
}

# big things

const PRICELIST = {
	FEED_KILL : {
		1 : 40,
		2 : 80,
		3 : 120,
		4 : 160
	},
	FEED_CAP : 220,
	FEED_PDEST : 110
}

const DEFAULT_MECH = {
	"pos" : Vector2(0,0),
	"vel" : Vector2(0,0),
	# game
	"hitbox" : {
		HB_LT : HITBOX_DEFAULT_BIG,
		HB_RT : HITBOX_DEFAULT_BIG,
		HB_LB : HITBOX_DEFAULT_BIG,
		HB_RB : HITBOX_DEFAULT_BIG,
		HB_CE : HITBOX_DEFAULT_CENTER
	},
	"shield_hp" : 0,
	"stealth_mode" : -1,
	# input
	"mouse_pos" : Vector2(0,0),
	"crouch" : false,
	"walk_left" : false,
	"walk_right" : false,
	"jump" : false,
	"jet" : false,
	"dash" : false,
	"hidden" : false,
	# animation stuff
	"jet_fuel" : 0.0,
	"drill" : false,
	"aim" : false,
	# misc
	"weapon" : {
		"shoot" : {}
	}
}
