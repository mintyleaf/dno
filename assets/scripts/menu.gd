extends CanvasLayer

onready var Pages := $"Pages" as Control
onready var VPlayer := $"VideoPlayer" as VideoPlayer
onready var MainPage := $"Pages/MainPage" as Control
onready var SettingsPage := $"Pages/SettingsPage" as Control

onready var BtnPlay = $"Pages/MainPage/BtnPlay" as Button
onready var TweenBtnPlay = $"Pages/MainPage/BtnPlay/TweenBtnPlay" as Tween
onready var AuthForm = $"Pages/MainPage/VBoxAuth" as VBoxContainer

onready var BtnSettings = $"Pages/MainPage/VBoxControls/BtnSettings" as Button
onready var BtnExit = $"Pages/MainPage/VBoxControls/BtnExit" as Button
onready var BtnMenu = $"Pages/SettingsPage/VBoxControls/BtnMenu" as Button

onready var Username = $"Pages/MainPage/VBoxAuth/LEditUsername" as LineEdit
onready var Password = $"Pages/MainPage/VBoxAuth/LEditPassword" as LineEdit

onready var Test = $"Pages/MainPage/Test"

onready var current_page = MainPage

onready var Auth = preload('res://assets/scripts/auth.gd').new()
onready var Conf = preload('res://assets/scripts/config.gd').new()

func refresh_size_pages() -> void:
	if is_instance_valid(Pages) and is_instance_valid(VPlayer):
		Pages.rect_size = Vector2(VPlayer.rect_size.x * 2, VPlayer.rect_size.y)
		if current_page == MainPage:
			Pages.rect_position = Vector2(0, 0)
		elif current_page == SettingsPage:
			Pages.rect_position = Vector2(-Pages.rect_size.x / 2, 0)
			
		for menu_ui_node in get_tree().get_nodes_in_group('menu_ui'):

			if menu_ui_node.name == 'BtnSettings': 
				menu_ui_node.get('custom_fonts/font').set_size(calc_font_size(BtnSettings.default_min_font_size, BtnSettings.default_max_font_size))
			elif menu_ui_node.name == 'BtnExit':
				menu_ui_node.get('custom_fonts/font').set_size(calc_font_size(BtnExit.default_min_font_size, BtnExit.default_max_font_size))
			elif menu_ui_node.name == 'BtnPlay':
				menu_ui_node.get('custom_fonts/font').set_size(calc_font_size(BtnPlay.default_min_font_size, BtnPlay.default_max_font_size))
			elif menu_ui_node.name == 'BtnMenu':
				menu_ui_node.get('custom_fonts/font').set_size(calc_font_size(BtnMenu.default_min_font_size, BtnMenu.default_max_font_size))
			elif menu_ui_node.name in ['LEditUsername', 'LEditPassword', 'BtnAuth']:
				var fs = calc_font_size(16, 42)
				menu_ui_node.get('custom_fonts/font').set_size(fs)
				yield(get_tree(), "idle_frame") #fix
				menu_ui_node.rect_size.x = calc_min_width(menu_ui_node)


		var m = calc_font_size(BtnPlay.default_min_font_size, BtnPlay.default_max_font_size)
		BtnPlay.tween_values_font_size = [m, m + (BtnPlay.default_max_font_size - BtnPlay.default_min_font_size)]


func calc_font_size(min_font_size: int = 150, max_font_size: int = 250, min_width: int = 1280, max_width: int = 2560) -> float:
	return (min_font_size + (max_font_size - min_font_size) * ((VPlayer.rect_size.x - min_width) / (max_width - min_width)))


func calc_min_width(ui_node: Control) -> float:
	var font_size = ui_node.get('custom_fonts/font').get_size()
	if !ui_node.has_meta('min_chars'):
		ui_node.set_meta('min_chars', ui_node.rect_size.x / (font_size / 2))
		return ui_node.rect_size.x
	else:
		return (font_size / 2) * ui_node.get_meta('min_chars')


func _ready() -> void:
	
	OS.set_window_title("DNO menu")
	
	#version.text = "build: " + state.ver

	if !OS.is_debug_build():
		$"/root/updater".update_dno_pck()
	else:
		Test.visible = true
	
	for menu_ui_node in get_tree().get_nodes_in_group('menu_ui'):
		calc_min_width(menu_ui_node)
	refresh_size_pages()


func _on_VideoPlayer_resized() -> void:
	refresh_size_pages()


func _on_VideoPlayer_finished() -> void:
	VPlayer.play() #replay


func _on_BtnAuth_pressed():
	Auth.auth_post(Username.text, Password.text)


func _on_BtnPlay_pressed():
	var game_scn = load("res://assets/scenes/game.tscn").instance()
	game_scn.loaded_ip = Test.get_node('IP').text
	game_scn.loaded_port = int(Test.get_node('Port').text)
	game_scn.loaded_nickname = Test.get_node('Nick').text
	$"/root".add_child(game_scn)
	queue_free()
