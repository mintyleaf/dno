shader_type canvas_item;
render_mode unshaded;

uniform int blurSize : hint_range(0,20); 

void fragment() 
{
	vec4 c = textureLod(SCREEN_TEXTURE, SCREEN_UV, float(blurSize)/10.0);
	COLOR = c;
}