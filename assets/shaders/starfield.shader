shader_type canvas_item;

uniform vec2 position;
uniform sampler2D starfield;
uniform sampler2D noise1;
uniform sampler2D noise2;

void fragment() {
	if (AT_LIGHT_PASS) {
		COLOR.rgb = texture(noise1, SCREEN_UV).rgb;
	} else {
		COLOR = texture(TEXTURE, UV);
		
		float aspect = TEXTURE_PIXEL_SIZE.y/TEXTURE_PIXEL_SIZE.x;
		vec2 pos = position * TEXTURE_PIXEL_SIZE;
		vec2 uv = UV;
		uv.x *= aspect;
		
		vec2 starfield_uv = uv * 1.0 + pos * 0.5;
		vec4 stars = texture(starfield, starfield_uv);
		starfield_uv = uv * 1.05 + pos * 0.4 + vec2(0.5, 0.3);
		stars += texture(starfield, starfield_uv);
		starfield_uv = uv * 1.2 + pos * 0.3 + vec2(0.7, 0.1);
		stars += texture(starfield, starfield_uv);
		starfield_uv = uv * 1.4 + pos * 0.1 + vec2(0.3, 0.7);
		stars += texture(starfield, starfield_uv);
		
		vec2 noise_uv = uv * 1.0 + pos * 0.1;
		vec3 neb = texture(noise1, noise_uv).rgb * vec3(0.5, 0.0, 0.0);
		noise_uv = uv * 0.9 + pos * 0.1;
		neb += texture(noise1, noise_uv).rgb * vec3(0.4, 0.1, 0.0);
		noise_uv = uv * 0.8 + pos * 0.2;
		neb += texture(noise1, noise_uv).rgb * vec3(0.1, 0.2, 0.0);
		noise_uv = uv * 1.0 + pos * 0.2 + vec2(0.5, 0.5);
		neb -= texture(noise1, noise_uv).rgb * vec3(0.5, 0.3, 0.0);
		noise_uv = uv * 1.3 + pos * 0.3;
		neb -= texture(noise1, noise_uv).rgb * vec3(0.5, 0.0, 0.0);
		noise_uv = uv * 1.3 + pos * 0.4 + vec2(0.3, 0.1);
		neb += texture(noise2, noise_uv).rgb * vec3(0.1, 0.1, 0.3);
		noise_uv = uv * 1.3 + pos * 1.0 + vec2(0.7, 0.3);
		neb += texture(noise1, noise_uv).rgb * vec3(0.0, 0.1, 0.2);
		
		neb.r = clamp(neb.r, 0.0, 1.0);
		neb.g = clamp(neb.g, 0.0, 1.0);
		neb.b = clamp(neb.b, 0.0, 1.0);
		
		COLOR.rgb = COLOR.rgb + stars.rgb + neb.rgb;
	}
}