extends TileMap

var view_mode = false
var prev_view = false

func reset_cursor():
	$drawer.reset_cursor()

func reset_rect_edit():
	$drawer.reset_rect_edit()

func draw_cursor():
	$drawer.draw_cursor(world_to_map(get_local_mouse_position())*$"../..".current_cell_size)

func draw_rect_edit(s,e,c):
	$drawer.draw_rect_edit(s,e,c)

func refresh_group_tiles():
	update()
	var g_tiles = $"../..".groups_tiles
	if view_mode:
		if !prev_view:
			for i in g_tiles.keys():
				randomize()
				set_cellv(i,$"../..".groups[g_tiles[i]][randi()%$"../..".groups[g_tiles[i]].size()])
			prev_view = true
	else:
		for i in g_tiles.keys():
			prev_view = false
			set_cellv(i,-1)

func _draw():
	if view_mode: return
	var g_tiles = $"../..".groups_tiles
	
	for i in g_tiles.keys():
		draw_rect(Rect2(i*Vector2(1,1)*$"../..".current_cell_size,Vector2(1,1)*$"../..".current_cell_size),$"../..".groups_colors[g_tiles[i]])