extends Panel

const MODE_EDIT = 0
const MODE_EDIT_RECT = 1
const MODE_VIEW = 2
const MODE_SET_ROOT = 3

var mode = MODE_EDIT

var mouse_drag = Vector2(0,0)
var mouse_coords = Vector2(0,0)
var mouse_predrag_coords = Vector2(0,0)
var mouse_afterdrag_coords = Vector2(0,0)
var tiles_coords = Vector2(0,0)
var tiles_offset = Vector2(0,0)
var tiles_offset_sum = Vector2(0,0)
var tiles_zoom = Vector2(1,1)

var ui_busy = false

var current_tileset = null
var current_cell_size = 64
var current_tile = -1
var last_tileset_path = ""

var groups = {}
var groups_colors = {}
var current_group = []
var groups_tiles = {}

var struct_name = ""
var struct_type = 0 # 0 - vert, 1 - horiz
var struct_length = 0
var struct_object_root = Vector2(0,0)

var work_folder = ""
var work_folder_dict = {}
var cur_work_folder_id = -1

func _ready():
	$tilemap_monitor/tiles.clear()
	_on_select_mode_toggled(false)
	connect("resized",self,"reset")
	reset()

func reset():
	tiles_coords = Vector2(0,0)
	tiles_zoom = Vector2(1,1)
	tiles_offset = Vector2(0,0)
	tiles_offset_sum = (($tilemap_monitor.rect_size/2 - 
	Vector2(1,1)*current_cell_size).snapped(Vector2(1,1)*current_cell_size))
	$tw.stop_all()
	$tw.interpolate_property($tilemap_monitor/tiles,"position",$tilemap_monitor/tiles.position,
	($tilemap_monitor.rect_size/2 - $tilemap_monitor.rect_size*tiles_zoom.x/2) + tiles_offset_sum * tiles_zoom.x,0.2,
	Tween.TRANS_QUAD,Tween.EASE_OUT)
	$tw.interpolate_property($tilemap_monitor/tiles,"scale",$tilemap_monitor/tiles.scale,tiles_zoom,0.2,
	Tween.TRANS_QUAD,Tween.EASE_OUT)
	$tw.start()
	$tabs/Tilemap/tiles_list.rect_min_size.y = rect_size.y-282
	$tabs/File/work_folder_browse.rect_min_size.y = rect_size.y-256
	$tabs/File/operations/Save.disabled = true
	$tabs/File/operations/Delete.disabled = true

func set_cell(pos,index):
	if index == -1:
		if groups_tiles.has(pos):
			groups_tiles.erase(pos)
			$tilemap_monitor/tiles.refresh_group_tiles()
		$tilemap_monitor/tiles.set_cellv(pos,index)
		return
	var group_text_id = int($tabs/Tilemap/tiles_list.get_item_text(index).replace("group ",""))
	if !group_text_id in groups.keys():
		if groups_tiles.has(pos):
			groups_tiles.erase(pos)
			$tilemap_monitor/tiles.refresh_group_tiles()
		$tilemap_monitor/tiles.set_cellv(pos,index)
	else:
		if !groups_colors.has(group_text_id):
			print("err!")
			return
		var c = groups_colors[group_text_id]
		groups_tiles[pos] = group_text_id
		$tilemap_monitor/tiles.refresh_group_tiles()
		

func _process(delta):
	
	if !ui_busy:
		$info_text.text = "Cursor: "+str($tilemap_monitor/tiles.world_to_map(
		$tilemap_monitor/tiles.get_local_mouse_position()))+", tile id: "+str(current_tile)
		$tilemap_monitor/tiles.draw_cursor()
	else:
		$tilemap_monitor/tiles.reset_cursor()
		return
	
	if $tabs.get_rect().has_point($tabs.get_local_mouse_position()): return
	
	if Input.is_action_pressed("crouch"):
		mode = MODE_VIEW
	elif Input.is_action_pressed("aim"):
		mode = MODE_EDIT_RECT
	elif $tabs/Tilemap/tilemap_size/edit_root_point.pressed:
		mode = MODE_SET_ROOT
	else: mode = MODE_EDIT
	
	match mode:
		MODE_SET_ROOT:
			if Input.is_action_just_released("shoot"):
				$tabs/Tilemap/tilemap_size/edit_root_point.pressed = false
				struct_object_root = $tilemap_monitor/tiles.world_to_map(
				$tilemap_monitor/tiles.get_local_mouse_position())
				$tilemap_monitor/tiles.update()
				$tabs/File/operations/Save.disabled = false
		MODE_EDIT:
			if Input.is_action_pressed("shoot"):
				set_cell($tilemap_monitor/tiles.world_to_map(
				$tilemap_monitor/tiles.get_local_mouse_position()),current_tile)
			elif Input.is_action_pressed("right_button"):
				set_cell($tilemap_monitor/tiles.world_to_map(
				$tilemap_monitor/tiles.get_local_mouse_position()),-1)
			elif Input.is_action_pressed("middle_button"):
				mouse_drag = (mouse_coords - get_local_mouse_position()).snapped(
				Vector2(current_cell_size,current_cell_size)*tiles_zoom.x)
				tiles_offset = - mouse_drag / tiles_zoom.x
				$tw.interpolate_property($tilemap_monitor/tiles,"position",$tilemap_monitor/tiles.position,
				tiles_coords - mouse_drag,0.2,Tween.TRANS_QUAD,Tween.EASE_OUT)
				$tw.start()
			else:
				if mouse_drag != Vector2(0,0):
					mouse_drag = Vector2(0,0)
					tiles_offset_sum += tiles_offset
				tiles_coords = $tilemap_monitor/tiles.position
				mouse_coords = get_local_mouse_position()
		MODE_EDIT_RECT:
			if Input.is_action_pressed("shoot"):
				mouse_afterdrag_coords = $tilemap_monitor/tiles.world_to_map($tilemap_monitor/tiles.get_local_mouse_position())
				$tilemap_monitor/tiles.draw_rect_edit(mouse_predrag_coords,mouse_afterdrag_coords,Color(0.25,0.75,0.25,0.5))
			elif Input.is_action_pressed("right_button"):
				mouse_afterdrag_coords = $tilemap_monitor/tiles.world_to_map($tilemap_monitor/tiles.get_local_mouse_position())
				$tilemap_monitor/tiles.draw_rect_edit(mouse_predrag_coords,mouse_afterdrag_coords,Color(0.75,0.25,0.25,0.5))
			
			elif Input.is_action_just_released("shoot"):
				$tilemap_monitor/tiles.reset_rect_edit()
				
				if mouse_afterdrag_coords.x >= mouse_predrag_coords.x and mouse_afterdrag_coords.y >= mouse_predrag_coords.y:
					for i in range(mouse_predrag_coords.x,mouse_afterdrag_coords.x+1):
						for j in range(mouse_predrag_coords.y,mouse_afterdrag_coords.y+1):
							set_cell(Vector2(i,j),current_tile)
							
				elif mouse_predrag_coords.x >= mouse_afterdrag_coords.x and mouse_predrag_coords.y >= mouse_afterdrag_coords.y:
					for i in range(mouse_afterdrag_coords.x,mouse_predrag_coords.x+1):
						for j in range(mouse_afterdrag_coords.y,mouse_predrag_coords.y+1):
							set_cell(Vector2(i,j),current_tile)
							
				elif mouse_predrag_coords.x >= mouse_afterdrag_coords.x and mouse_afterdrag_coords.y >= mouse_predrag_coords.y:
					for i in range(mouse_afterdrag_coords.x,mouse_predrag_coords.x+1):
						for j in range(mouse_predrag_coords.y,mouse_afterdrag_coords.y+1):
							set_cell(Vector2(i,j),current_tile)
							
				elif mouse_afterdrag_coords.x >= mouse_predrag_coords.x and mouse_predrag_coords.y >= mouse_afterdrag_coords.y:
					for i in range(mouse_predrag_coords.x,mouse_afterdrag_coords.x+1):
						for j in range(mouse_afterdrag_coords.y,mouse_predrag_coords.y+1):
							set_cell(Vector2(i,j),current_tile)
							
			elif Input.is_action_just_released("right_button"):
				$tilemap_monitor/tiles.reset_rect_edit()
				
				if mouse_afterdrag_coords.x >= mouse_predrag_coords.x and mouse_afterdrag_coords.y >= mouse_predrag_coords.y:
					for i in range(mouse_predrag_coords.x,mouse_afterdrag_coords.x+1):
						for j in range(mouse_predrag_coords.y,mouse_afterdrag_coords.y+1):
							set_cell(Vector2(i,j),-1)
							
				elif mouse_predrag_coords.x >= mouse_afterdrag_coords.x and mouse_predrag_coords.y >= mouse_afterdrag_coords.y:
					for i in range(mouse_afterdrag_coords.x,mouse_predrag_coords.x+1):
						for j in range(mouse_afterdrag_coords.y,mouse_predrag_coords.y+1):
							set_cell(Vector2(i,j),-1)
							
				elif mouse_predrag_coords.x >= mouse_afterdrag_coords.x and mouse_afterdrag_coords.y >= mouse_predrag_coords.y:
					for i in range(mouse_afterdrag_coords.x,mouse_predrag_coords.x+1):
						for j in range(mouse_predrag_coords.y,mouse_afterdrag_coords.y+1):
							set_cell(Vector2(i,j),-1)
							
				elif mouse_afterdrag_coords.x >= mouse_predrag_coords.x and mouse_predrag_coords.y >= mouse_afterdrag_coords.y:
					for i in range(mouse_predrag_coords.x,mouse_afterdrag_coords.x+1):
						for j in range(mouse_afterdrag_coords.y,mouse_predrag_coords.y+1):
							set_cell(Vector2(i,j),-1)
							
			else:
				mouse_predrag_coords = $tilemap_monitor/tiles.world_to_map($tilemap_monitor/tiles.get_local_mouse_position())
		MODE_VIEW:
			if Input.is_action_pressed("shoot"):
				mouse_drag = (mouse_coords - get_local_mouse_position()).snapped(
				Vector2(current_cell_size,current_cell_size)*tiles_zoom.x)
				tiles_offset = - mouse_drag / tiles_zoom.x
				$tw.interpolate_property($tilemap_monitor/tiles,"position",$tilemap_monitor/tiles.position,
				tiles_coords - mouse_drag,0.2,Tween.TRANS_QUAD,Tween.EASE_OUT)
				$tw.start()
			else:
				if mouse_drag != Vector2(0,0):
					mouse_drag = Vector2(0,0)
					tiles_offset_sum += tiles_offset
				tiles_coords = $tilemap_monitor/tiles.position
				mouse_coords = get_local_mouse_position()
			if Input.is_action_just_pressed("jump"):
				reset()
			
func _input(event):
	if event is InputEventMouseButton:
		if mode == MODE_VIEW:
			match event.button_index:
				5:
					if tiles_zoom.x > 0.25:
						tiles_zoom -= Vector2(0.05,0.05)
						$tw.interpolate_property($tilemap_monitor/tiles,"scale",$tilemap_monitor/tiles.scale,
						tiles_zoom,0.2,Tween.TRANS_QUAD,Tween.EASE_OUT)
						$tw.interpolate_property($tilemap_monitor/tiles,"position",$tilemap_monitor/tiles.position,
						($tilemap_monitor.rect_size/2 - $tilemap_monitor.rect_size*tiles_zoom.x/2) + tiles_offset_sum * tiles_zoom.x,0.2,
						Tween.TRANS_QUAD,Tween.EASE_OUT)
						$tw.start()
				4:
					if tiles_zoom.x < 4:
						tiles_zoom += Vector2(0.05,0.05)
						$tw.interpolate_property($tilemap_monitor/tiles,"scale",$tilemap_monitor/tiles.scale,
						tiles_zoom,0.2,Tween.TRANS_QUAD,Tween.EASE_OUT)
						$tw.interpolate_property($tilemap_monitor/tiles,"position",$tilemap_monitor/tiles.position,
						($tilemap_monitor.rect_size/2 - $tilemap_monitor.rect_size*tiles_zoom.x/2) + tiles_offset_sum * tiles_zoom.x,0.2,
						Tween.TRANS_QUAD,Tween.EASE_OUT)
						$tw.start()

func generate_tiles_list():
	$tabs/Tilemap/tiles_list.clear()
	var tiles_ids = current_tileset.get_tiles_ids()
	for i in tiles_ids:
		$tabs/Tilemap/tiles_list.add_item(str(i)+": "+current_tileset.tile_get_name(i),
		get_cutted_texture(current_tileset.tile_get_texture(i),
		current_tileset.tile_get_region(i)))

func get_cutted_texture(tex,reg):
	var img = tex.get_data()
	var new_img = img.get_rect(reg)
	var new_tex = ImageTexture.new()
	new_tex.create_from_image(new_img,0)
	return new_tex

func load_tileset(path,no_clear=false):
	var d = Directory.new()
	if d.file_exists(path):
		print("File exists...")
		var tileset_res = load(path)
		if tileset_res is TileSet:
			print("TileSet loaded!")
			$tabs/Tilemap/tileset_path.text = path
			current_tileset = tileset_res
			last_tileset_path = path
			$tilemap_monitor/tiles.tile_set = current_tileset
			if !no_clear:
				$tilemap_monitor/tiles.clear()
				generate_tiles_list()
			$tilemap_monitor/tiles.cell_size = current_tileset.tile_get_region(0).size
			$tabs/Tilemap/tilemap_size/x.value = current_tileset.tile_get_region(0).size.x
		else:
			print("Can't load this file as TileSet!")
	else:
		print("Can't find file at this path!")

func _on_open_pressed():
	$tileset_open.popup_centered(Vector2(400,400))
	ui_busy = true

func _on_tileset_path_text_entered(new_text):
	load_tileset(new_text)

func _on_tileset_open_file_selected(path):
	load_tileset(path)

func _on_tileset_open_popup_hide():
	ui_busy = false

func _on_x_value_changed(value):
	$tilemap_monitor/tiles.cell_size = Vector2(1,1)*value
	current_cell_size = value
	$tilemap_monitor/tiles.refresh_group_tiles()

func _on_col_hint_toggled(button_pressed):
	get_tree().debug_collisions_hint = button_pressed
	if last_tileset_path != "":
		load_tileset(last_tileset_path,true)

func _on_tiles_list_item_selected(index):
	current_tile = index
	if get_group_id_by_index(index) in groups.keys():
		$tabs/Tilemap/groups/color_group.color = groups_colors[get_group_id_by_index(index)]

func _on_tiles_list_multi_selected(index, selected):
	
	$tabs/Tilemap/groups/edit_group.disabled = true
	$tabs/Tilemap/groups/delete_group.disabled = true
	$tabs/Tilemap/groups/color_group.disabled = true
	for i in $tabs/Tilemap/tiles_list.get_selected_items():
		var group_text_id = int($tabs/Tilemap/tiles_list.get_item_text(i).replace("group ",""))
		if group_text_id in groups.keys():
			$tabs/Tilemap/groups/edit_group.disabled = false
			$tabs/Tilemap/groups/delete_group.disabled = false
			$tabs/Tilemap/groups/color_group.disabled = false
	
	if get_group_id_by_index(index) in groups.keys():
		var a = 0
		for i in $tabs/Tilemap/tiles_list.get_selected_items():
			if i in groups.keys():
				a+=1
		if a > 1:
			$tabs/Tilemap/tiles_list.unselect(index)
			return
			
		current_group = []
		
		$tabs/Tilemap/tiles_list.unselect_all()
		for i in groups[get_group_id_by_index(index)]:
			current_group.append(i)
			$tabs/Tilemap/tiles_list.select(i,false)
		$tabs/Tilemap/tiles_list.select(index,false)
		$tabs/Tilemap/groups/color_group.color = groups_colors[get_group_id_by_index(index)]
		
		return
	if !index in current_group and selected:
		current_group.append(index)
	elif index in current_group and !selected:
		current_group.erase(index)
	
	if current_group.size() > 1:
		$tabs/Tilemap/groups/new_group.disabled = false
		$tabs/Tilemap/groups/color_group.disabled = false
	else:
		$tabs/Tilemap/groups/color_group.disabled = true
		$tabs/Tilemap/groups/new_group.disabled = true

func _on_select_mode_toggled(button_pressed):
	if !button_pressed:
		$tabs/Tilemap/groups/edit_group.disabled = true
		$tabs/Tilemap/groups/new_group.disabled = true
		$tabs/Tilemap/groups/delete_group.disabled = true
		$tabs/Tilemap/groups/color_group.disabled = true
		$tabs/Tilemap/tiles_list.select_mode = ItemList.SELECT_SINGLE
	else:
		$tabs/Tilemap/tiles_list.select_mode = ItemList.SELECT_MULTI
		current_group = []

func _on_edit_group_pressed():
	var group_index = -1
	for i in $tabs/Tilemap/tiles_list.get_selected_items():
		if i in groups.keys():
			group_index = i
			break
	if group_index == -1: return
	
	$tilemap_monitor/tiles.refresh_group_tiles()
	groups[group_index] = current_group
	groups_colors[group_index] = $tabs/Tilemap/groups/color_group.color
	$tabs/Tilemap/groups/color_group.color = Color(1,1,1)

func _on_new_group_pressed():
	if current_group.size() < 2: return
	
	var idx = $tabs/Tilemap/tiles_list.get_item_count()
	for i in current_group:
		if i in groups.keys():
			$tabs/Tilemap/tiles_list.unselect_all()
			return
	groups[idx] = current_group
	groups_colors[idx] = $tabs/Tilemap/groups/color_group.color
	$tabs/Tilemap/groups/color_group.color = Color(1,1,1)
	current_group = []
	$tabs/Tilemap/tiles_list.add_item("group "+str(idx))

func _on_delete_group_pressed():
	if $tabs/Tilemap/tiles_list.get_selected_items().size() > 0:
		var group_id = $tabs/Tilemap/tiles_list.get_selected_items()[$tabs/Tilemap/tiles_list.get_selected_items().size()-1]
		var group_text_id = int($tabs/Tilemap/tiles_list.get_item_text(group_id).replace("group ",""))
		if group_text_id in groups.keys():
			groups.erase(group_text_id)
			$tabs/Tilemap/tiles_list.remove_item(group_id)
			groups_colors.erase(group_text_id)

func _on_color_group_color_changed(color):
	for i in $tabs/Tilemap/tiles_list.get_selected_items():
		var group_text_id = int($tabs/Tilemap/tiles_list.get_item_text(i).replace("group ",""))
		if group_text_id in groups.keys():
			groups_colors[group_text_id] = color

func _on_groups_mode_toggled(button_pressed):
	$tilemap_monitor/tiles.view_mode = button_pressed
	$tilemap_monitor/tiles.refresh_group_tiles()
	if last_tileset_path != "":
		load_tileset(last_tileset_path,true)

func get_group_id_by_index(idx):
	var group_text_id = int($tabs/Tilemap/tiles_list.get_item_text(idx).replace("group ",""))
	return group_text_id

func write_file(path):
	
	_on_groups_mode_toggled(false)
	
	var used_tiles = {}
	for i in $tilemap_monitor/tiles.tile_set.get_tiles_ids():
		used_tiles[i] = $tilemap_monitor/tiles.get_used_cells_by_id(i)
	var data = {
		"name" : struct_name,
		"type" : struct_type, # 0 - vert, 1 - horiz
		"length" : struct_length,
		"groups" : groups,
		"groups_colors" : groups_colors,
		"groups_tiles" : groups_tiles,
		"tiles" : used_tiles,
		"tileset" : last_tileset_path,
		"struct_object_root" : struct_object_root
		}
	var json_data = to_json(data)
	var f = File.new()
	f.open(path,File.WRITE_READ)
	f.store_string(json_data)
	f.close()
	
	_on_groups_mode_toggled($tabs/Tilemap/groups/groups_mode.pressed)

func read_file(path):
	
	$tilemap_monitor/tiles.clear()
	$tabs/Tilemap/tiles_list.clear()
	$tabs/Tilemap/groups/groups_mode.pressed = false
	_on_groups_mode_toggled(false)
	
	var f = File.new()
	f.open(path,File.READ)
	var json_data = f.get_as_text()
	f.close()
	var data = parse_json(json_data)
	struct_name = data["name"]
	struct_type = int(data["type"])
	struct_length = int(data["length"])
	var text_groups = data["groups"]
	var text_groups_colors = data["groups_colors"]
	var text_groups_tiles = data["groups_tiles"]
	var used_tiles = data["tiles"]
	last_tileset_path = data["tileset"]
	var text_struct_object_root = data["struct_object_root"]
	
	var int_groups = {}
	var int_groups_colors = {}
	var int_groups_tiles = {}
	
	for i in text_groups.keys():
		int_groups[int(i)] = []
		for j in text_groups[i]:
			int_groups[int(i)].append(int(j))
	
	for i in text_groups_colors.keys():
		var color_array = text_groups_colors[i].split(',')
		int_groups_colors[int(i)] = Color(float(color_array[0]),float(color_array[1]),float(color_array[2]),float(color_array[3]))
	
	for i in text_groups_tiles.keys():
		var vec = i.split(',')
		vec[0] = vec[0].replace('(','')
		vec[1] = vec[1].replace(')','')
		vec = Vector2(int(vec[0]),int(vec[1]))
		int_groups_tiles[vec] = int(text_groups_tiles[i])
	
	text_struct_object_root = text_struct_object_root.split(',')
	text_struct_object_root[0] = text_struct_object_root[0].replace('(','')
	text_struct_object_root[1] = text_struct_object_root[1].replace(')','')
	struct_object_root = Vector2(int(text_struct_object_root[0]),int(text_struct_object_root[1]))
	
	$tabs/File/length/length.value = struct_length
	$tabs/File/type/type.selected = struct_type
	$tabs/File/name/name.text = struct_name
	_on_length_value_changed(struct_length)
	_on_type_item_selected(struct_type)
	
	groups_tiles = int_groups_tiles
	groups = int_groups
	groups_colors = int_groups_colors
	
	load_tileset(last_tileset_path)
	
	for i in used_tiles:
		for j in used_tiles[i]:
			var vec = j.split(',')
			vec[0] = vec[0].replace('(','')
			vec[1] = vec[1].replace(')','')
			vec = Vector2(int(vec[0]),int(vec[1]))
			$tilemap_monitor/tiles.set_cellv(vec,int(i))
	$tilemap_monitor/tiles.refresh_group_tiles()
	generate_tiles_list()
	
	for i in groups.keys():
		$tabs/Tilemap/tiles_list.add_item("group "+str(i))

func work_folder_browse_generate_list():
	var dir = Directory.new()
	if dir.change_dir(work_folder) != OK:
		print("Can't open dir!")
		return
	$tabs/File/work_folder_browse.clear()
	work_folder_dict = {}
	_on_name_text_entered(struct_name)
	
	dir.list_dir_begin()
	var file_name = dir.get_next()
	while file_name != "":
		if !dir.current_is_dir():
			work_folder_dict[$tabs/File/work_folder_browse.get_item_count()] = file_name
			$tabs/File/work_folder_browse.add_item(file_name)
		file_name = dir.get_next()

func _on_work_folder_text_entered(new_text):
	work_folder = new_text
	work_folder_browse_generate_list()

func _on_select_pressed():
	$work_folder_select.popup_centered(Vector2(400,400))
	ui_busy = true

func _on_work_folder_select_dir_selected(dir):
	work_folder = dir
	work_folder_browse_generate_list()
	$tabs/File/work_folder.text = work_folder

func _on_work_folder_select_popup_hide():
	ui_busy = false

func _on_Save_pressed():
	write_file(work_folder+"/"+struct_name+".struct")
	work_folder_browse_generate_list()

func _on_work_folder_browse_item_activated(index):
	read_file(work_folder+"/"+work_folder_dict[index])

func _on_work_folder_browse_item_selected(index):
	$tabs/File/operations/Delete.disabled = false
	cur_work_folder_id = index

func _on_Delete_pressed():
	$tabs/File/operations/Delete.disabled = true
	var file_name = $tabs/File/work_folder_browse.get_item_text(cur_work_folder_id)
	cur_work_folder_id = -1
	var dir = Directory.new()
	if dir.file_exists(work_folder+"/"+file_name):
		if dir.remove(work_folder+"/"+file_name) != OK:
			print("Can't delete file!")
	else:
		print("Can't find file!")
	work_folder_browse_generate_list()

func _on_name_text_entered(new_text):
	struct_name = new_text
	if struct_name != "":
		var dir = Directory.new()
		if dir.dir_exists(work_folder) and work_folder != "":
			$tabs/File/operations/Save.disabled = false
	else:
		$tabs/File/operations/Save.disabled = true

func _on_length_value_changed(value):
	struct_length = value
	$tilemap_monitor/tiles/drawer.length = struct_length
	$tilemap_monitor/tiles/drawer.update()

func _on_type_item_selected(ID):
	struct_type = ID
	$tilemap_monitor/tiles/drawer.type = ID
	$tilemap_monitor/tiles/drawer.update()