extends Node2D

var rect_start = null
var rect_end = null
var rect_c = null

var cursor_p = null
var type = 0
var length = 0

func reset_rect_edit():
	rect_c = Color(1,1,0.25,0.25)
	yield(get_tree().create_timer(0.1),"timeout")
	rect_start = null
	rect_end = null
	rect_c = null

func reset_cursor():
	cursor_p = null
	update()

func draw_rect_edit(s,e,c):
	if e.x >= s.x and e.y >= s.y:
		rect_start = s*$"../../..".current_cell_size
		rect_end = (e-s)*$"../../..".current_cell_size + Vector2($"../../..".current_cell_size,$"../../..".current_cell_size)
	elif s.x >= e.x and s.y >= e.y:
		rect_start = s*$"../../..".current_cell_size + Vector2($"../../..".current_cell_size,$"../../..".current_cell_size)
		rect_end = -(s-e)*$"../../..".current_cell_size - Vector2($"../../..".current_cell_size,$"../../..".current_cell_size)
	elif s.x >= e.x and e.y >= s.y:
		rect_start = s*$"../../..".current_cell_size + Vector2($"../../..".current_cell_size,0)
		rect_end = -(s-e)*$"../../..".current_cell_size + Vector2(-$"../../..".current_cell_size,$"../../..".current_cell_size)
	elif e.x >= s.x and s.y >= e.y:
		rect_start = s*$"../../..".current_cell_size + Vector2(0,$"../../..".current_cell_size)
		rect_end = (e-s)*$"../../..".current_cell_size + Vector2($"../../..".current_cell_size,-$"../../..".current_cell_size)
	rect_c = c

func draw_cursor(p):
	cursor_p = p
	update()

func _draw():
	draw_rect(Rect2($"../../..".struct_object_root*Vector2(1,1)*$"../../..".current_cell_size,Vector2(1,1)*$"../../..".current_cell_size),Color(1,0,0,0.25))
	if cursor_p != null:
		draw_rect(Rect2(cursor_p,Vector2($"../../..".current_cell_size,$"../../..".current_cell_size)),Color(1,1,1,0.25))
	if !null in [rect_c,rect_end,rect_start]:
		draw_rect(Rect2(rect_start,rect_end),rect_c)
	if type == 0:
		draw_rect(Rect2(Vector2(-$"../../..".current_cell_size*length/2,0).snapped(Vector2(1,1)*$"../../..".current_cell_size),Vector2($"../../..".current_cell_size*length,$"../../..".current_cell_size)),Color(1,1,0,0.25))
	elif type == 1:
		draw_rect(Rect2(Vector2(0,-$"../../..".current_cell_size*length/2).snapped(Vector2(1,1)*$"../../..".current_cell_size),Vector2($"../../..".current_cell_size,$"../../..".current_cell_size*length)),Color(1,1,0,0.25))
	elif type == 2:
		draw_rect(Rect2((Vector2(1,1)*(-$"../../..".current_cell_size*length/2)).snapped(Vector2(1,1)*$"../../..".current_cell_size),Vector2(1,1)*$"../../..".current_cell_size*length),Color(1,1,0,0.25))