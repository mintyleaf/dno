extends TextureRect

var points = []
var highlight_point = null
var mouse_position = null

func _draw():
	
	draw_rect(Rect2(Vector2(0,0),rect_size),Color(1,0,0),false)
	if mouse_position != null:
		draw_rect(Rect2(mouse_position-Vector2(4,4),Vector2(8,8)),Color(0,1,0))
	
	for i in points:
		if i == highlight_point:
			draw_circle(i*$"../../..".zoom,4,Color(1,0.4,0.4))
		else:
			draw_circle(i*$"../../..".zoom,4,Color(1,1,0.4))
	
	if points.size() > 2:
		$"../polygon".show()
		var zoomed_points = []
		for i in points:
			zoomed_points.append(i*$"../../..".zoom)
		$"../polygon".polygon = zoomed_points
	else:
		$"../polygon".hide()