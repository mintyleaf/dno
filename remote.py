import socket
from random import randint

SERVERDATA_AUTH = 3
SERVERDATA_AUTH_RESPONSE = 2

SERVERDATA_EXECCOMMAND = 2
SERVERDATA_RESPONSE_VALUE = 0

def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)

ip = "127.0.0.1"
port = 10569
password = ""
id = random_with_N_digits(9)

is_conn = False

remote_client = None

print("Available commands:\nconnect [ip] [port] [password], exit\nUse 'help' after connecting to server for a list of available server commands.")

def wait_response():
    
    global remote_client
    global is_conn
    
    size = None
    resp_id = None
    resp_type = None
    body = None

    while True:
        
        if size == None:
            data = remote_client.recv(4)
            size = int.from_bytes(data, byteorder='little', signed=True)
        elif resp_id == None:
            data = remote_client.recv(4)
            resp_id = int.from_bytes(data, byteorder='little', signed=True)
        elif resp_type == None:
            data = remote_client.recv(4)
            resp_type = int.from_bytes(data, byteorder='little', signed=True)
        elif body == None:
            data = remote_client.recv(size-9)
            body = data.decode('utf-8')
        else:
            data = remote_client.recv(1)
            break
    
    if resp_type == SERVERDATA_AUTH_RESPONSE:
        if resp_id == id:
            print("Auth success!")
        else:
            print("Auth failed!")
            remote_client.close()
            is_connn = False
    else:
        print(body)

def send_data(type, id, body):
    
    global remote_client

    body = body.encode()
    size = len(body) + 10

    data = size.to_bytes(4, byteorder='little', signed=True)
    data += id.to_bytes(4, byteorder='little', signed=True)
    data += type.to_bytes(4, byteorder='little', signed=True)
    data += body
    data += b"\x00\x00"

    remote_client.send(data)

    wait_response()

def connect():
    global is_conn
    global remote_client

    if is_conn: remote_client.close()
    remote_client = socket.socket()
    try:
        remote_client.connect( (ip,port) )
    except:
        print("Can't connect!")
        return

    is_conn = True

    send_data(SERVERDATA_AUTH, id, password)

print("> ", end = "")

while True:
    a = input()

    if a == "exit":
        if is_conn: remote_client.close()
        break

    elif a.startswith("connect"):
        a_arr = a.split(" ")
        if len(a_arr) == 4:
            ip = a_arr[1]
            port = int(a_arr[2])
            password = a_arr[3]
            connect()
        else:
            print("Usage: connect [ip] [port] [password]")

    else:
        if is_conn:
            send_data(SERVERDATA_EXECCOMMAND, id, a)
        else:
            print("Connect to the server first!")
            
    print("> ", end = "")
